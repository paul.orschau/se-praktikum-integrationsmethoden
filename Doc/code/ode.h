// A Case Study in Simulation Software Engineering with C++
// Uwe Naumann, STCE, RWTH Aachen, 2018

#ifndef _ODE_INC_
#define _ODE_INC_

#include <Eigen/LU>
#include <Eigen/QR>
using namespace Eigen;

#include "nonlin_sys.h"

// ODE Class
// Represents a system of differential equations
template<typename TS=double, int NP=Dynamic, int NS=Dynamic, typename TP=TS>
class ODE {
  typedef Matrix<TS,NS,1> VTS;
  typedef Matrix<TS,NS,NS> MTS;
  typedef Matrix<TP,NP,1> VTP;
protected:
  VTS _x;       // Current state
  VTP _p;       // Parameters
  VTS _x_saved; // Saved state
  VTP _p_saved; // Saved parameters
public:
  // Constructor
  ODE(int ns, int np) : _x(VTS::Zero(ns)), _p(VTP::Zero(np)) {}

  // Access methods
  int ns() { return _x.size(); }
  int np() { return _p.size(); }
  VTS& x() { return _x; }
  TS& x(int i) { return _x(i); }
  VTP& p() { return _p; }
  TP& p(int i) { return _p(i); }

  // Save and reset methods
  void save() {
    _x_saved = _x;
    _p_saved = _p;
  }
  void reset() {
    _x = _x_saved;
    _p = _p_saved;
  }

  // Virtual methods
  virtual VTS g()=0;
  virtual MTS dgdx()=0;

  // Plotting
  virtual void plot(ostream& s) {
    for (int i=0;i<ns();i++) s << std::fixed << std::setprecision(12) << _x(i) << " ";
  }
};

#include "integrator.h"

// ODE_SOLVER Class
// Uses an integration method to solve a system of differential equations
template<typename TS=double, int NP=Dynamic, int NS=Dynamic, typename TP=TS>
class ODE_SOLVER {
protected:
  TP _T_end;  // End time
  int _nts;   // Number of timesteps
public:
  // Constructor
  ODE_SOLVER() : _T_end(1), _nts(1) {}

  // Access methods
  TP& T_end() { return _T_end; }
  int& nts() { return _nts; }

  // Routines
  // Helper function used for solving without plotting
  void solve(ODE<TS,NP,NS,TP>& ode, INTEGRATOR<TS,NP,NS,TP>& integrator) {
    ofstream s;
    solve_and_plot(ode,integrator,s);
  }
  // Function which solves an ODE system with a given integrator
  void solve_and_plot(ODE<TS,NP,NS,TP>& ode, INTEGRATOR<TS,NP,NS,TP>& integrator, ostream& s, const int& plotPoints = 100) {
    assert(plotPoints > 0);   // Interval can not be zero
    assert(_nts > 0);         // Number of time steps can not be zero
    assert(_T_end > 0);       // End time can not be zero
    // Calculate step size
    TP h = _T_end/_nts;
    int plotInterval = _nts/plotPoints+1;
    // Calculate solution for all time steps
    for (int i=0;i<nts();i++) {
      // Plot previous (and initial) solution
      if (i%plotInterval == 0) {
        s << std::fixed << std::setprecision(3) << i*h << " ";
        ode.plot(s);
        s << std::endl;
      }
      // Integrate gradient
      ode.x() += integrator.integrate(i*h,(i+1)*h,ode);
    }
    // Output final solution
    s << std::fixed << std::setprecision(3) << _T_end << " ";
    ode.plot(s);
    s << std::endl;    
  }
};

#endif
