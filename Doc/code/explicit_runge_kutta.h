#ifndef _EXP_RUNGE_KUTTA_INC_
#define _EXP_RUNGE_KUTTA_INC_

#include "../integrator.h"

// EXPLICIT RUNGE KUTTA Class
// This integration scheme uses the gradient at the current time step for integration, but has several stages.
// It is an explicit scheme.
template<const int S, typename TS=double, int NP=Dynamic, int NS=Dynamic, typename TP=TS>
class EXPLICIT_RUNGE_KUTTA : public INTEGRATOR<TS,NP,NS,TP> {
  typedef Matrix<TS,NS,1>  VTS;
  typedef Matrix<TS,NS,NS> MTS;
  typedef Matrix<TP,NP,1>  VTP;
  Matrix<TP,S,S> _A;
  Matrix<TP,S,1> _b;
  Matrix<TP,S,1> _c;

public:
  // Constructor
  EXPLICIT_RUNGE_KUTTA() : _A(Matrix<TP,S,S>::Zero()), _b(Matrix<TP,S,1>::Zero()), _c(Matrix<TP,S,1>::Zero()) {};

  // Setter methods
  void setA(Matrix<TP,S,S>& A) { 
    _A = A;
    // Determine whether method is really explicit
    for (int i=0;i<S;i++) {
      for (int j=i;j<S;j++) {
        assert(_A(i,j)==0);
      }
    }
  }
  void setB(Matrix<TP,S,1>& b) { _b=b; }
  void setC(Matrix<TP,S,1>& c) { _c=c; }

  // Routines
  VTS integrate(TP lb, TP ub, ODE<TS,NP,NS,TP>& ode) {
    TP h = ub-lb;
    VTS x_prev = ode.x(); // remember old x

    // calculate vector of increments k
    Matrix<VTS,S,1> k;  // vector of increments
    for (int i=0;i<S;i++) {
      VTS sum = VTS::Zero();
      for (int j=0;j<i;j++) sum += _A(i,j)*k(j);
      ode.x() = x_prev+h*sum; // evaluate f at correct point
      k(i) = ode.g();
    }

    ode.x() = x_prev; // reset x in ode

    // apply weighted sum of k
    VTS sum = VTS::Zero();
    for (int i=0;i<S;i++) sum += _b(i)*k(i);
    return h*sum;
  }
};

#endif