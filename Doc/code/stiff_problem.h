// A Case Study in Simulation Software Engineering with C++
// Paul Orschau, CATS, RWTH Aachen, 2019

#include "../ode.h"

template<typename TS=double, typename TP=TS, const int NP=2, const int NS=1>
class STIFF_PROBLEM : public ODE<TS,NP,NS,TP> {
  using ODE<TS,NP,NS,TP>::_x;
  using ODE<TS,NP,NS,TP>::_p;
public:
  // Constructor
  STIFF_PROBLEM() : ODE<TS,NP,NS,TP>(NS,NP) {} 

  // Gradient
  template<typename LTS,const int LNS=1,typename LTP,const int LNP=2>
  static inline Matrix<LTS,LNS,1> g(Matrix<LTS,LNS,1>& x, Matrix<LTP,LNP,1>& p) {
    Matrix<TS,NS,1> r; 
    r(0)=p(0)*(x(0)-p(1));
    return r;
  }

  // Current gradient
  Matrix<TS,NS,1> g() { 
    return g(_x,_p);
  }

  // Jacobian
  Matrix<TS,NS,NS> dgdx() { 
    Matrix<TS,NS,NS> drdx;
    drdx(0,0)=_p(0); 
    return drdx;
  }
};
