// A Case Study in Simulation Software Engineering with C++
// Uwe Naumann, STCE, RWTH Aachen, 2018

#include<cassert>
#include<cstdlib>
#include<iostream>
#include<time.h>
#include<math.h>
#include<string>
using namespace std;

#include "dco.hpp"
typedef double DCO_BT;
typedef dco::gt1s<DCO_BT>::type DCO_TT;
typedef dco::ga1s<DCO_BT> DCO_AM;
typedef DCO_AM::type DCO_AT;
typedef DCO_AM::tape_t DCO_AT_TAPE_T;

// Include problems
#include "ode.h"
#include "problems/lotka_volterra.h"
#include "problems/diffusion.h"
#include "problems/stiff_problem.h"

// Include integration methods
#include "integrator.h"
#include "methods/explicit_euler.h"
#include "methods/implicit_euler.h"
#include "methods/explicit_runge_kutta.h"
#include "methods/implicit_runge_kutta.h"

// Function which runs a series of predefined tests
// Input arguments:
// ode          – The differential equation system that should be solved
// problem      – A brief description of the ODE, e.g. "Diffusion", which is used for console outputs and in the result file name
// T_end        – End time
// nts          – Number of time steps
// whichTests   – Array of bools that specify which tests should be run
// Ouput arguments:
// times        – Array in which the function writes the runtimes of the tests
template<typename TS=double, int NP=Dynamic, int NS=Dynamic, typename TP=TS>
void runAllTests(ODE<TS,NP,NS,TP>& ode,string problem,TP T_end,int nts,bool* whichTests, double* times) {
  // Save initial state of ode
  ode.save();

  // Configure solver  
  ODE_SOLVER<TS,NP,NS> solver;
  solver.T_end()=T_end;
  solver.nts()=nts;

  // Helper variables
  clock_t startTime;

  // EXPLICIT EULER
  if (whichTests[0]) {
    ode.reset();
    cout << "** Running " << problem << " using Explicit Euler!" << endl;
    startTime = clock();

    EXPLICIT_EULER<TS,NP,NS> integrator;
    ofstream s("sol_" + problem + "_exp_euler.out");
    solver.solve_and_plot(ode,integrator,s);

    times[0] = ((double) (clock() - startTime)) / CLOCKS_PER_SEC;
    cout << "** it took " << times[0] << " seconds!" << endl << endl;
  }

  // EXPLICIT EULER WITH EXPLICIT RUNGE KUTTA
  if (whichTests[1]) {
    ode.reset();
    cout << "** Running " << problem << " using Explicit Euler with Explicit Runge Kutta!" << endl;
    startTime = clock();

    const int S = 1;
    Matrix<TP,S,S> A;  A << 0;
    Matrix<TP,S,1> b;  b << 1;
    Matrix<TP,S,1> c;  c << 0;
    EXPLICIT_RUNGE_KUTTA<S,TS,NP,NS> integrator;
    integrator.setA(A);
    integrator.setB(b);
    integrator.setC(c);
    ofstream s("sol_" + problem + "_exp_euler_exp_RK.out");
    solver.solve_and_plot(ode,integrator,s);
  
    times[1] = ((double) (clock() - startTime)) / CLOCKS_PER_SEC;
    cout << "** it took " << times[1] << " seconds!" << endl << endl;
  }

  // EXPLICIT EULER WITH IMPLICIT RUNGE KUTTA
  if (whichTests[2]) {
    ode.reset();
    cout << "** Running " << problem << " using Explicit Euler with Implicit Runge Kutta!" << endl;
    startTime = clock();

    const int S = 1;
    Matrix<TP,S,S> A;  A << 0;
    Matrix<TP,S,1> b;  b << 1;
    Matrix<TP,S,1> c;  c << 0;
    const int SN = S*NS;
    QR<TS,SN> lsol;
    NEWTON<TS,NP,SN> nlsol(lsol);
    nlsol.eps() =  1e-7;
    IMPLICIT_RUNGE_KUTTA<S,SN,TS,NP,NS> integrator(nlsol);
    integrator.setA(A);
    integrator.setB(b);
    integrator.setC(c);
    ofstream s("sol_" + problem + "_exp_euler_imp_RK.out");
    solver.solve_and_plot(ode,integrator,s);
   
    times[2] = ((double) (clock() - startTime)) / CLOCKS_PER_SEC;
    cout << "** it took " << times[2] << " seconds!" << endl << endl;
 }

  // IMPLICIT EULER
  if (whichTests[3]) {
    ode.reset();
    cout << "** Running " << problem << " using Implicit Euler!" << endl;
    startTime = clock();

    QR<TS,NS> lsol;
    NEWTON<TS,NP,NS> nlsol(lsol);
    nlsol.eps() =  1e-7;
    IMPLICIT_EULER<TS,NP,NS> integrator(nlsol);
    ofstream s("sol_" + problem + "_imp_euler.out");
    solver.solve_and_plot(ode,integrator,s);
   
    times[3] = ((double) (clock() - startTime)) / CLOCKS_PER_SEC;
    cout << "** it took " << times[3] << " seconds!" << endl << endl;
  }

  // IMPLICIT EULER WITH IMPLICIT RUNGE KUTTA
  if (whichTests[4]) {
    ode.reset();
    cout << "** Running " << problem << " using Implicit Euler with Implicit Runge Kutta!" << endl;
    startTime = clock();

    const int S = 1;
    Matrix<TP,S,S> A;  A << 1;
    Matrix<TP,S,1> b;  b << 1;
    Matrix<TP,S,1> c;  c << 1;
    const int SN = S*NS;
    QR<TS,SN> lsol;
    NEWTON<TS,NP,SN> nlsol(lsol);
    nlsol.eps() =  1e-7;
    IMPLICIT_RUNGE_KUTTA<S,SN,TS,NP,NS> integrator(nlsol);
    integrator.setA(A);
    integrator.setB(b);
    integrator.setC(c);
    ofstream s("sol_" + problem + "_imp_euler_imp_RK.out");
    solver.solve_and_plot(ode,integrator,s);
   
    times[4] = ((double) (clock() - startTime)) / CLOCKS_PER_SEC;
    cout << "** it took " << times[4] << " seconds!" << endl << endl;
  }

  // CLASSICAL RUNGE KUTTA WITH EXPLICIT RUNGE KUTTA
  if (whichTests[5]) {
    ode.reset();
    cout << "** Running " << problem << " using Classical Runge Kutta with Explicit Runge Kutta!" << endl;
    startTime = clock();

    const int S = 4;
    Matrix<TP,S,S> A;  A << 0,  0,  0,  0,
                            0.5,0,  0,  0,
                            0,  0.5,0,  0,
                            0,  0,  1,  0;
    Matrix<TP,S,1> b;  b << 1.0/6, 1.0/3, 1.0/3, 1.0/6;
    Matrix<TP,S,1> c;  c << 0,  0.5,0.5,1;
    EXPLICIT_RUNGE_KUTTA<S,TS,NP,NS> integrator;
    integrator.setA(A);
    integrator.setB(b);
    integrator.setC(c);
    ofstream s("sol_" + problem + "_classic_RK_exp_RK.out");
    solver.solve_and_plot(ode,integrator,s);
   
    times[5] = ((double) (clock() - startTime)) / CLOCKS_PER_SEC;
    cout << "** it took " << times[5] << " seconds!" << endl << endl;
  }

  // CLASSICAL RUNGE KUTTA WITH IMPLICIT RUNGE KUTTA
  if (whichTests[6]) {
    ode.reset();
    cout << "** Running " << problem << " using Classical Runge Kutta with Implicit Runge Kutta!" << endl;
    startTime = clock();

    const int S = 4;
    Matrix<TP,S,S> A;  A << 0,  0,  0,  0,
                            0.5,0,  0,  0,
                            0,  0.5,0,  0,
                            0,  0,  1,  0;
    Matrix<TP,S,1> b;  b << 1.0/6, 1.0/3, 1.0/3, 1.0/6;
    Matrix<TP,S,1> c;  c << 0,  0.5,0.5,1;
    const int SN = S*NS;
    QR<TS,SN> lsol;
    NEWTON<TS,NP,SN> nlsol(lsol);
    nlsol.eps() =  1e-7;
    IMPLICIT_RUNGE_KUTTA<S,SN,TS,NP,NS> integrator(nlsol);
    integrator.setA(A);
    integrator.setB(b);
    integrator.setC(c);
    ofstream s("sol_" + problem + "_classic_RK_imp_RK.out");
    solver.solve_and_plot(ode,integrator,s);
   
    times[6] = ((double) (clock() - startTime)) / CLOCKS_PER_SEC;
    cout << "** it took " << times[6] << " seconds!" << endl << endl;
  }

  // TRAPEZOIDAL WITH IMPLICIT RUNGE KUTTA
  if (whichTests[7]) {
    ode.reset();
    cout << "** Running " << problem << " using Trapezoidal!" << endl;
    startTime = clock();

    const int S = 2;
    Matrix<TP,S,S> A;  A << 0,  0,
                            0.5,0.5;
    Matrix<TP,S,1> b;  b << 0.5,0.5;
    Matrix<TP,S,1> c;  c << 0,  1;
    const int SN = S*NS;
    QR<TS,SN> lsol;
    NEWTON<TS,NP,SN> nlsol(lsol);
    nlsol.eps() =  1e-7;
    IMPLICIT_RUNGE_KUTTA<S,SN,TS,NP,NS> integrator(nlsol);
    integrator.setA(A);
    integrator.setB(b);
    integrator.setC(c);
    ofstream s("sol_" + problem + "_trapzoidal.out");
    solver.solve_and_plot(ode,integrator,s);
   
    times[7] = ((double) (clock() - startTime)) / CLOCKS_PER_SEC;
    cout << "** it took " << times[7] << " seconds!" << endl << endl;
  }
}

// Function which prints the results for all the tests to the console
// Input arguments:
// problem      – A brief description of the ODE, e.g. "Diffusion", which is used for console outputs and in the result file name
// NS           – Number of dimensions of the ODE system
// nts          – Number of time steps
// whichTests   – Array of bools that specify which positions in the "times" array contain data
// times        – Array of the runtimes of the tests
void printResults(string problem, int NS, int nts, bool* whichTests, double* times) {
  printf("#########################################################################\n");
  printf("# Problem:       %s\n", problem.c_str());
  printf("# Dimensions:    %i\n", NS);
  printf("# Time Steps:    %i\n", nts);
  printf("# \n");
  printf("# Execution times:\n");
  if (whichTests[0]) printf("# %50s:     %7.2f sec\n",  "Explicit Euler",                                   times[0]);
  if (whichTests[1]) printf("# %50s:     %7.2f sec\n",  "Explicit Euler with Explicit Runge Kutta",         times[1]);
  if (whichTests[2]) printf("# %50s:     %7.2f sec\n",  "Explicit Euler with Implicit Runge Kutta",         times[2]);
  if (whichTests[3]) printf("# %50s:     %7.2f sec\n",  "Implicit Euler",                                   times[3]);
  if (whichTests[4]) printf("# %50s:     %7.2f sec\n",  "Implicit Euler with Implicit Runge Kutta",         times[4]);
  if (whichTests[5]) printf("# %50s:     %7.2f sec\n",  "Classical Runge Kutta with Explicit Runge Kutta",  times[5]);
  if (whichTests[6]) printf("# %50s:     %7.2f sec\n",  "Classical Runge Kutta with Implicit Runge Kutta",  times[6]);
  if (whichTests[7]) printf("# %50s:     %7.2f sec\n",  "Trapezoidal",                                      times[7]);
  printf("#########################################################################\n");
  printf("\n");
}

// Main function
// Defines the three different ode systems and runs all tests on all of them
int main() {
  // Helper arrays
  bool whichTests[8];
  double times[8];

  // LOTKA_VOLTERRA
  {
    const int NS=2,NP=4; 
    LOTKA_VOLTERRA<double> ode;
    ode.x(0)=1000; ode.x(1)=20;
    ode.p(0)=0.015; ode.p(1)=0.0001; ode.p(2)=0.03; ode.p(3)=0.0001;

    double T_end = 1000;
    int nts = 100000;

    for (int i=0;i<8;i++) {
      whichTests[i] = true;
      times[i] = 0;
    }
    runAllTests<double,NP,NS>(ode,"lotka",T_end,nts,whichTests,times);
    printResults("Lotka-Volterra",NS,nts,whichTests,times);
  }

  // DIFFUSION
  {
    const int NS=40,NP=3; 
    DIFFUSION<double,NS> ode(NS);
    for (int i=0;i<NS;i++) ode.x(i)=sin(M_PI*(i+1)*(1.0/(NS+1)));
    ode.p(0)=1; ode.p(1)=0; ode.p(2)=0;

    double T_end = 1;
    int nts = 10000;

    for (int i=0;i<8;i++) {
      whichTests[i] = true;
      times[i] = -1;
    }
    whichTests[6]=false; // Skip Classical Runge Kutta with Implicit Runge Kutta
    runAllTests<double,NP,NS>(ode,"diff",T_end,nts,whichTests,times);
    printResults("Diffusion",NS,nts,whichTests,times);
  }

  // STIFF PROBLEM
  {
    const int NS=1,NP=2; 
    STIFF_PROBLEM<double> ode;
    ode.x(0)=1;
    ode.p(0)=-4;
    ode.p(1)=2;

    double T_end = 5;
    int nts = 9;

    for (int i=0;i<8;i++) {
      whichTests[i] = true;
      times[i] = 0;
    }
    runAllTests<double,NP,NS>(ode,"stiff",T_end,nts,whichTests,times);
    printResults("Stiff Problem",NS,nts,whichTests,times);
  }

  return 0;
}
