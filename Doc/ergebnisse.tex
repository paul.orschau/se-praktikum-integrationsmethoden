\chapter{Messergebnisse der Testprobleme}
\label{ch:7}

\section{Getestete Verfahren}

Obwohl wir nur vier verschiedene Integrationsmethoden tatsächlich implementiert haben, lassen sich mit unseren Klassen deutlich mehr Verfahren testen. Wir haben die folgenden acht Konfigurationen getestet:

\begin{enumerate}
    \item Expliziter Euler – Mit der EXPLICIT\_EULER Klasse
    \item Expliziter Euler – Mit der EXPLICIT\_RUNGE\_KUTTA Klasse
    \item Expliziter Euler – Mit der IMPLICIT\_RUNGE\_KUTTA Klasse
    \item Impliziter Euler – Mit der IMPLICIT\_EULER Klasse
    \item Impliziter Euler – Mit der IMPLICIT\_RUNGE\_KUTTA Klasse
    \item Klassisches Runge Kutta – Mit der EXPLICIT\_RUNGE\_KUTTA Klasse
    \item Klassisches Runge Kutta – Mit der IMPLICIT\_RUNGE\_KUTTA Klasse
    \item Trapezregel – Mit der IMPLICIT\_RUNGE\_KUTTA Klasse
\end{enumerate}{}

Das \enquote{Klassische Runge Kutta} Verfahren ist dabei durch folgenden Butcher-Tableau gegeben:

\[
\renewcommand\arraystretch{1.2}
\begin{array}
{c|cccc}
0\\
\frac{1}{2} & \frac{1}{2}\\
\frac{1}{2} &0 &\frac{1}{2} \\
1& 0& 0& 1\\
\hline
& \frac{1}{6} &\frac{1}{3} &\frac{1}{3} &\frac{1}{6} 
\end{array}
\]

Durch diese Auswahl an Konfigurationen war es uns u.A. möglich, die Laufzeit und Ergebnisse von (theoretisch) identischen Verfahren zu testen, wenn diese von unterschiedlichen Klassen ausgeführt werden. So sollte zum Beispiel der Implizite Euler theoretisch identische Ergebnisse liefern, ob er nun \enquote{nativ} als eigene Klasse ausgeführt wird, oder mithilfe der IMPLICIT\_RUNGE\_KUTTA Klasse und folgendem Butcher Tableau:

\[
\renewcommand\arraystretch{1.2}
\begin{array}
{c|c}
1&1\\
\hline
& 1 
\end{array}
\]

Ebenfalls sollte es bei einem expliziten Runge-Kutta Verfahren theoretisch keinerlei Unterschied in den Ergebnissen geben, wenn man es mit der IMPLICIT\_RUNGE\_KUTTA Klasse ausführt. (Umgekehrt ist es nicht möglich, ein implizites Runge-Kutta Verfahren mit der EXPLICIT\_RUNGE\_KUTTA Klasse auszuführen, daher kann dies auch nicht getestet werden).

In der Realität kommen allerdings Unterschiede zustande, die sich durch das Lösen der nichtlinearen Gleichungssysteme erklären lassen. Hierbei nutzen wir das Newton-Verfahren zur Nullstellensuche, wobei es sich um ein iteratives Verfahren handelt, welches die Lösung i.d.R. nicht exakt berechnen kann, sondern nur bis zu einer gewissen Fehlertoleranz hin.

Dieser minimale Fehler lässt sich in den Ergebnissen erkennen:

\begin{figure}[H]
    \hspace*{-3cm}
    \includegraphics[scale=0.4]{source/Diff1.png}
    \caption{Vergleich der Ergebnisse zum Lotka-Volterra Problem zwischen \enquote{nativem} Impliziten Euler und der Runge-Kutta Variante – Eigentlich erwartet man identische Ergebnisse}
    \label{fig1}
\end{figure}

Da die EXPLICI\_RUNGE\_KUTTA Klasse keine nichtlinearen Gleichungen lösen muss, sondern die einzelnen Stages direkt und explizit ausrechnen kann, liefert der Explizite Euler tatsächlich identische Ergebnisse zu seiner \enquote{nativen} Version:

\begin{figure}[H]
    \hspace*{-2cm}
    \includegraphics[scale=0.4]{source/Diff2.png}
    \caption{Vergleich der Ergebnisse zum Lotka-Volterra Problem zwischen \enquote{nativem} Explizitem Euler und der Runge-Kutta Variante – Hier sind die Ergebnisse tatsächlich identisch!}
    \label{fig1}
\end{figure}

\section{Ergebnisse Diffusion}

Zunächst einmal erlaubt uns dieses Beispielproblem die Anzahl der Dimensionen $n$ mehr oder weniger frei anzupassen, und somit die Performance der Verfahren hinsichtlich der Dimension zu vergleichen.

\begin{figure}[H]
    \hspace*{-6cm}
    \includegraphics[scale=0.4]{source/plots/diffusion_graf_gross.png}
    \caption{Laufzeit in Sekunden über Anzahl Dimensionen für verschiedene Verfahren}
    \label{dif1}
\end{figure}

Hier ist besonders auffällig, wie teuer die impliziten Verfahren sind! Die IMPLICIT\_RUNGE\_KUTTA Klasse löst in jedem Zeitschritt ein Gleichungssystem von $n*s$ nichtlinearen Gleichungen mittels des Newton-Verfahrens. Jede einzelne Newton Iteration muss $s$ Mal den Gradienten des Differentialgleichungssystems auswerten, sowie $n*s$ lineare Gleichungen lösen.

Aber auch der native Implizite Euler, der ohne mehrere Stages auskommt, braucht deutlich länger als alle expliziten Verfahren. Bei 250 Dimensionen braucht er etwa 40 Sekunden.

Alle expliziten Verfahren sind so schnell, dass man sie im Diagramm kaum erkennen kann. Sie brauchen selbst bei 200 Dimensionen nur Bruchteile von Sekunden für die Rechnung. In unseren Experimenten wurden die numerischen Ergebnisse der expliziten Verfahren allerdings völlig unbrauchbar bei mehr als 200 Dimensionen, wofür wir leider keine gute Erklärung finden konnten. Wir vermuten, dass es mit der Diskretisierung der Diffusionsgleichung zusammenhängt, und dass das finite Differenzen Schema ab einer bestimmten Anzahl von Dimensionen für explizite Verfahren zusammenbricht.

\begin{figure}[H]
    \hspace*{-4cm} 
    \includegraphics[scale=0.4]{source/plots/diffusion_grad_zoom.png}
    \caption{Laufzeit in Sekunden über Anzahl Dimensionen für verschiedene Verfahren (Zoom)}
    \label{dif2}
\end{figure}
	
\section{Ergebnisse Lotka-Volterra}

\begin{figure}[H]
	\hspace*{-1cm} 
	\includegraphics[scale=0.8]{source/plots/lotka_resultat_graf.png}
	\caption{Lösung für Lotka-Volterra}
	\label{lv1}
\end{figure}

\begin{figure}[H]
    \centering
	\includegraphics[scale=0.8]{source/plots/lotka_resultat_graf_kreis.png}
	\caption{Lotka-Volterra Phasenplot}
	\label{lv2}
\end{figure}

Das Lotka-Volterra Problem eignet sich gut um die Anzahl der Zeitschritte zu variieren. Im Diagramm \ref{lv3} ist die Lauzeit der verschiedenen Testkonfigurationen über der Anzahl Zeitschritte geplottet. Man kann es aufgrund der logarithmischen Skala zwar kaum erkennen, doch alle Testkonfigurationen skalieren linear mit der Anzahl Zeitschritte. Das ist wenig überraschend, denn in jedem Zeitschritt wird die {\tt integrate()} Funktion genau einmal ausgeführt. Die Konfiguratioenn unterscheiden sich also nur in ihrer jeweiligen \enquote{Basislaufzeit} der {\tt integrate()} Funktion, und diese variiert stark.

\begin{figure}[H]
    \hspace*{-4cm} 
	\includegraphics[scale=0.35]{source/plots/lotka_times.png}
	\caption{Laufzeit in Sekunden über Anzahl Zeitschritte für verschiedene Verfahren, logarithmisch skaliert}
	\label{lv3}
\end{figure}

Außerdem lässt sich hier gut beobachten, wie schnell die einzelnen Verfahren zusammenbrechen, wenn man die Anzahl der Zeitschritte verringert, und die Zeitschritte somit vergrößert. Während man bei diesem Testproblem bei $10^5$ Zeitschritten noch kaum Unterschiede zwischen den Verfahren sehen kann (\ref{n5}), so sieht man bei $10^4$ und $10^3$ Zeitschritten (\ref{n4} \& \ref{n3}) bereits, wie die beiden Euler Verfahren die richtige Lösung jeweils unter- oder überschätzen. Selbst bei nur 100 Zeitschritten liefert sowohl das explizite klassische Runge-Kutta Verfahren als auch das implizite Trapezverfahren noch brauchbare Ergebnisse! (\ref{n2})

\begin{figure}[H]
    \hspace*{-4cm} 
	\includegraphics[scale=0.35]{source/plots/Lotka_Volterran100000.png}
	\caption{Lotka-Volterra Lösung für $10^5$ Zeitschritte}
	\label{n5}
\end{figure}

\begin{figure}[H]
    \hspace*{-4cm} 
	\includegraphics[scale=0.35]{source/plots/Lotka_Volterran10000.png}
	\caption{Lotka-Volterra Lösung für $10^2$ Zeitschritte}
	\label{n4}
\end{figure}

\begin{figure}[H]
    \hspace*{-4cm} 
	\includegraphics[scale=0.35]{source/plots/Lotka_Volterran1000.png}
	\caption{Lotka-Volterra Lösung für $10^3$ Zeitschritte}
	\label{n3}
\end{figure}

\begin{figure}[H]
    \hspace*{-4cm} 
	\includegraphics[scale=0.35]{source/plots/Lotka_Volterran100.png}
	\caption{Lotka-Volterra Lösung für $100$ Zeitschritte}
	\label{n2}
\end{figure}

\section{Ergebnisse Steifes Problem}

Das steife Anfangswertproblem diente vor allem dazu, zu demonstrieren, wie stark sich verschiedene Verfahren in manchen Situationen unterscheiden können. Während die impliziten Verfahren alle stabil bleiben, wird der Explizite Euler instabil. Das explizite klassische Runge-Kutta Verfahren hingegen bleibt aufgrund seiner großen Anzahl von Stages für dieses Testproblem stabil.

\begin{figure}[H]
	    \hspace*{-5cm} 
	    \includegraphics[scale=0.4]{source/plots/stiff_gross.png}
	    \caption{Lösung für Steifes Problem für verschiedene Verfahren}
	    \label{dif1}
\end{figure}
	
\begin{figure}[H]
	    \hspace*{-6cm} 
	    \includegraphics[scale=0.4]{source/plots/stiff_zoom.png}
	    \caption{Lösung für Steifes Problem für verschiedene Verfahren (Zoom)}
	    \label{dif2}
\end{figure}

Da wir bei diesem Problem auch die analytische Lösung kennen, lässt sich der Fehler der Verfahren und die jeweilige Fehlerordnung in Abhängigkeit der Schrittweite $h$ genau bestimmen. In Abbildung \ref{err1} sind die Fehler zur analytischen Lösung in Abhängigkeit von $h$ aufgetragen, und in Abbildung \ref{err2} sind Referenzlinien eingeblendet, die den jeweiligen Fehlerordnungen entsprechen.

Man kann erkennen, dass die beiden Euler Verfahren eine Fehlerordnung von $\mathcal{O}(h)$ besitzen, das Trapezverfahren eine Ordnung von $\mathcal{O}(h^2)$ und das klassische Runge-Kutta Verfahren eine Ordnung von $\mathcal{O}(h^4)$.  

\begin{figure}[H]
	    \hspace*{-4cm} 
	    \includegraphics[scale=0.4]{source/plots/error.png}
	    \caption{Fehlerordnung für verschiedene Verfahren}
	    \label{err1}
\end{figure}

\begin{figure}[H]
	    \hspace*{-6cm} 
	    \includegraphics[scale=0.4]{source/plots/error_with_Os.png}
	    \caption{Fehlerordnung für verschiedene Verfahren (mit Referenzlinien)}
	    \label{err2}
\end{figure}