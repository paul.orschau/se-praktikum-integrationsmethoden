Ziel des Projekts ist es, einen gegebenen ODE Solver dahingehend zu erweitern, dass man *austauschbare Integrationsmethoden* verwenden kann.

Die Mitglieder von Gruppe 2 sind:
- Florian Berthold
- Steffen Thevis
- Anas Hamrouni
- Paul Orschau