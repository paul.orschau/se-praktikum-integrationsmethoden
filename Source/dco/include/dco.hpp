// dco/c++ version: v3.3.1
// branch: 3.3.1
// used DCO_FLAGS: -DDCO_LICENSE

// ================================================================ //
// *** This is a generated file from above given source version *** //
// ================================================================ //
#pragma once

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable: 4512 )
#pragma warning( disable: 4503 )
#pragma warning( disable: 4127 )
#pragma warning( disable: 4355 )
#endif

#include "dco_global_includes.hpp"
#include "dco_configuration.hpp"
#include "dco_logging.hpp"


  
       
       
       






namespace dco {






typedef DCO_TAPE_INT_DEFINE DCO_TAPE_INT;
inline DCO_TAPE_INT& stce_9752( DCO_TAPE_INT& stce_9753) { return stce_9753; }
inline const DCO_TAPE_INT& stce_9752(const DCO_TAPE_INT& stce_9753) { return stce_9753; }


typedef DCO_TAPE_INT_DEFINE DCO_INTEGRAL_TAPE_INT;





static const bool stce_9754 = true;







static const bool stce_9755 = false;



static const bool stce_9756 = true;







static const bool stce_9757 = true;





static const bool stce_9758 = false;


struct stce_9759
{

    static const bool stce_9760 = true;



};

template<class stce_9761,
    class stce_9762 = stce_9761,
    class stce_9763 = stce_9761,
    class stce_9764 = stce_9761>
struct stce_9765 : stce_9759
{
  typedef stce_9761 derivative_t;
  typedef stce_9762 stce_9766;
  typedef stce_9763 adjoint_real_t;
  typedef stce_9764 ext_adjoint_real_t;
};

template<class stce_9767>
struct stce_9768 : stce_9759
{
  typedef stce_9767 derivative_t;
  typedef stce_9767 stce_9766;
  typedef stce_9767 adjoint_real_t;
  typedef stce_9767 ext_adjoint_real_t;
};

}
       
       
namespace dco
{
typedef unsigned long mem_long_t;
inline size_t get_allocation_size(size_t stce_9769);
struct memory_model {
  enum stce_9770
  {
    stce_9771,
    BLOB_TAPE,
    BLOB_TAPE_SPLINT,
    CHUNK_TAPE,
    stce_9772,
    stce_9773,
    stce_9774,
    stce_9775
  };
};
namespace internal
{
template<class stce_9776, class stce_9777>
  struct active_type;
template<class stce_9776, class stce_9778, class stce_9779>
  struct stce_9780;
template<class stce_9776, class stce_9781, class stce_9782, class stce_9779>
  struct stce_9783;
template<class stce_9776, class stce_9781, class stce_9779>
  struct stce_9784;
template<class stce_9776, class stce_9782, class stce_9779>
  struct stce_9785;
template <typename stce_9786, typename enable_if = void>
  struct trait_value;
template <typename stce_9786, typename stce_9787 = void, typename enable_if = void>
  struct stce_9788;
template <typename stce_9786, typename enable_if = void>
  struct stce_9789;
template<typename stce_9786, typename enable_if = void>
  struct stce_9790;
template<typename stce_9786, typename enable_if = void>
  struct stce_9791;
template<enum memory_model::stce_9770 MEMORY_MODEL,
         class stce_9792,
         enum memory_model::stce_9770 stce_9793=memory_model::stce_9771>
  class tape;
template<typename stce_9786>
  struct stce_9794 {};
template<memory_model::stce_9770 MEMORY_MODEL,
         typename stce_9792,
         memory_model::stce_9770 stce_9793>
  struct stce_9794<tape<MEMORY_MODEL, stce_9792, stce_9793> > {
  typedef typename stce_9792::stce_9766 type;
};
template<class stce_9795, typename stce_9796>
  class stce_9797;
template<class stce_9798, class stce_9799>
  struct jacobian_preaccumulator_t;
}
template <class stce_9800> struct mode;
enum stce_9801 {
  stce_9802, stce_9803, stce_9804, stce_9805
};
template <typename stce_9786>
  typename internal::stce_9790<stce_9786>::stce_9806 tape(const stce_9786&);
  struct stce_9807 {
    static const int stce_9808 = 0;
  };
}
namespace dco {
template<memory_model::stce_9770 MEMORY_MODEL, class stce_9792, memory_model::stce_9770 stce_9793>
  bool is_null(internal::tape<MEMORY_MODEL, stce_9792, stce_9793> *tape) {
  return (tape == NULL);
}
 static unsigned int stce_9809=0xABCDE;
 static unsigned int stce_9810(unsigned int *stce_9811, unsigned int stce_9812) {
    int stce_9813;
    stce_9813 = *stce_9811 & 1;
    *stce_9811 >>=1;
    if(stce_9813==1)
        *stce_9811=stce_9812;
    return *stce_9811;
 }
 static inline double stce_9814() {
      stce_9810(&stce_9809, 0xB4BCD35C);
      unsigned int stce_9815 = stce_9810(&stce_9809, 0xB4BCD35C);
      return double(stce_9815)/RAND_MAX;
 }
  template <class stce_9786>
  std::string
  type_name()
  {
    return typeid(stce_9786).name();
  }
namespace internal {
template <typename stce_9786, bool>
struct stce_9816 { typedef stce_9786 type; };
template <typename stce_9786>
  struct stce_9816<stce_9786, true> { typedef stce_9786& type; };
}
namespace helper {
static bool stce_9817 = true;
template<typename stce_9786>
  struct stce_9818 {
  static const bool stce_9753 = true;
};
template<bool stce_9819, class stce_9786 = void>
struct stce_9820 {};
template<class stce_9786>
struct stce_9820<true, stce_9786> { typedef stce_9786 type; };
template<class stce_9786, class stce_9821>
struct is_same {
  static const bool value = false;
};
template<class stce_9786>
struct is_same<stce_9786, stce_9786> {
  static const bool value = true;
};
template <typename stce_9786>
  struct stce_9822 {
  typedef stce_9786 type;
};
template<class stce_9767, const int stce_9823=1>
  struct stce_9824 {
  static const int stce_9825 = stce_9823;
  typedef stce_9767 value_t;
  stce_9767 stce_9826[stce_9823];
  void stce_9827(const stce_9767 &stce_9828) {
    for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_9823; ++stce_9829)
      stce_9826[stce_9829] = stce_9828;
  }
  stce_9824(const stce_9767 &stce_9828) {
    stce_9827(stce_9828);
  }
  stce_9824() {
    stce_9827(0);
  }
  stce_9767 &operator[](const DCO_TAPE_INT stce_9830) {
    return stce_9826[stce_9830];
  }
  const stce_9767 &operator[](const DCO_TAPE_INT stce_9830) const {
    return stce_9826[stce_9830];
  }
  stce_9824 &operator= (const stce_9767 &stce_9831) {
    for (int stce_9829 = 0; stce_9829 < stce_9823; ++stce_9829)
      this->stce_9826[stce_9829] = stce_9831;
    return *this;
  }
  stce_9824 &operator += (const stce_9824 &stce_9831) {
    for (int stce_9829 = 0; stce_9829 < stce_9823; ++stce_9829)
      this->stce_9826[stce_9829] += stce_9831.stce_9826[stce_9829];
    return *this;
  }
};
template <typename stce_9767, const int stce_9823>
  bool operator==(stce_9824<stce_9767, stce_9823> const& stce_9832, int const& stce_9833) {
  for (int stce_9829 = 0; stce_9829 < stce_9823; ++stce_9829) {
    if (stce_9832[stce_9829] != stce_9833) {
      return false;
    }
  }
  return true;
}
template<class stce_9786>
struct stce_9834 {
  static const int stce_9835;
};
template<class stce_9786>
const int stce_9834<stce_9786>::stce_9835 = 1;
template<class stce_9767, int stce_9823>
struct stce_9834<stce_9824<stce_9767, stce_9823> > {
  static const int stce_9835;
};
template<class stce_9767, int stce_9823>
const int stce_9834<stce_9824<stce_9767, stce_9823> > ::stce_9835 = stce_9823;
template<class stce_9767, int stce_9823>
  static inline
  stce_9824<stce_9767, stce_9823>
  operator + (const stce_9824<stce_9767, stce_9823> &stce_9836, const stce_9824<stce_9767, stce_9823> &stce_9831) {
  stce_9824<stce_9767, stce_9823> stce_9837;
  for (int stce_9829 = 0; stce_9829 < stce_9823; ++stce_9829)
    stce_9837.stce_9826[stce_9829] = stce_9836.stce_9826[stce_9829] + stce_9831.stce_9826[stce_9829];
  return stce_9837;
}
template<class stce_9767, int stce_9823>
static inline stce_9824<stce_9767, stce_9823> operator *(const stce_9824<stce_9767, stce_9823> &stce_9836,
  const stce_9824<stce_9767, stce_9823> &stce_9831)
{
 stce_9824<stce_9767, stce_9823> stce_9837;
 for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_9823; ++stce_9829)
  stce_9837.stce_9826[stce_9829] = stce_9836.stce_9826[stce_9829] * stce_9831.stce_9826[stce_9829];
 return stce_9837;
}
template<class stce_9767, class stce_9838, int stce_9823>
static inline stce_9824<stce_9767, stce_9823> operator*(const stce_9838 &stce_9839,
  const stce_9824<stce_9767, stce_9823> &stce_9840)
{
 stce_9824<stce_9767, stce_9823> stce_9837;
 for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_9823; ++stce_9829)
  stce_9837.stce_9826[stce_9829] = stce_9840.stce_9826[stce_9829] * stce_9839;
 return stce_9837;
}
template<class stce_9767, class stce_9838, int stce_9823>
static inline stce_9824<stce_9767, stce_9823> operator *(const stce_9824<stce_9767, stce_9823> &stce_9836,
  const stce_9838 &stce_9831)
{
 stce_9824<stce_9767, stce_9823> stce_9837;
 for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_9823; ++stce_9829)
  stce_9837.stce_9826[stce_9829] = stce_9836.stce_9826[stce_9829] * stce_9831;
 return stce_9837;
}
template<class stce_9767, int stce_9823>
  static inline std::ostream &operator << (std::ostream &out, const stce_9824<stce_9767, stce_9823> &stce_9841) {
  out << "(";
  for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_9823 - 1; ++stce_9829) {
    out << stce_9841.stce_9826[stce_9829] << ";";
  }
  out << stce_9841.stce_9826[stce_9823 - 1];
  out << ")";
  return out;
}
}
template<typename stce_9770>
  class stce_9842 {
  const stce_9770 &stce_9786;
public:
  stce_9842(const stce_9770 &stce_9843) : stce_9786(stce_9843) {}
  template <typename stce_9844>
    stce_9844 *create() const {
    return new stce_9844(stce_9786);
  }
};
template<>
  class stce_9842<void *> {
public:
  stce_9842(void *stce_9845) {
    (void) stce_9845;
  }
  template <typename stce_9844>
    stce_9844 *create() const {
    return new stce_9844();
  }
};
class stce_9846 {
  std::ofstream stce_9847;
  std::string stce_9848;
  std::string stce_9849;
  std::stringstream stce_9850;
  std::stringstream stce_9851;
public:
  stce_9846(const std::string &filename) : stce_9848("digraph {\n"), stce_9849("\n}\n") {
    stce_9850.clear();
    stce_9851.clear();
    stce_9847.open(filename.c_str());
  }
  void stce_9852(std::vector<DCO_TAPE_INT> &stce_9853, std::vector<DCO_TAPE_INT> &stce_9854, DCO_TAPE_INT stce_9855) {
    stce_9850 << stce_9855 << "[shape=box,label=\"(" << stce_9855 << ", ext. function)\",color=\"red\"]\n";
    for (std::vector<DCO_TAPE_INT>::iterator stce_9829 = stce_9853.begin(); stce_9829 != stce_9853.end(); ++stce_9829) {
      stce_9851 << *stce_9829 << " -> " << stce_9855 << "[label=\"unknown" << "\",color=\"red\"]\n";
    }
    for (std::vector<DCO_TAPE_INT>::iterator stce_9829 = stce_9854.begin(); stce_9829 != stce_9854.end(); ++stce_9829) {
      stce_9851 << stce_9855 << " -> " << *stce_9829 << "[label=\"unknown" << "\",color=\"red\"]\n";
    }
  }
  void stce_9856(DCO_TAPE_INT stce_9855) {
    if(stce_9855 == 0){
      return;
    }
    stce_9850 << stce_9855 << "[shape=box,label=\"" << stce_9855 << "\", color=\"green\"]\n";
  }
  template <typename stce_9857>
  void stce_9856(DCO_TAPE_INT stce_9855, stce_9857 stce_9858) {
    if(stce_9855 == 0){
      return;
    }
    stce_9850 << stce_9855 << "[shape=box,label=\"(" << stce_9855 << ", " << stce_9858 << ")\", color=\"green\"]\n";
  }
  template <typename stce_9857>
  void stce_9859(DCO_TAPE_INT stce_9860, DCO_TAPE_INT stce_9861, stce_9857 stce_9862) {
    if(stce_9860 == 0 || stce_9861 == 0){
      return;
    }
    stce_9851 << stce_9860 << " -> " << stce_9861 << "[label=\"" << stce_9862 << "\"]\n";
  }
  ~stce_9846() {
    stce_9847 << stce_9848;
    stce_9847 << stce_9850.str();
    stce_9847 << stce_9851.str();
    stce_9847 << stce_9849;
    stce_9847.close();
  }
};
template<bool stce_9819, typename stce_9786 = void> struct enable_if {};
template< typename stce_9786 > struct enable_if<true, stce_9786> { typedef stce_9786 type; };
template<class stce_9786> struct remove_const { typedef stce_9786 type; };
template<class stce_9786> struct remove_const<const stce_9786> { typedef stce_9786 type; };
template <typename stce_9863, typename stce_9864>
  struct stce_9865 {
  static const bool value = (stce_9863::stce_9808 < stce_9864::stce_9808);
};
template <>
  struct stce_9865<void, void> {
  static const bool value = false;
};
}
       
namespace dco {
  namespace internal {
    template<class stce_9776, class stce_9799 = stce_9776>
    struct stce_9866 {
      DCO_TAPE_INT stce_9867;
      DCO_TAPE_INT stce_9868;
      DCO_TAPE_INT _progvarcounter;
      std::string stce_9869;
      std::ofstream stce_9870;
      std::ofstream stce_9871;
    private:
      stce_9866(const std::string stce_9872) : stce_9867(0), stce_9868(0), _progvarcounter(0), stce_9869(stce_9872) {
      }
    public:
      static stce_9866 *create(const std::string stce_9872, const std::string filename) {
        const char *stce_9848 = "//=================================================//\n"
                             "// This file is generated by dco/c++.              //\n"
                             "// Further inquiries info@stce.rwth-aachen.de      //\n"
                             "//=================================================//\n\n"
                             "#include <cmath>\n\n";
        stce_9866 *stce_9837 = new stce_9866(stce_9872);
        stce_9837->stce_9870.open(filename.c_str());
        stce_9837->stce_9870 << stce_9848 << "void " << stce_9872 << "(const double *x, double *x_a1s, double *y, const double *y_a1s) {\n double va0_a1s=0; \n";
        stce_9837->stce_9871.open("ad_tmp");
        return stce_9837;
      }
      static void remove(stce_9866 *tape) {
        tape->stce_9871.close();
        std::ifstream stce_9873("ad_tmp");
        std::vector<std::string> stce_9874;
        std::string stce_9875;
        while (std::getline(stce_9873, stce_9875)) {
          stce_9874.insert(stce_9874.begin(), stce_9875);
        }
        tape->stce_9870 << "// BEGIN OF ADJOINT SECTION\n";
        for (size_t stce_9829 = 0; stce_9829 < stce_9874.size(); ++stce_9829) {
          tape->stce_9870 << stce_9874[stce_9829] << "\n";
        }
        tape->stce_9870 << "}\n";
        tape->stce_9870.close();
      }
      template<class stce_9876>
      void register_variable(stce_9876 &stce_9877) {
        _progvarcounter++;
        stce_9877._tape_index() = _progvarcounter;
        stce_9870 << "const double &va" << _progvarcounter << "=x[" << stce_9867 << "];\n";
        stce_9870 << "double &va" << _progvarcounter << "_a1s=x_a1s[" << stce_9867 << "];\n";
        stce_9867++;
      }
      template<class stce_9876>
      void register_output_variable(stce_9876 &stce_9877) {
        stce_9870 << "y[" << stce_9868 << "]=va" << stce_9877._tape_index() << ";\n";
        stce_9870 << "va" << stce_9877._tape_index() << "_a1s+=y_a1s[" << stce_9868 << "];\n";
        stce_9868++;
      }
    };
    class stce_9878
    {
      template<typename>
      friend class stce_9879;
      DCO_TAPE_INT stce_9880;
      stce_9878(): stce_9880(0) {}
    public:
      std::string stce_9881() const {
        std::stringstream stce_9882;
        stce_9882 << "va" << stce_9880;
        return stce_9882.str();
      }
      DCO_TAPE_INT& _tape_index() {
        return stce_9880;
      }
      DCO_TAPE_INT _tape_index() const {
        return stce_9880;
      }
      void clear() {
        _tape_index() = 0;
      }
    };
    template<class stce_9883>
    class stce_9879: public stce_9878{
    public:
      typedef stce_9883 mode_t;
      stce_9879(){}
      template<class stce_9884>
      __attribute__((always_inline)) inline void stce_9885(const stce_9884 &stce_9886) {
        stce_9886.stce_9887(stce_9883::global_tape->stce_9870);
        _tape_index() = stce_9883::global_tape->_progvarcounter;
        std::stringstream stce_9888;
        stce_9888 << "va" << ++stce_9883::global_tape->_progvarcounter;
       stce_9883::global_tape->stce_9871 << stce_9886.stce_9881() << "_a1s+=" <<
            stce_9888.str() << "_a1s;\t";
        stce_9886.stce_9889(stce_9883::global_tape->stce_9871);
        stce_9883::global_tape->stce_9871 << "//EOL\n";
        _tape_index()++;
        stce_9883::global_tape->stce_9870 << "double " << stce_9881()
            << "=" << stce_9886.stce_9881() << ";\n";
        stce_9883::global_tape->stce_9870 << "double "
            << stce_9881() << "_a1s=0.0;\n";
      }
    };
  }
}
namespace dco {
namespace internal {
static inline bool is_character(const std::string &stce_9890, size_t stce_9830) {
  if(stce_9830 >=stce_9890.size()) return false;
  return std::isalpha(stce_9890[stce_9830]) != 0 ;
}
static inline std::string replace_all_in_string(std::string& str, const std::string &stce_9860, const std::string &stce_9861) {
  size_t stce_9891=0;
  while ((stce_9891 = str.find(stce_9860, stce_9891)) != std::string::npos) {
    if((stce_9891 > 0 && is_character(str, stce_9891-1))
      || is_character(str, stce_9891 + stce_9860.length())) {
        stce_9891++;
        continue;
    }
    str.replace(stce_9891, stce_9860.length(), stce_9861);
    stce_9891 += stce_9861.length();
  }
  return str;
}
template<class stce_9776>
class stce_9892 {
  static DCO_TAPE_INT stce_9893;
  DCO_TAPE_INT stce_9894;
public:
  stce_9892() :
      stce_9894(stce_9893++) {
  }
   std::string stce_9881() const {
    std::stringstream stce_9882;
    stce_9882 << "im" << stce_9894;
    return stce_9882.str();
  }
protected:
  void stce_9895(std::ofstream &out, std::string stce_9896) const {
    out << "const double " << stce_9881() << "=" << stce_9896 << ";\n";
    out << "double " << stce_9881() << "_a1s=0.0;\n";
  }
  template<class stce_9786>
  std::string str(stce_9786 arg) const {
    return arg.stce_9881();
  }
  std::string str(stce_9776 stce_9897) const {
    std::stringstream str;
    str << std::scientific << std::setprecision(17) << stce_9897;
    return str.str();
  }
  template <class stce_9898>
  void stce_9899(std::string& stce_9896, const stce_9898& arg, const std::string stce_9900) const {
    replace_all_in_string(stce_9896, stce_9900 + "._value()", str(arg));
  }
  void stce_9899(std::string& stce_9896, const stce_9776& arg, const std::string stce_9900) const {
    replace_all_in_string(stce_9896, stce_9900, str(arg));
  }
  template <class stce_9779, class stce_9901, class stce_9902>
  void stce_9903(std::ofstream &out, const stce_9901& stce_9904, const stce_9902& stce_9905) const {
    std::string stce_9896 = stce_9779::stce_9906();
    this->stce_9899(stce_9896, stce_9904, "arg1");
    this->stce_9899(stce_9896, stce_9905, "arg2");
    out << this->str(stce_9904) << "_a1s +=(" << stce_9896 << ")*" << this->stce_9881() << "_a1s;\t";
    stce_9904.stce_9889(out);
  }
  template <class stce_9779, class stce_9902>
  void stce_9903(std::ofstream&, const stce_9776&, const stce_9902&) const {}
  template <class stce_9779, class stce_9901, class stce_9902>
  void stce_9907(std::ofstream &out, const stce_9901& stce_9904, const stce_9902& stce_9905) const {
    std::string stce_9896 = stce_9779::stce_9908();
    this->stce_9899(stce_9896, stce_9904, "arg1");
    this->stce_9899(stce_9896, stce_9905, "arg2");
    out << this->str(stce_9905) << "_a1s +=(" << stce_9896 << ")*" << this->stce_9881() << "_a1s;\t";
    stce_9905.stce_9889(out);
  }
  template <class stce_9779, class stce_9901>
  void stce_9907(std::ofstream&, const stce_9901&, const stce_9776&) const {}
};
template<class stce_9776>
DCO_TAPE_INT stce_9892<stce_9776>::stce_9893 = 0;
}
}
       
       
       
namespace dco {
  class exception {
  public:
    template <typename stce_9909>
    static stce_9909 create(std::string stce_9910, std::string stce_9911 = "", int stce_9875 = 0) {
      std::stringstream stce_9912;
      stce_9912 << "--- dco/c++ --- " << stce_9910;
      if (stce_9911 != "") stce_9912 << " --- " << stce_9911 << ":" << stce_9875 << ".";
      DCO_LOG(dco::logERROR) << "EXCEPTION thrown: " << stce_9912.str();
      return stce_9909(stce_9912.str());
    }
    template <typename stce_9909>
    static stce_9909 stce_9913(std::string stce_9910, std::string stce_9911 = "", int stce_9875 = 0) {
      std::stringstream stce_9912;
      stce_9912 << "--- dco/c++ --- " << stce_9910;
      if (stce_9911 != "") stce_9912 << " --- " << stce_9911 << ":" << stce_9875 << ".";
      DCO_LOG(dco::logERROR) << "EXCEPTION thrown: " << stce_9912.str();
      return stce_9909();
    }
};
}
       
       
namespace dco {
namespace folding {
template <typename stce_9786, typename enable_if = void>
  struct is_zero_trait {
  static bool get(const stce_9786& stce_9841) {
    return stce_9841 == 0;
  }
};
template <typename stce_9786>
  bool stce_9914(const stce_9786& stce_9841) {
  return is_zero_trait<stce_9786>::get(stce_9841);
}
template <typename stce_9786>
  struct is_zero_trait<stce_9786, typename dco::enable_if<dco::mode<stce_9786>::is_tangent_type>::type> {
  static bool get(const stce_9786& stce_9841) {
    return stce_9914(value(stce_9841)) && stce_9914(derivative(stce_9841));
  }
};
template <typename stce_9786>
  struct is_zero_trait<stce_9786, typename dco::enable_if<dco::mode<stce_9786>::is_adjoint_type>::type> {
  static bool get(const stce_9786& stce_9841) {
    return stce_9914(value(stce_9841)) && !tape_index(stce_9841);
  }
};
}
}
namespace dco {
namespace folding {
template<class stce_9915>
  class stce_9916 {
  typedef typename stce_9915::derivative_t stce_9776;
  typedef typename stce_9915::stce_9917 stce_9918;
public:
  stce_9916(stce_9915* tape) : stce_9919(1), stce_9920(tape->stce_9921()) {}
  template<class stce_9922>
    __attribute__((always_inline)) inline stce_9922& stce_9923(stce_9776 pval) {
    stce_9919 *= pval;
    return static_cast<stce_9922&>(*this);
  }
  __attribute__((always_inline)) inline stce_9776& pval() { return stce_9919; }
  __attribute__((always_inline)) inline DCO_TAPE_INT index() const { return stce_9920.index(); }
  template <class stce_9924>
    __attribute__((always_inline)) inline void insert(stce_9924 stce_9841) {
    const DCO_TAPE_INT stce_9925 = stce_9841._tape_index();
    if (!stce_9756 || stce_9925 != 0) {
      if (!stce_9757 || !stce_9914(stce_9919)) {
        stce_9920.insert(stce_9925, stce_9919);
      }
    }
  }
protected:
  stce_9776 stce_9919;
private:
  stce_9916();
  stce_9916(stce_9916& stce_9926);
  stce_9918 stce_9920;
};
template<class stce_9915>
  class stce_9927 : public stce_9916<stce_9915> {
  typedef typename stce_9915::derivative_t stce_9776;
  typedef typename stce_9915::stce_9917 stce_9918;
public:
  stce_9927(stce_9915* tape) : stce_9916<stce_9915>(tape),
    stce_9928(0) {
  }
  template <class stce_9924>
    __attribute__((always_inline)) inline void insert(stce_9924 stce_9841) {
    stce_9916<stce_9915>::insert(stce_9841);
    stce_9928 += this->stce_9919 * stce_9841.stce_9929;
  }
  stce_9776 stce_9930() const {
    return stce_9928;
  }
private:
  stce_9927();
  stce_9927(stce_9927& stce_9926);
  stce_9776 stce_9928;
};
template<class stce_9931, class stce_9776, class stce_9932>
  __attribute__((always_inline)) inline void interpret(const dco::internal::active_type<stce_9776, stce_9932> &stce_9841,
                                   stce_9931& stce_9933) {
  stce_9933.insert(stce_9841);
}
template<class stce_9931, class stce_9776, class stce_9934, class stce_9935, class stce_9936>
  __attribute__((always_inline)) inline void interpret(
                                   const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841,
                                   stce_9931& stce_9933) {
  stce_9776 stce_9937 = stce_9933.pval();
  interpret<stce_9931>(stce_9841.stce_9938, stce_9933.template stce_9923<stce_9931>(stce_9841.stce_9939()));
  stce_9933.pval() = stce_9937;
  interpret<stce_9931>(stce_9841.stce_9940, stce_9933.template stce_9923<stce_9931>(stce_9841.stce_9941()));
  stce_9933.pval() = stce_9937;
}
template<class stce_9931, class stce_9776, class stce_9942, class stce_9936>
  __attribute__((always_inline)) inline void interpret(
                                   const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841,
                                   stce_9931& stce_9933) {
  stce_9776 stce_9937 = stce_9933.pval();
  interpret<stce_9931>(stce_9841.stce_9943, stce_9933.template stce_9923<stce_9931>(stce_9841.pval()));
  stce_9933.pval() = stce_9937;
}
template<class stce_9931, class stce_9776, class stce_9934, class stce_9936>
  __attribute__((always_inline)) inline void interpret(
                                   const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841,
                                   stce_9931& stce_9933) {
  stce_9776 stce_9937 = stce_9933.pval();
  interpret<stce_9931>(stce_9841.stce_9938, stce_9933.template stce_9923<stce_9931>(stce_9841.stce_9939()));
  stce_9933.pval() = stce_9937;
}
template<class stce_9931, class stce_9776, class stce_9935, class stce_9936>
  __attribute__((always_inline)) inline void interpret(
                                   const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841,
                                   stce_9931& stce_9933) {
  stce_9776 stce_9937 = stce_9933.pval();
  interpret<stce_9931>(stce_9841.stce_9940, stce_9933.template stce_9923<stce_9931>(stce_9841.stce_9941()));
  stce_9933.pval() = stce_9937;
}
template<class stce_9915>
  class stce_9944 {
public:
  stce_9944() :
    stce_9945(0) {
  }
  template <class stce_9924>
    __attribute__((always_inline)) inline void insert(stce_9924 stce_9841) {
    stce_9945 = stce_9841.tape();
  }
  stce_9915* tape() const {
    return stce_9945;
  }
private:
  stce_9944(stce_9944& stce_9926);
  stce_9915* stce_9945;
};
template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_9915>
  __attribute__((always_inline)) inline void interpret(
                                   const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841,
                                   stce_9944<stce_9915>& stce_9933) {
  interpret(stce_9841.stce_9938, stce_9933);
  if(stce_9933.tape())
    {
      if(stce_9755)
        {
          stce_9915 * tape = stce_9933.tape();
          interpret(stce_9841.stce_9940, stce_9933);
          if (stce_9933.tape() && tape != stce_9933.tape())
            throw dco::exception::create<std::runtime_error>(
                                                             "multiple tape error: different tapes on right-hand side of statement",
                                                             "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_active_type_folding.hpp", 156);
        }
      return;
    }
  interpret(stce_9841.stce_9940, stce_9933);
}
template<class stce_9776, class stce_9942, class stce_9936, class stce_9915>
  __attribute__((always_inline)) inline void interpret(
                                   const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841,
                                   stce_9944<stce_9915>& stce_9933) {
  interpret(stce_9841.stce_9943, stce_9933);
}
template<class stce_9776, class stce_9934, class stce_9936, class stce_9915>
  __attribute__((always_inline)) inline void interpret(
                                   const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841,
                                   stce_9944<stce_9915>& stce_9933) {
  interpret(stce_9841.stce_9938, stce_9933);
}
template<class stce_9776, class stce_9935, class stce_9936, class stce_9915>
  __attribute__((always_inline)) inline void interpret(
                                   const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841,
                                   stce_9944<stce_9915>& stce_9933) {
  interpret(stce_9841.stce_9940, stce_9933);
}
}
}
namespace dco {
namespace internal {
struct stce_9946 {
  template<class stce_9947, class stce_9948> static bool stce_9949(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950._value() == stce_9951._value(); } template<class stce_9947, class stce_9948> static bool stce_9952(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950._value() == stce_9951; } template<class stce_9947, class stce_9948> static bool stce_9953(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950 == stce_9951._value(); }
  template<class stce_9947, class stce_9948> static bool stce_9954(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950._value() != stce_9951._value(); } template<class stce_9947, class stce_9948> static bool stce_9955(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950._value() != stce_9951; } template<class stce_9947, class stce_9948> static bool stce_9956(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950 != stce_9951._value(); }
  template<class stce_9947, class stce_9948> static bool stce_9957(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950._value() < stce_9951._value(); } template<class stce_9947, class stce_9948> static bool stce_9958(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950._value() < stce_9951; } template<class stce_9947, class stce_9948> static bool stce_9959(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950 < stce_9951._value(); }
  template<class stce_9947, class stce_9948> static bool stce_9960(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950._value() <= stce_9951._value(); } template<class stce_9947, class stce_9948> static bool stce_9961(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950._value() <= stce_9951; } template<class stce_9947, class stce_9948> static bool stce_9962(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950 <= stce_9951._value(); }
  template<class stce_9947, class stce_9948> static bool stce_9963(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950._value() > stce_9951._value(); } template<class stce_9947, class stce_9948> static bool stce_9964(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950._value() > stce_9951; } template<class stce_9947, class stce_9948> static bool stce_9965(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950 > stce_9951._value(); }
  template<class stce_9947, class stce_9948> static bool stce_9966(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950._value() >= stce_9951._value(); } template<class stce_9947, class stce_9948> static bool stce_9967(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950._value() >= stce_9951; } template<class stce_9947, class stce_9948> static bool stce_9968(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { return stce_9950 >= stce_9951._value(); }
};
template<class stce_9969, stce_9969 *&stce_9945>
struct stce_9970 {
  template<class stce_9947, class stce_9948> static bool stce_9949(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950._value() == stce_9951._value(); const bool stce_9971 = stce_9950.stce_9972(0) == stce_9951.stce_9972(0); const bool stce_9973 = stce_9950.stce_9972(1) == stce_9951.stce_9972(1); stce_9945->stce_9974("compare_AA", stce_9837, stce_9971,stce_9973); return stce_9837; } template<class stce_9947, class stce_9948> static bool stce_9952(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950._value() == stce_9951; const bool stce_9971 = stce_9950.stce_9972(0) == stce_9951; const bool stce_9973 = stce_9950.stce_9972(1) == stce_9951; stce_9945->stce_9974("compare_AP", stce_9837, stce_9971, stce_9973); return stce_9837; } template<class stce_9947, class stce_9948> static bool stce_9953(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950 == stce_9951._value(); const bool stce_9971 = stce_9950 == stce_9951.stce_9972(0); const bool stce_9973 = stce_9950 == stce_9951.stce_9972(1); stce_9945->stce_9974("compare_PA", stce_9837, stce_9971, stce_9973); return stce_9837; }
  template<class stce_9947, class stce_9948> static bool stce_9954(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950._value() != stce_9951._value(); const bool stce_9971 = stce_9950.stce_9972(0) != stce_9951.stce_9972(0); const bool stce_9973 = stce_9950.stce_9972(1) != stce_9951.stce_9972(1); stce_9945->stce_9974("compare_AA", stce_9837, stce_9971,stce_9973); return stce_9837; } template<class stce_9947, class stce_9948> static bool stce_9955(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950._value() != stce_9951; const bool stce_9971 = stce_9950.stce_9972(0) != stce_9951; const bool stce_9973 = stce_9950.stce_9972(1) != stce_9951; stce_9945->stce_9974("compare_AP", stce_9837, stce_9971, stce_9973); return stce_9837; } template<class stce_9947, class stce_9948> static bool stce_9956(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950 != stce_9951._value(); const bool stce_9971 = stce_9950 != stce_9951.stce_9972(0); const bool stce_9973 = stce_9950 != stce_9951.stce_9972(1); stce_9945->stce_9974("compare_PA", stce_9837, stce_9971, stce_9973); return stce_9837; }
  template<class stce_9947, class stce_9948> static bool stce_9957(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950._value() < stce_9951._value(); const bool stce_9971 = stce_9950.stce_9972(0) < stce_9951.stce_9972(0); const bool stce_9973 = stce_9950.stce_9972(1) < stce_9951.stce_9972(1); stce_9945->stce_9974("compare_AA", stce_9837, stce_9971,stce_9973); return stce_9837; } template<class stce_9947, class stce_9948> static bool stce_9958(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950._value() < stce_9951; const bool stce_9971 = stce_9950.stce_9972(0) < stce_9951; const bool stce_9973 = stce_9950.stce_9972(1) < stce_9951; stce_9945->stce_9974("compare_AP", stce_9837, stce_9971, stce_9973); return stce_9837; } template<class stce_9947, class stce_9948> static bool stce_9959(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950 < stce_9951._value(); const bool stce_9971 = stce_9950 < stce_9951.stce_9972(0); const bool stce_9973 = stce_9950 < stce_9951.stce_9972(1); stce_9945->stce_9974("compare_PA", stce_9837, stce_9971, stce_9973); return stce_9837; }
  template<class stce_9947, class stce_9948> static bool stce_9960(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950._value() <= stce_9951._value(); const bool stce_9971 = stce_9950.stce_9972(0) <= stce_9951.stce_9972(0); const bool stce_9973 = stce_9950.stce_9972(1) <= stce_9951.stce_9972(1); stce_9945->stce_9974("compare_AA", stce_9837, stce_9971,stce_9973); return stce_9837; } template<class stce_9947, class stce_9948> static bool stce_9961(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950._value() <= stce_9951; const bool stce_9971 = stce_9950.stce_9972(0) <= stce_9951; const bool stce_9973 = stce_9950.stce_9972(1) <= stce_9951; stce_9945->stce_9974("compare_AP", stce_9837, stce_9971, stce_9973); return stce_9837; } template<class stce_9947, class stce_9948> static bool stce_9962(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950 <= stce_9951._value(); const bool stce_9971 = stce_9950 <= stce_9951.stce_9972(0); const bool stce_9973 = stce_9950 <= stce_9951.stce_9972(1); stce_9945->stce_9974("compare_PA", stce_9837, stce_9971, stce_9973); return stce_9837; }
  template<class stce_9947, class stce_9948> static bool stce_9963(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950._value() > stce_9951._value(); const bool stce_9971 = stce_9950.stce_9972(0) > stce_9951.stce_9972(0); const bool stce_9973 = stce_9950.stce_9972(1) > stce_9951.stce_9972(1); stce_9945->stce_9974("compare_AA", stce_9837, stce_9971,stce_9973); return stce_9837; } template<class stce_9947, class stce_9948> static bool stce_9964(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950._value() > stce_9951; const bool stce_9971 = stce_9950.stce_9972(0) > stce_9951; const bool stce_9973 = stce_9950.stce_9972(1) > stce_9951; stce_9945->stce_9974("compare_AP", stce_9837, stce_9971, stce_9973); return stce_9837; } template<class stce_9947, class stce_9948> static bool stce_9965(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950 > stce_9951._value(); const bool stce_9971 = stce_9950 > stce_9951.stce_9972(0); const bool stce_9973 = stce_9950 > stce_9951.stce_9972(1); stce_9945->stce_9974("compare_PA", stce_9837, stce_9971, stce_9973); return stce_9837; }
  template<class stce_9947, class stce_9948> static bool stce_9966(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950._value() >= stce_9951._value(); const bool stce_9971 = stce_9950.stce_9972(0) >= stce_9951.stce_9972(0); const bool stce_9973 = stce_9950.stce_9972(1) >= stce_9951.stce_9972(1); stce_9945->stce_9974("compare_AA", stce_9837, stce_9971,stce_9973); return stce_9837; } template<class stce_9947, class stce_9948> static bool stce_9967(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950._value() >= stce_9951; const bool stce_9971 = stce_9950.stce_9972(0) >= stce_9951; const bool stce_9973 = stce_9950.stce_9972(1) >= stce_9951; stce_9945->stce_9974("compare_AP", stce_9837, stce_9971, stce_9973); return stce_9837; } template<class stce_9947, class stce_9948> static bool stce_9968(const stce_9947 &stce_9950, const stce_9948 &stce_9951) { const bool stce_9837 = stce_9950 >= stce_9951._value(); const bool stce_9971 = stce_9950 >= stce_9951.stce_9972(0); const bool stce_9973 = stce_9950 >= stce_9951.stce_9972(1); stce_9945->stce_9974("compare_PA", stce_9837, stce_9971, stce_9973); return stce_9837; }
};
}
}
namespace dco {
namespace internal {
template<class stce_9975>
class local_gradient_t {
private:
  typedef typename stce_9975::data_t::tape_t tape_t;
  typedef typename tape_t::stce_9976 stce_9976;
  typedef typename tape_t::stce_9917 stce_9917;
  stce_9975& stce_9977;
  stce_9917 stce_9920;
  local_gradient_t(const local_gradient_t&);
public:
  local_gradient_t(stce_9975 &stce_9978)
    : stce_9977(stce_9978)
    , stce_9920(stce_9978.tape()->stce_9921())
  {}
  ~local_gradient_t() {
    stce_9977._tape_index() = stce_9920.index();
  }
  template<typename stce_9798>
    void put(const stce_9798 &stce_9841, const stce_9976& stce_9979) {
    if (!stce_9756 || stce_9841._tape_index() != 0) {
      bool stce_9980 = (stce_9920.index() == 0);
      stce_9920.insert(stce_9841._tape_index(), stce_9979);
      if (stce_9980)
        stce_9977.stce_9981(stce_9977._value(), stce_9814());
    }
  }
};
template<class stce_9883>
struct stce_9982 {
  typedef typename stce_9883::tape_t::stce_9983 derivative_t;
protected:
  mutable DCO_TAPE_INT stce_9880;
public:
  stce_9982() : stce_9880(0){}
  inline stce_9982 &operator =(const stce_9982 &stce_9841) {
    stce_9880 = stce_9841.stce_9880;
    return *this;
  }
  void clear() {
    stce_9880 = 0;
  }
  derivative_t stce_9984() const {
    return _adjoint();
  }
  derivative_t &stce_9984() {
    return _adjoint();
  }
  derivative_t _adjoint() const {
    if (stce_9755 && !stce_9883::global_tape)
      throw dco::exception::create<std::runtime_error>("Tape not created yet. Please use tape_t::create() before accessing derivative components of adjoint types.");
    return stce_9883::global_tape->_adjoint(stce_9880);
  }
  derivative_t &_adjoint(){
    if (stce_9755 && !stce_9883::global_tape)
      throw dco::exception::create<std::runtime_error>("Tape not created yet. Please use tape_t::create() before accessing derivative components of adjoint types.");
    return stce_9883::global_tape->_adjoint(stce_9880);
  }
  void stce_9985(const derivative_t &stce_9845) const {
    if (stce_9755 && !stce_9883::global_tape)
      throw dco::exception::create<std::runtime_error>("Tape not created yet. Please use tape_t::create() before accessing derivative components of adjoint types.");
    stce_9883::global_tape->_adjoint(stce_9880) = stce_9845;
  }
  bool _is_registered() const {
    return stce_9880 == 0 ? false : true;
  }
  DCO_TAPE_INT & _tape_index() { return stce_9880; }
  DCO_TAPE_INT const& _tape_index() const { return stce_9880; }
  template<class stce_9986>
  inline void stce_9981(const stce_9986 &, const double &) { }
  template <typename stce_9924>
    static inline void register_variable(stce_9924& stce_9841, DCO_TAPE_INT index, typename stce_9883::tape_t*) {
    if (stce_9755 && !stce_9883::global_tape) {
      throw dco::exception::create<std::runtime_error>("Global tape not yet created (is NULL).");
    }
    stce_9841.stce_9880 = index;
  }
  static typename stce_9883::tape_t *tape() {
    return stce_9883::global_tape;
  }
  void set_tape(typename stce_9883::tape_t *) {}
};
template<typename stce_9883>
  class single_tape_data : public stce_9982<stce_9883> {
  typedef stce_9982<stce_9883> stce_9987;
public:
  typedef stce_9883 mode_t;
  typedef stce_9946 stce_9988;
  typedef typename stce_9883::tape_t tape_t;
  typedef typename tape_t::stce_9983 derivative_t;
  single_tape_data(){}
  inline single_tape_data &operator =(const single_tape_data &stce_9841) {
    stce_9987::operator= (static_cast<const stce_9987&>(stce_9841));
    return *this;
  }
  template<class stce_9884>
  __attribute__((always_inline)) inline void stce_9885(const stce_9884 &stce_9989) {
    if (!stce_9883::global_tape || !stce_9883::global_tape->is_active()) {
      this->clear();
      return;
    }
    using namespace folding;
    stce_9916<tape_t> stce_9990(stce_9883::global_tape);
    interpret(stce_9989, stce_9990);
    this->stce_9880 = stce_9990.index();
  }
};
template<class stce_9969, typename stce_9883>
  class stce_9991 {
  mutable DCO_TAPE_INT stce_9880;
  mutable stce_9969 *stce_9992;
public:
  typedef stce_9883 mode_t;
  typedef stce_9946 stce_9988;
  typedef stce_9969 tape_t;
  typedef typename tape_t::stce_9983 derivative_t;
  stce_9991() : stce_9880(0), stce_9992(0) {}
  inline stce_9991 &operator =(const stce_9991 &stce_9841) {
   stce_9880 = stce_9841.stce_9880;
   stce_9992 = stce_9841.stce_9992;
    return *this;
  }
  void clear() {
    stce_9880 = 0;
    stce_9992 = 0;
  }
  derivative_t stce_9984() const {
    return _adjoint();
  }
  derivative_t &stce_9984() {
    return _adjoint();
  }
  derivative_t &_adjoint() {
    static derivative_t stce_9993 = 0;
    if (stce_9992 == 0) {
      stce_9993 = 0;
      return stce_9993;
    } else {
      return stce_9992->_adjoint(stce_9880);
    }
  }
  derivative_t _adjoint() const {
      return (stce_9992)?stce_9992->_adjoint(stce_9880):0;
  }
  void stce_9985(const derivative_t &stce_9845) const {
    if (stce_9992 != 0)
      stce_9992->_adjoint(stce_9880) = stce_9845;
  }
  bool _is_registered() const {
    return stce_9880 == 0 ? false : true;
  }
  DCO_TAPE_INT &_tape_index() {
    return stce_9880;
  }
  const DCO_TAPE_INT &_tape_index() const {
    return stce_9880;
  }
  template<typename stce_9986>
  inline void stce_9981(const stce_9986 &, const double &) { }
  template <typename stce_9924>
    static inline void register_variable(stce_9924& stce_9841, DCO_TAPE_INT index, tape_t* tape) {
    stce_9841.stce_9992 = tape;
    stce_9841.stce_9880 = index;
  }
  tape_t *tape() const {
    return stce_9992;
  }
  void set_tape(tape_t *stce_9786) {
    stce_9992 = stce_9786;
  }
  template<class stce_9884>
  __attribute__((always_inline)) inline void stce_9885(const stce_9884 &stce_9989) {
    using namespace folding;
    stce_9944<tape_t> stce_9944;
    interpret(stce_9989, stce_9944);
    tape_t *stce_9994 = stce_9944.tape();
    if (!stce_9994 || !stce_9994->is_active()) {
       clear();
       return;
     }
    stce_9916<tape_t> stce_9990(stce_9994);
    interpret(stce_9989, stce_9990);
    stce_9880 = stce_9990.index();
    stce_9992 = stce_9994;
  }
};
template<typename stce_9883, size_t stce_9995=2>
class stce_9996: public stce_9982<stce_9883> {
  typedef stce_9982<stce_9883> stce_9987;
public:
  typedef stce_9883 mode_t;
  typedef stce_9970<typename stce_9883::tape_t, stce_9883::global_tape> stce_9988;
  typedef typename stce_9883::tape_t tape_t;
  typedef typename tape_t::stce_9983 derivative_t;
  mutable derivative_t stce_9929;
  mutable derivative_t stce_9997[stce_9995];
  void stce_9998(const derivative_t &value, const double &stce_9930) const {
    if(stce_9883::global_tape->stce_9999 == 0)
      this->stce_9997[0] = this->stce_9997[1] = value;
    stce_9883::global_tape->stce_10000(this, stce_9930);
    stce_9929 = stce_9930;
  }
public:
  stce_9996(): stce_9929(0) {}
  inline stce_9996 &operator =(const stce_9996 &stce_9841) {
    stce_9987::operator= (static_cast<const stce_9987&>(stce_9841));
    stce_9929 = stce_9841.stce_9929;
    return *this;
  }
  inline void clear() {
    stce_9987::clear();
    stce_9929 = 0;
  }
  template<class stce_9986>
  inline void stce_9981(const stce_9986 &value, const double &stce_9930) {
    stce_9998(value, stce_9930);
  }
  template <typename stce_9924>
    static inline void register_variable(stce_9924& stce_9841, DCO_TAPE_INT index, tape_t* tape) {
    stce_9987::register_variable(stce_9841, index, tape);
    stce_9841.stce_9998(trait_value<stce_9924>::value(stce_9841), stce_9814());
  }
  const derivative_t& stce_9972(const derivative_t &value, const int stce_9830) const {
      if(this->stce_9880==0) return value;
      return stce_9997[stce_9830];
  }
  template<class stce_9884>
    __attribute__((always_inline)) inline void stce_9885(const stce_9884 &stce_9989) {
    if (!stce_9883::global_tape || !stce_9883::global_tape->is_active()) {
      clear();
      return;
    }
    {
      using namespace folding;
      stce_9927<tape_t> stce_9990(stce_9883::global_tape);
      interpret(stce_9989, stce_9990);
      this->stce_9880 = stce_9990.index();
      if (this->stce_9880 == 0)
        return;
      stce_9929 = stce_9990.stce_9930();
    }
    stce_9997[0] = stce_9989.stce_9972(0);
    stce_9997[1] = stce_9989.stce_9972(1);
    derivative_t stce_10001 = stce_9997[1] - stce_9997[0];
    double stce_10002 = stce_10001/(2*stce_9883::global_tape->stce_9999);
    stce_9883::global_tape->stce_10003(stce_9929, stce_10002);
    stce_9883::global_tape->stce_10004();
    if(stce_9883::global_tape->stce_10005) {
      const double stce_10006=3.1415692;
      stce_9883::global_tape->_adjoint(this->stce_9880) = stce_10006;
      stce_9883::global_tape->interpret_adjoint();
      double stce_10007=0;
      for(size_t stce_9829=0;stce_9829<stce_9883::global_tape->stce_10008.size();++stce_9829)
        stce_10007 += stce_9883::global_tape->_adjoint(stce_9883::global_tape->stce_10008[stce_9829]) * stce_9883::global_tape->stce_10009[stce_9829];
      stce_9883::global_tape->zero_adjoints();
      stce_9883::global_tape->stce_10010(stce_10007, stce_9929 * stce_10006, stce_10002 * stce_10006);
    }
    stce_9883::global_tape->stce_10004();
  }
};
template <typename stce_10011, typename stce_9883>
  struct stce_10012 : public stce_10011 {
  typedef stce_9883 mode_t;
  template<class stce_9884>
    __attribute__((always_inline)) inline void stce_9885(stce_9884 const&) {}
  void clear() {}
  stce_10012& operator=(stce_10012 const&) {
    return *this;
  }
};
}
}
namespace dco
{
  template <class stce_9786>
  class ga1s_codegen {
    static const memory_model::stce_9770 MEMORY_MODEL = memory_model::stce_9772;
    typedef stce_9768<stce_9786> stce_9792;
public:
    typedef typename stce_9792::derivative_t derivative_t;
    typedef typename stce_9792::stce_9766 stce_9766;
    typedef typename internal::stce_9866<derivative_t, stce_9766> tape_t;
    static tape_t *global_tape;
    typedef dco::internal::stce_9879<ga1s_codegen> _data;
    typedef dco::internal::active_type<derivative_t, _data> type;
    typedef typename internal::local_gradient_t<type> local_gradient_t;
    typedef local_gradient_t DCO_DEPRECATED local_gradient_with_activity_t;
    static const bool is_dco_type = true;
    static const bool is_adjoint_type = true;
    static const bool is_tangent_type = false;
    static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
  };
template <class stce_9786>
  typename ga1s_codegen<stce_9786>::tape_t *ga1s_codegen<stce_9786>::global_tape;
}
namespace dco {
namespace internal {
template<class stce_9776>
struct stce_10013 {
  const stce_9776 stce_10014;
  stce_10013(const stce_9776& value) :
      stce_10014(value) {
  }
  inline const stce_9776 &_value() const {
    return stce_10014;
  }
};
template<class stce_9776, class DATA_TYPE>
struct stce_10015 : public stce_10013<stce_9776> {
  stce_10015(const stce_9776& value) :
    stce_10013<stce_9776>(value) {
  }
};
template<class stce_9776>
  struct stce_10015<stce_9776, typename ga1s_codegen<stce_9776>::_data >
: public stce_10013<stce_9776>, public stce_9892<stce_9776> {
  stce_10015(const stce_9776& value) :
    stce_10013<stce_9776>(value) {
  }
};
template<class stce_9776, class stce_9778, class stce_9779>
struct stce_9780: public stce_10015<stce_9776, typename stce_9778::data_t>
  {
      const stce_9778 DCO_TEMPORARY_REFORCOPY stce_9943;
      typedef stce_9776 VALUE_TYPE;
      typedef typename stce_9778::data_t data_t;
      stce_9780(const stce_9778 &arg)
      :stce_10015<stce_9776, data_t>(stce_9779::stce_10016(arg._value())),
       stce_9943(arg) {
      }
    inline const stce_9776 stce_9972(const int stce_9830) const {
        return stce_9779::stce_10016(stce_9943.stce_9972(stce_9830));
    }
    inline const stce_9776 pval() const {
        return stce_9779::stce_10017(this->_value(), stce_9943);
    }
    void stce_9887(std::ofstream &out) const {
      stce_9943.stce_9887(out);
      this->stce_9895(out, stce_9779::stce_10018(this->str(stce_9943)));
    }
    void stce_9889(std::ofstream &out) const {
      std::string stce_9896 = stce_9779::stce_10019();
      this->stce_9899(stce_9896, stce_9943, "x");
      out << this->str(stce_9943) << "_a1s +=(" << stce_9896 << ")*" << this->stce_9881() << "_a1s;\t";
      stce_9943.stce_9889(out);
    }
};
    template<class stce_9776, class stce_9781, class stce_9782, class stce_9779>
    struct stce_9783: public stce_10015<stce_9776, typename stce_9781::data_t> {
      const stce_9781 DCO_TEMPORARY_REFORCOPY stce_9938;
      const stce_9782 DCO_TEMPORARY_REFORCOPY stce_9940;
      typedef stce_9776 VALUE_TYPE;
      typedef typename stce_9781::data_t data_t;
      stce_9783(const stce_9781 &stce_9904, const stce_9782 &stce_9905)
        :stce_10015<stce_9776, data_t>(stce_9779::stce_10016(stce_9904._value(), stce_9905._value())),
        stce_9938(stce_9904),
        stce_9940(stce_9905) {}
      inline const stce_9776 stce_9939() const {
        return stce_9779::stce_10020(this->_value(), stce_9938, stce_9940);
      }
      inline const stce_9776 stce_9941() const {
        return stce_9779::stce_10021(this->_value(), stce_9938, stce_9940);
      }
      inline const stce_9776 stce_9972(const int stce_9830) const {
        return stce_9779::stce_10016(stce_9938.stce_9972(stce_9830),stce_9940.stce_9972(stce_9830));
      }
      void stce_9887(std::ofstream &out) const {
        stce_9938.stce_9887(out);
        stce_9940.stce_9887(out);
        this->stce_9895(out, stce_9779::stce_10018(this->str(stce_9938),
                                                                                   this->str(stce_9940)));
      }
      void stce_9889(std::ofstream &out) const {
        this->template stce_9903<stce_9779>(out, stce_9938, stce_9940);
        this->template stce_9907<stce_9779>(out, stce_9938, stce_9940);
      }
    };
    template<class stce_9776, class stce_9781, class stce_9779>
    struct stce_9784: public stce_10015<stce_9776, typename stce_9781::data_t> {
    const stce_9781 DCO_TEMPORARY_REFORCOPY stce_9938;
    const stce_9776 stce_9940;
      typedef stce_9776 VALUE_TYPE;
      typedef typename stce_9781::data_t data_t;
      stce_9784(const stce_9781 &stce_9904, const stce_9776 &stce_9905)
      :stce_10015<stce_9776, data_t>(stce_9779::stce_10016(stce_9904._value(), stce_9905)),
        stce_9938(stce_9904), stce_9940(stce_9905) {
      }
    inline const stce_9776 stce_9972(const int stce_9830) const {
      return stce_9779::stce_10016(stce_9938.stce_9972(stce_9830), stce_9940);
    }
    inline const stce_9776 stce_9939() const {
        return stce_9779::stce_10020(this->stce_10014, stce_9938, stce_9940);
    }
    void stce_9887(std::ofstream &out) const {
      stce_9938.stce_9887(out);
      this->stce_9895(out, stce_9779::stce_10018(this->str(stce_9938),
          this->str(stce_9940)));
    }
    void stce_9889(std::ofstream &out) const {
      this->template stce_9903<stce_9779>(out, stce_9938, stce_9940);
      this->template stce_9907<stce_9779>(out, stce_9938, stce_9940);
    }
};
    template<class stce_9776, class stce_9782, class stce_9779>
      struct stce_9785: public stce_10015<stce_9776, typename stce_9782::data_t> {
      const stce_9776 stce_9938;
      const stce_9782 DCO_TEMPORARY_REFORCOPY stce_9940;
      typedef stce_9776 VALUE_TYPE;
      typedef typename stce_9782::data_t data_t;
      stce_9785(const stce_9776 &stce_9904, const stce_9782 &stce_9905)
        :stce_10015<stce_9776, data_t>(stce_9779::stce_10016(stce_9904, stce_9905._value())),
        stce_9938(stce_9904), stce_9940(stce_9905) {
      }
      inline const stce_9776 stce_9972(const int stce_9830) const {
        return stce_9779::stce_10016(stce_9938, stce_9940.stce_9972(stce_9830));
      }
      inline const stce_9776 stce_9941() const {
        return stce_9779::stce_10021(this->stce_10014, stce_9938, stce_9940);
      }
      void stce_9887(std::ofstream &out) const {
        stce_9940.stce_9887(out);
        this->stce_9895(out, stce_9779::stce_10018(this->str(stce_9938),
                                                                                   this->str(stce_9940)));
      }
      void stce_9889(std::ofstream &out) const {
        this->template stce_9903<stce_9779>(out, stce_9938, stce_9940);
        this->template stce_9907<stce_9779>(out, stce_9938, stce_9940);
      }
    };
}
}
namespace dco {
template <typename stce_9786,typename stce_10022=stce_9786>
  struct dco_type_constructable_from {
  const static bool value = false;
};
namespace internal {
template <typename stce_9786>
struct passive_value_type_of {
    typedef stce_9786 stce_9770;
};
template <typename stce_9786>
  class stce_10023 {
protected:
  stce_9786 stce_10014;
public:
  stce_10023() : stce_10014(0) {}
  template <typename stce_9821>
    stce_10023(const stce_9821& stce_9841) : stce_10014(stce_9841) {}
};
template<class stce_9776, class stce_9777>
struct active_type : private stce_10023<stce_9776>, public stce_9777 {
  typedef stce_10023<stce_9776> stce_10024;
  using stce_10024::stce_10014;
public:
  typedef stce_9776 VALUE_TYPE;
  typedef stce_9777 data_t;
  typedef typename passive_value_type_of<stce_9776>::stce_9770 stce_10025;
  inline const stce_9776& _value() const {
    return stce_10014;
  }
  stce_9777& data() {
    return *static_cast<stce_9777*>(this);
  }
  inline stce_9776 &_value() {
    return stce_10014;
  }
  inline const stce_9776 stce_9972(const int stce_9830) const {
        return stce_9777::stce_9972(stce_10014, stce_9830);
    }
  inline active_type() :
    stce_10024(static_cast<stce_9776>(0.0)) {
  }
  template<typename stce_9770>
    inline active_type(const stce_9770 &stce_9753, typename dco_type_constructable_from<stce_9770>::type* = 0) :
      stce_10024(stce_9753) { }
  template<typename stce_9770>
    inline typename helper::stce_9820<dco_type_constructable_from<stce_9770>::value, active_type>::type& operator=(const stce_9770 &stce_9753) {
    stce_10014 = stce_9753;
    return *this;
  }
  template<typename stce_9770>
    inline active_type(const stce_9770& stce_9753, typename dco::enable_if<stce_9865<typename stce_9770::data_t::mode_t, typename data_t::mode_t>::value, void*>::type = NULL) :
       stce_10024(stce_9753) { }
  inline active_type(const stce_10025 &stce_9753) : stce_10024(stce_9753) { }
  inline active_type &operator =(const active_type &stce_9841) {
    stce_9777::operator= (static_cast<const stce_9777&>(stce_9841));
    stce_10014 = stce_9841.stce_10014;
    return *this;
  }
  inline active_type &operator =(const stce_10025 &stce_9989) {
    this->stce_10014 = stce_9989;
    this->clear();
    return *this;
  }
  template<class stce_10026, class stce_10027>
  inline active_type &operator =(const active_type<stce_10026, stce_10027> &stce_9989) {
    stce_10014 = stce_9989;
    this->clear();
    return *this;
  }
  template<typename stce_9770>
  inline typename dco_type_constructable_from<stce_9770, active_type>::type& operator=(const stce_9770 &stce_9753){
    stce_10014 = stce_9753;
    this->clear();
    return *this;
  }
    template<class stce_9934, class stce_9935, class stce_9936 > __attribute__((always_inline)) inline active_type(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841) { this->stce_9885(stce_9841); this->stce_10014 = stce_9841.stce_10014; } template<class stce_9934, class stce_9935, class stce_9936 > __attribute__((always_inline)) inline active_type& operator=(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841) { this->stce_9885(stce_9841); this->stce_10014 = stce_9841.stce_10014; return *this; }
    template<class stce_9934, class stce_9936 > __attribute__((always_inline)) inline active_type(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841) { this->stce_9885(stce_9841); this->stce_10014 = stce_9841.stce_10014; } template<class stce_9934, class stce_9936 > __attribute__((always_inline)) inline active_type& operator=(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841) { this->stce_9885(stce_9841); this->stce_10014 = stce_9841.stce_10014; return *this; }
    template<class stce_9935, class stce_9936 > __attribute__((always_inline)) inline active_type(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841) { this->stce_9885(stce_9841); this->stce_10014 = stce_9841.stce_10014; } template<class stce_9935, class stce_9936 > __attribute__((always_inline)) inline active_type& operator=(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841) { this->stce_9885(stce_9841); this->stce_10014 = stce_9841.stce_10014; return *this; }
    template<class stce_9942, class stce_9936 > __attribute__((always_inline)) inline active_type(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841) { this->stce_9885(stce_9841); this->stce_10014 = stce_9841.stce_10014; } template<class stce_9942, class stce_9936 > __attribute__((always_inline)) inline active_type& operator=(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841) { this->stce_9885(stce_9841); this->stce_10014 = stce_9841.stce_10014; return *this; }
    template<class stce_10027> __attribute__((always_inline)) inline active_type& operator += (const active_type<stce_9776, stce_10027> &stce_9841){ *this = *this + stce_9841; return *this; } template<class stce_9934, class stce_9935, class stce_9936 > __attribute__((always_inline)) inline active_type& operator += (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841){ *this = *this + stce_9841; return *this; } template<class stce_9934, class stce_9936 > __attribute__((always_inline)) inline active_type& operator += (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841){ *this = *this + stce_9841; return *this; } template<class stce_9935, class stce_9936 > __attribute__((always_inline)) inline active_type& operator += (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841){ *this = *this + stce_9841; return *this; } template<class stce_9942, class stce_9936 > __attribute__((always_inline)) inline active_type& operator += (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841){ *this = *this + stce_9841; return *this; } __attribute__((always_inline)) inline active_type& operator += (const stce_9776 &stce_9841) { this->_value() += stce_9841; return *this; }
    template<class stce_10027> __attribute__((always_inline)) inline active_type& operator -= (const active_type<stce_9776, stce_10027> &stce_9841){ *this = *this - stce_9841; return *this; } template<class stce_9934, class stce_9935, class stce_9936 > __attribute__((always_inline)) inline active_type& operator -= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841){ *this = *this - stce_9841; return *this; } template<class stce_9934, class stce_9936 > __attribute__((always_inline)) inline active_type& operator -= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841){ *this = *this - stce_9841; return *this; } template<class stce_9935, class stce_9936 > __attribute__((always_inline)) inline active_type& operator -= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841){ *this = *this - stce_9841; return *this; } template<class stce_9942, class stce_9936 > __attribute__((always_inline)) inline active_type& operator -= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841){ *this = *this - stce_9841; return *this; } __attribute__((always_inline)) inline active_type& operator -= (const stce_9776 &stce_9841) { this->_value() -= stce_9841; return *this; }
    template<class stce_10027> __attribute__((always_inline)) inline active_type& operator *= (const active_type<stce_9776, stce_10027> &stce_9841){ *this = *this * stce_9841; return *this; } template<class stce_9934, class stce_9935, class stce_9936 > __attribute__((always_inline)) inline active_type& operator *= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841){ *this = *this * stce_9841; return *this; } template<class stce_9934, class stce_9936 > __attribute__((always_inline)) inline active_type& operator *= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841){ *this = *this * stce_9841; return *this; } template<class stce_9935, class stce_9936 > __attribute__((always_inline)) inline active_type& operator *= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841){ *this = *this * stce_9841; return *this; } template<class stce_9942, class stce_9936 > __attribute__((always_inline)) inline active_type& operator *= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841){ *this = *this * stce_9841; return *this; } inline active_type& operator *= (const stce_9776 &stce_9841) { *this = *this * stce_9841; return *this; }
    template<class stce_10027> __attribute__((always_inline)) inline active_type& operator /= (const active_type<stce_9776, stce_10027> &stce_9841){ *this = *this / stce_9841; return *this; } template<class stce_9934, class stce_9935, class stce_9936 > __attribute__((always_inline)) inline active_type& operator /= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841){ *this = *this / stce_9841; return *this; } template<class stce_9934, class stce_9936 > __attribute__((always_inline)) inline active_type& operator /= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841){ *this = *this / stce_9841; return *this; } template<class stce_9935, class stce_9936 > __attribute__((always_inline)) inline active_type& operator /= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841){ *this = *this / stce_9841; return *this; } template<class stce_9942, class stce_9936 > __attribute__((always_inline)) inline active_type& operator /= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841){ *this = *this / stce_9841; return *this; } inline active_type& operator /= (const stce_9776 &stce_9841) { *this = *this / stce_9841; return *this; }
  inline active_type &operator ++() {
    ++this->stce_10014;
    return *this;
  }
  inline active_type &operator --() {
    --this->stce_10014;
    return *this;
  }
  inline active_type operator ++(int) {
    active_type stce_9837(*this);
    ++this->stce_10014;
    return stce_9837;
  }
  inline active_type operator --(int) {
    active_type stce_9837(*this);
    --this->stce_10014;
    return stce_9837;
  }
  std::string stce_9881() const {
    if (stce_9777::_tape_index() == 0) {
      std::stringstream stce_10028;
      return stce_10028.str();
    } else
      return stce_9777::stce_9881();
  }
  void stce_9887(const std::ofstream &) const {
  }
  void stce_9889(const std::ofstream &) const {
  }
};
template<class stce_9776, class stce_9777>
struct passive_value_type_of<active_type<stce_9776, stce_9777> > {
  typedef typename passive_value_type_of<stce_9776>::stce_9770 stce_9770;
};
}
}
extern "C" {
extern int stce_10029(int stce_10030=false);
extern void* stce_10031(size_t *stce_10032);
extern int stce_10033(int *stce_9830);
extern long stce_10034(long *stce_9830);
extern void stce_10035(void* stce_9882);
}
template <typename stce_9786> inline stce_9786 stce_10036(stce_9786 &stce_9830);
template<> inline int stce_10036<int>(int &stce_9830) {
  return stce_10033(&stce_9830);
}
template<>
  inline long stce_10036<long>(long &stce_9830) {
  return stce_10034(&stce_9830);
}
namespace dco {
  namespace internal {
  template<class stce_9776, typename stce_9883>
    struct ts_data {
    typedef stce_9883 mode_t;
    typedef stce_9946 stce_9988;
    typedef stce_9776 derivative_t;
    derivative_t stce_10037;
    ts_data() : stce_10037(0) {}
    ts_data &operator = (const ts_data &stce_10038) {
      stce_10037 = stce_10038.stce_10037;
      return *this;
    }
    void stce_9985(const derivative_t &stce_9753) {
        stce_10037 = stce_9753;
      }
      void stce_10039(derivative_t &stce_9753) const {
        stce_9753 = stce_10037;
      }
      void clear() {
        stce_10037 = 0;
      }
      const derivative_t &stce_9984() const
      {
        stce_10029();;
        return stce_10037;
      }
      derivative_t &stce_9984()
      {
        stce_10029();;
        return stce_10037;
      }
      template<class stce_9884>
      void stce_9885(const stce_9884 &stce_9989) {
        stce_10037 = stce_10040(stce_9989, 1.0);
      }
      template<class stce_9932 >
      static derivative_t stce_10040(const dco::internal::active_type<stce_9776, stce_9932> &stce_9841, const derivative_t &pval) {
        return stce_9841.stce_10037 * pval;
      }
      template<class stce_9934, class stce_9935, class stce_9936 >
      static derivative_t stce_10040(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841, const derivative_t &pval) {
        return stce_10040(stce_9841.stce_9938, stce_9841.stce_9939() * pval) + stce_10040(stce_9841.stce_9940, stce_9841.stce_9941() * pval);
      }
      template<class stce_9942, class stce_9936 >
      static derivative_t stce_10040(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841, const derivative_t &pval) {
        return stce_10040(stce_9841.stce_9943, stce_9841.pval() * pval);
      }
      template<class stce_9934, class stce_9936 >
      static derivative_t stce_10040(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841, const derivative_t &pval) {
        return stce_10040(stce_9841.stce_9938, stce_9841.stce_9939() * pval);
      }
      template<class stce_9935, class stce_9936 >
      static derivative_t stce_10040(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841, const derivative_t &pval) {
        return stce_10040(stce_9841.stce_9940, stce_9841.stce_9941() * pval);
      }
      typedef void tape_t;
      typedef DCO_TAPE_INT stce_10041;
      void *tape() const {
        return NULL;
      }
      DCO_TAPE_INT _tape_index() const {
        return 0;
      }
    };
  }
}
namespace dco {
  namespace internal {
  template<class stce_9776, typename stce_10042, typename stce_9883>
    struct stce_10043 {
      typedef stce_9883 mode_t;
      typedef stce_9946 stce_9988;
      typedef stce_10042 derivative_t;
      static const int stce_9835;
      derivative_t tlms;
      stce_10043() {
        for (int stce_9829 = 0; stce_9829 < stce_9835; ++stce_9829) tlms[stce_9829] = 0;
      }
      derivative_t &stce_9984() {
        return tlms;
      }
      const derivative_t &stce_9984() const {
        return tlms;
      }
      void stce_9985(const derivative_t &stce_9753, const int stce_10044) {
        stce_10029();;
        tlms[stce_10044] = stce_9753;
      }
      void stce_10039(derivative_t &stce_9753, const int stce_10044) const {
        stce_10029();;
        stce_9753 = tlms[stce_10044];
      }
      void clear() {
        for (int stce_9829 = 0; stce_9829 < stce_9835; ++stce_9829) {
          tlms[stce_9829] = 0.0;
        }
      }
      template<class stce_9884, class stce_9798>
      static void stce_9885(const stce_9884 &stce_9989, stce_9798 &stce_10045) {
        stce_10043 &data = const_cast<stce_10043 &>(stce_10045);
        for (int stce_9829 = 0; stce_9829 < stce_9835; ++stce_9829) {
          data.tlms[stce_9829] = stce_10040(stce_9989, 1.0, stce_9829);
        }
      }
      template<class stce_9932 >
      static stce_9776 stce_10040(const dco::internal::active_type<stce_9776, stce_9932> &stce_9841, const stce_9776 &pval, const int stce_10044) {
        return stce_9841.tlms[stce_10044] * pval;
      }
      template<class stce_9934, class stce_9935, class stce_9936 >
      static stce_9776 stce_10040(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841, const stce_9776 &pval, const int stce_10044) {
        return stce_10040(stce_9841.stce_9938, stce_9841.stce_9939() * pval, stce_10044) + stce_10040(stce_9841.stce_9940, stce_9841.stce_9941() * pval, stce_10044);
      }
      template<class stce_9942, class stce_9936 >
      static stce_9776 stce_10040(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841, const stce_9776 &pval, const int stce_10044) {
        return stce_10040(stce_9841.stce_9943, stce_9841.pval() * pval, stce_10044);
      }
      template<class stce_9934, class stce_9936 >
      static stce_9776 stce_10040(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841, const stce_9776 &pval, const int stce_10044) {
        return stce_10040(stce_9841.stce_9938, stce_9841.stce_9939() * pval, stce_10044);
      }
      template<class stce_9935, class stce_9936 >
      static stce_9776 stce_10040(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841, const stce_9776 &pval, const int stce_10044) {
        return stce_10040(stce_9841.stce_9940, stce_9841.stce_9941() * pval, stce_10044);
      }
      typedef void stce_10046;
      typedef DCO_TAPE_INT stce_10041;
      void *stce_9945() const {
        return NULL;
      }
      DCO_TAPE_INT _tape_index() const {
        return 0;
      }
    };
  template<class stce_9776, typename stce_10042, typename stce_9883>
    struct stce_10047 {
      typedef stce_9883 mode_t;
      typedef stce_9946 stce_9988;
      typedef stce_10042 derivative_t;
      static const int stce_9835 = derivative_t::stce_9825;
      bool stce_10048;
      derivative_t tlms;
      stce_10047() : stce_10048(false) {
        for (int stce_9829 = 0; stce_9829 < stce_9835; ++stce_9829) tlms[stce_9829] = 0;
      }
      void stce_9985(const stce_9776 &stce_9753, const int stce_10044) {
        if (!stce_10048) {
          stce_10048 = true;
        }
        tlms[stce_10044] = stce_9753;
      }
      derivative_t &stce_9984() {
        stce_10029();;
        stce_10048 = true;
        return tlms;
      }
      const derivative_t &stce_9984() const {
        stce_10029();;
        return tlms;
      }
      void stce_10039(stce_9776 &stce_9753, const int stce_10044) const {
        stce_10029();;
        if (stce_10048) {
          stce_9753 = tlms[stce_10044];
        } else {
          stce_9753 = 0;
        }
      }
      void clear() {
        for (int stce_9829 = 0; stce_9829 < stce_9835; ++stce_9829) {
          tlms[stce_9829] = 0.0;
        }
      }
      template<class stce_9884>
      __attribute__((always_inline)) inline void stce_9885(const stce_9884 &stce_9989) {
        bool stce_10049 = stce_10050(stce_9989);
        if (stce_10049) {
          for (int stce_9829 = 0; stce_9829 < stce_9835; ++stce_9829) {
            tlms[stce_9829] = stce_10040(stce_9989, 1.0, stce_9829);
          }
          stce_10048 = true;
        } else
          stce_10048 = false;
      }
      template<class stce_9932 >
      static stce_9776 stce_10040(const dco::internal::active_type<stce_9776, stce_9932> &stce_9841, const stce_9776 &pval, const int stce_10044) {
        if (stce_9841.stce_10048) {
          return stce_9841.tlms[stce_10044] * pval;
        } else
          return 0;
      }
      template<class stce_9934, class stce_9935, class stce_9936 >
      static stce_9776 stce_10040(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841, const stce_9776 &pval, const int stce_10044) {
        return stce_10040(stce_9841.stce_9938, stce_9841.stce_9939() * pval, stce_10044) + stce_10040(stce_9841.stce_9940, stce_9841.stce_9941() * pval, stce_10044);
      }
      template<class stce_9942, class stce_9936 >
      static stce_9776 stce_10040(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841, const stce_9776 &pval, const int stce_10044) {
        return stce_10040(stce_9841.stce_9943, stce_9841.pval() * pval, stce_10044);
      }
      template<class stce_9934, class stce_9936 >
      static stce_9776 stce_10040(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841, const stce_9776 &pval, const int stce_10044) {
        return stce_10040(stce_9841.stce_9938, stce_9841.stce_9939() * pval, stce_10044);
      }
      template<class stce_9935, class stce_9936 >
      static stce_9776 stce_10040(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841, const stce_9776 &pval, const int stce_10044) {
        return stce_10040(stce_9841.stce_9940, stce_9841.stce_9941() * pval, stce_10044);
      }
      template<class stce_9932 >
      static bool stce_10050(const dco::internal::active_type<stce_9776, stce_9932> &stce_9841) {
        return stce_9841.stce_10048;
      }
      template<class stce_9934, class stce_9935, class stce_9936 >
      static bool stce_10050(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841) {
        return stce_10050(stce_9841.stce_9938) || stce_10050(stce_9841.stce_9940);
      }
      template<class stce_9942, class stce_9936 >
      static bool stce_10050(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841) {
        return stce_10050(stce_9841.stce_9943);
      }
      template<class stce_9934, class stce_9936 >
      static bool stce_10050(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841) {
        return stce_10050(stce_9841.stce_9938);
      }
      template<class stce_9935, class stce_9936 >
      static bool stce_10050(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841) {
        return stce_10050(stce_9841.stce_9940);
      }
      typedef void stce_10046;
      typedef void tape_t;
      typedef DCO_TAPE_INT stce_10041;
      void *tape() const {
        return NULL;
      }
      DCO_TAPE_INT _tape_index() const {
        return 0;
      }
    };
  }
}
namespace dco {
  namespace internal {
  template<DCO_INTEGRAL_TAPE_INT size, class stce_9776>
    struct stce_10051 {
    typedef stce_9946 stce_9988;
    typedef std::bitset<size> stce_10052;
    typedef stce_9807 mode_t;
      stce_10052 stce_10053;
      stce_10051() {}
      inline stce_10051 &operator = (const stce_10051 &stce_10038) {
        stce_10053 = stce_10038.stce_10053;
        return *this;
      }
       void stce_9985(const size_t &stce_9830) {
        stce_10053.set(stce_9830);
      }
       bool stce_10039(const size_t &stce_9830) const {
        return stce_10053[stce_9830];
      }
       void clear() {
        stce_10053.reset();
      }
      template<class stce_9884>
      __attribute__((always_inline)) inline void stce_9885(const stce_9884 &stce_9989) {
        stce_10051 stce_10054;
        stce_10055(stce_9989, stce_10054.stce_10053);
        stce_10053 = stce_10054.stce_10053;
      }
      template<class stce_9932 >
      static void stce_10055(const dco::internal::active_type<stce_9776, stce_9932> &stce_9841, stce_10052 &stce_10045) {
        stce_10045 |= stce_9841.stce_10053;
      }
      template<class stce_9934, class stce_9935, class stce_9936 >
      static void stce_10055(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841, stce_10052 &stce_10045) {
        stce_10055(stce_9841.stce_9938, stce_10045);
        stce_10055(stce_9841.stce_9940, stce_10045);
      }
      template<class stce_9942, class stce_9936 >
      static void stce_10055(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841, stce_10052 &stce_10045) {
        stce_10055(stce_9841.stce_9943, stce_10045);
      }
      template<class stce_9934, class stce_9936 >
      static void stce_10055(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841, stce_10052 &stce_10045) {
        stce_10055(stce_9841.stce_9938, stce_10045);
      }
      template<class stce_9935, class stce_9936 >
      static void stce_10055(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841, stce_10052 &stce_10045) {
        stce_10055(stce_9841.stce_9940, stce_10045);
      }
    };
  }
}
       
       
namespace dco {
  namespace internal {
struct interpretation_settings {
  bool reset;
  bool stce_10056;
  interpretation_settings(bool stce_10057=false, bool stce_10058=false)
    : reset(stce_10057), stce_10056(stce_10058) {
  }
};
}
}
namespace dco {
    class tape_options {
    private:
      size_t stce_10059, stce_10060;
      bool stce_10061;
      std::string stce_10062;
      bool stce_10063;
    public:
      tape_options()
 : stce_10059(128*1024*1024),
          stce_10060(0),
   stce_10061(false),
   stce_10062("dco_tape_data.bin"),
   stce_10063(false) { }
      bool operator==(const tape_options& stce_9926) {
        return
          stce_10059 == stce_9926.stce_10059 &&
          stce_10060 == stce_9926.stce_10060 &&
          stce_10061 == stce_9926.stce_10061 &&
          stce_10063 == stce_9926.stce_10063 &&
          stce_10062 == stce_9926.stce_10062;
      }
      bool operator!=(const tape_options& stce_9926) { return !operator==(stce_9926); }
      DCO_TAPE_INT chunk_size() const {
        if (helper::stce_9817) {
          throw dco::exception::create<std::runtime_error>("chunk_size removed; use chunk_size_in_byte() instead");
        }
        return -1;
      }
      size_t chunk_size_in_byte() const { return stce_10059; }
      size_t& chunk_size_in_byte() { return stce_10059; }
      void set_chunk_size_in_byte (mem_long_t size) {
        if (static_cast<double>(size) / static_cast<double>(std::numeric_limits<size_t>::max()) > 1.0) {
          DCO_LOG(logWARNING) << "set_chunk_size: requesting too much memory for your system (size_t too small). Resetting to std::numeric_limits<size_t>::max()";
          stce_10059 = std::numeric_limits<size_t>::max();
        } else {
          stce_10059 = static_cast<size_t>(size);
        }
      }
      void set_chunk_size_in_kbyte(double size) { set_chunk_size_in_byte(1024*static_cast<mem_long_t>(size)); }
      void set_chunk_size_in_mbyte(double size) { set_chunk_size_in_kbyte(1024.*size); }
      void set_chunk_size_in_gbyte(double size) { set_chunk_size_in_mbyte(1024.*size); }
      size_t blob_size_in_byte() const { return stce_10060; }
      size_t& blob_size_in_byte() { return stce_10060; }
      void set_blob_size_in_byte (mem_long_t size) {
        if (static_cast<double>(size) / static_cast<double>(std::numeric_limits<size_t>::max()) > 1.0) {
          DCO_LOG(logWARNING) << "set_blob_size: requesting too much memory for your system (size_t too small). Resetting to std::numeric_limits<size_t>::max()";
          stce_10060 = std::numeric_limits<size_t>::max();
        } else {
          stce_10060 = static_cast<size_t>(size);
        }
      }
      void set_blob_size_in_kbyte(double size) { set_blob_size_in_byte(1024*static_cast<mem_long_t>(size)); }
      void set_blob_size_in_mbyte(double size) { set_blob_size_in_kbyte(1024.*size); }
      void set_blob_size_in_gbyte(double size) { set_blob_size_in_mbyte(1024.*size); }
      bool &deallocation_on_reset() { return stce_10061; }
      const bool &deallocation_on_reset() const { return stce_10061; }
      std::string &filename() { return stce_10062; }
      const std::string &filename() const { return stce_10062; }
      bool& write_to_file() { return stce_10063; }
      bool write_to_file() const { return stce_10063; }
    };
}
       
       
namespace dco {
template <typename stce_9786, typename stce_10064 = void> struct trait_size_of;
template<typename stce_9786> mem_long_t size_of(const stce_9786& stce_9841) {
  return trait_size_of<stce_9786>::get(stce_9841);
}
template<typename stce_9786> mem_long_t size_of(const stce_9786& stce_9841, int stce_10065) {
  return trait_size_of<stce_9786>::get(stce_9841, stce_10065);
}
template <typename stce_9786, typename stce_10064> struct trait_size_of {
  static mem_long_t get(const stce_9786&) {
    if (helper::stce_9817) {
      throw dco::exception::create<std::runtime_error>("You're trying to compute the size of the checkpoint, but a type ("
                                                       + type_name<stce_9786>()
                                                       + ") you checkpointed does not have a correct specialization:\n\n" +
                                                       "template <typename T> struct trait_size_of {\n" +
                                                       "   static mem_long_t get(const T&) { ... }\n" +
                                                       "};\n\n");
    }
    return 0;
  }
};
template<memory_model::stce_9770 MEMORY_MODEL, class stce_9792, memory_model::stce_9770 stce_9793>
  struct trait_size_of<dco::internal::tape<MEMORY_MODEL, stce_9792, stce_9793>*> {
  typedef dco::internal::tape<MEMORY_MODEL, stce_9792, stce_9793> tape_t;
  static mem_long_t get(const tape_t *tape,
                        const int stce_10065 = tape_t::stce_10066) {
  return tape->stce_10067(stce_10065);
  }
};
template<typename stce_9786> struct trait_size_of<std::vector<stce_9786> > {
  static mem_long_t get(const std::vector<stce_9786>& stce_9841) { return stce_9841.size() * sizeof(stce_9786) + sizeof(stce_9841); }
};
template<> struct trait_size_of<char> { static mem_long_t get(const char&) { return sizeof(char); } };
template<> struct trait_size_of<wchar_t> { static mem_long_t get(const wchar_t&) { return sizeof(wchar_t); } };
template<> struct trait_size_of<signed char> { static mem_long_t get(const signed char&) { return sizeof(signed char); } };
template<> struct trait_size_of<short> { static mem_long_t get(const short&) { return sizeof(short); } };
template<> struct trait_size_of<int> { static mem_long_t get(const int&) { return sizeof(int); } };
template<> struct trait_size_of<long> { static mem_long_t get(const long&) { return sizeof(long); } };
template<> struct trait_size_of<unsigned char> { static mem_long_t get(const unsigned char&) { return sizeof(unsigned char); } };
template<> struct trait_size_of<unsigned short> { static mem_long_t get(const unsigned short&) { return sizeof(unsigned short); } };
template<> struct trait_size_of<unsigned int> { static mem_long_t get(const unsigned int&) { return sizeof(unsigned int); } };
template<> struct trait_size_of<unsigned long> { static mem_long_t get(const unsigned long&) { return sizeof(unsigned long); } };
template<> struct trait_size_of<float> { static mem_long_t get(const float&) { return sizeof(float); } };
template<> struct trait_size_of<double> { static mem_long_t get(const double&) { return sizeof(double); } };
template<> struct trait_size_of<long double> { static mem_long_t get(const long double&) { return sizeof(long double); } };
template<> struct trait_size_of<bool> { static mem_long_t get(const bool&) { return sizeof(bool); } };
}
       
namespace dco {
namespace helper {
template <typename, typename stce_10068>
  struct stce_10069 {
  stce_10069(stce_10068*) {}
  template <typename stce_9786> void register_input(const stce_9786&) { }
  template <typename stce_9786> void register_output(const stce_9786&) { }
};
}
}
       
       
namespace dco {
template<class stce_9799>
  struct stce_10070 {
  virtual ~stce_10070(){};
  virtual stce_9799 stce_10071(const DCO_TAPE_INT stce_9830)=0;
  virtual void stce_10072(const DCO_TAPE_INT stce_9830, const stce_9799 &stce_10073)=0;
};
}
namespace dco {
namespace helper {
template<typename stce_10074, typename stce_10075>
  class stce_10076 {
  template<memory_model::stce_9770, typename, memory_model::stce_9770>
    friend class internal::tape;
protected:
  stce_10074 *stce_10077;
public:
  virtual ~stce_10076() {
  }
  dco::stce_10070<stce_10075> *stce_10078;
public:
  void set_tape(stce_10074 *stce_9897) {
    if (stce_10077 != 0) { return; }
    else { stce_10077 = stce_9897; }
  }
  inline stce_10074 *get_tape() { return stce_10077; }
  stce_10076() : stce_10077(0) {}
  virtual void stce_10079(stce_9846 &stce_9847, DCO_TAPE_INT stce_9855) {
    (void)stce_9847;
    (void)stce_9855;
  }
  virtual mem_long_t size_in_byte() {
    return sizeof(stce_10074);
  }
};
}
}
       
namespace dco {
namespace helper {
template<class stce_10080, class stce_10074>
  class stce_10081 : public stce_10076<stce_10074, typename stce_10074::stce_10082> {
private:
  class stce_10083 {
public:
    virtual ~stce_10083() {};
    virtual mem_long_t size_in_byte() = 0;
    virtual void stce_10084(const std::string&, const std::string&) {};
  };
  template <typename stce_10085>
    class stce_10086 : public stce_10083 {
public:
    const stce_10085 _data;
    stce_10086(const stce_10085 &stce_10087) : _data(stce_10087) {}
    const stce_10085 &stce_10088() const { return _data; }
  };
  template <typename stce_10085>
    class stce_10089 : public stce_10086<stce_10085> {
public:
    stce_10089(stce_10085 data) : stce_10086<stce_10085>(data) {}
    virtual ~stce_10089() { }
    virtual mem_long_t size_in_byte() {
      return size_of(stce_10086<stce_10085>::_data);
    }
    virtual void stce_10084(const std::string& stce_10090, const std::string& stce_10091) {
      if (false && stce_9755 && stce_10091 != stce_10090) {
        std::stringstream stce_10054;
        stce_10054 << "read_data error: tried to read " << stce_10090 << " instead of " << stce_10091 << "!";
        throw dco::exception::create<std::runtime_error>(stce_10054.str());
      }
    }
  };
  template <typename stce_10085>
    class stce_10092 : public stce_10086<stce_10085*> {
public:
    const DCO_TAPE_INT stce_10093;
    stce_10092(const stce_10085 *data, DCO_TAPE_INT stce_10094) : stce_10086<stce_10085*>(new stce_10085[size_t(stce_10094)]), stce_10093(stce_10094) {
      for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829)
        this->_data[stce_9829] = data[stce_9829];
    }
    stce_10092(const stce_10085 *data, const DCO_TAPE_INT stce_10095, const DCO_TAPE_INT stce_10094) :
      stce_10086<stce_10085 *>(new stce_10085[stce_9752(stce_10094)]), stce_10093(stce_10094) {
      for (DCO_TAPE_INT stce_9829 = 0, stce_9830 = 0; stce_9829 < stce_10094; ++stce_9829, stce_9830 += stce_10095)
        this->_data[stce_9829] = data[stce_9830];
    }
    virtual ~stce_10092() {
      delete [] this->_data;
    }
    virtual mem_long_t size_in_byte() {
      return stce_10093 * static_cast<DCO_TAPE_INT>(sizeof(stce_10085));
    }
    virtual void stce_10084(const std::string& stce_10090, const std::string& stce_10091) {
      (void) stce_10090; (void) stce_10091;
      if (false && stce_9755 && stce_10091+"*" != stce_10090) {
        std::stringstream stce_10054;
        stce_10054 << "read_data error: tried to read " << stce_10090 << " instead of " << stce_10091 << "!";
        throw dco::exception::create<std::runtime_error>(stce_10054.str());
      }
    }
  };
  DCO_TAPE_INT stce_10096;
  std::vector<stce_10083 *> stce_10097;
  std::vector<std::string> stce_10098;
protected:
  virtual ~stce_10081() {
    for (size_t stce_9829 = 0; stce_9829 < stce_10097.size(); stce_9829++)
      delete stce_10097[stce_9829];
    stce_10097.clear();
  }
public:
  stce_10081(): stce_10076<stce_10074, typename stce_10074::stce_10082>(), stce_10096(0) {}
  virtual mem_long_t size_in_byte() {
    mem_long_t stce_9912 = stce_10076<stce_10074, typename stce_10074::stce_10082>::size_in_byte();
    for (size_t stce_9829 = 0; stce_9829 < stce_10097.size(); stce_9829++) {
      stce_9912 += stce_10097[stce_9829]->size_in_byte();
    }
    stce_9912 += stce_10097.size() * sizeof(stce_10083*);
    stce_9912 += stce_10098.size() * sizeof(std::string);
    return stce_9912;
  }
  template<typename stce_10085>
    void write_data(const stce_10085 &stce_10099) {
    stce_10097.push_back(new stce_10089<stce_10085>(stce_10099));
    if (stce_9755)
      stce_10098.push_back(type_name<stce_10085>());
  }
  template<typename stce_10085>
    void write_data(const stce_10085 *const stce_10099, const DCO_TAPE_INT stce_10094) {
    stce_10097.push_back(new stce_10092<stce_10085>(stce_10099, stce_10094));
    if (stce_9755)
      stce_10098.push_back(type_name<stce_10085>());
  }
  template<typename stce_10085>
    void write_data(const stce_10085 *const &stce_10099, const DCO_TAPE_INT stce_10095, const DCO_TAPE_INT stce_10094) {
    stce_10097.push_back(new stce_10092<stce_10085>(stce_10099, stce_10095, stce_10094));
    if (stce_9755) {
      stce_10098.push_back(type_name<stce_10085>());
    }
  }
  template<typename stce_10085>
    const stce_10085 &read_data() {
    const stce_10085 &stce_10099 = static_cast<stce_10086<stce_10085>* >(stce_10097[stce_10096])->stce_10088();
    if (stce_9755)
      stce_10097[stce_10096]->stce_10084(type_name<stce_10085>(), stce_10098.at(stce_10096));
    ++stce_10096;
    if (stce_10096 == static_cast<DCO_TAPE_INT>(stce_10097.size())) stce_10096 = 0;
    return stce_10099;
  }
};
}
}
       
namespace dco {
namespace helper {
template<class stce_10080, class stce_10074>
  class stce_10100 :
           public stce_10081<stce_10080, stce_10074>,
           public stce_10069<stce_10074, stce_10100<stce_10080, stce_10074> > {
  typedef stce_10069<stce_10074, stce_10100> stce_10101;
protected:
  std::vector<DCO_TAPE_INT> stce_9853;
  std::vector<DCO_TAPE_INT> stce_9854;
  DCO_TAPE_INT stce_10102;
  DCO_TAPE_INT stce_10103;
public:
  stce_10074* &tape() { return this->stce_10077; }
  virtual mem_long_t size_in_byte() {
    mem_long_t stce_9912 = stce_10081<stce_10080, stce_10074>::size_in_byte();
    stce_9912 += stce_9853.size() * sizeof(DCO_TAPE_INT);
    stce_9912 += stce_9854.size() * sizeof(DCO_TAPE_INT);
    stce_9912 += 2 * sizeof(DCO_TAPE_INT);
    return stce_9912;
  }
  size_t stce_10104() {
    return stce_9853.size();
  }
  size_t stce_10105() {
    return stce_9854.size();
  }
  void stce_10079(stce_9846 &stce_9847, DCO_TAPE_INT stce_9855) {
    stce_9847.stce_9852(stce_9853, stce_9854, stce_9855 - static_cast<DCO_TAPE_INT>(stce_9854.size()) - 1);
  }
public:
  void stce_10106(const stce_10080 &stce_9841) {
    if ((stce_9841.tape() != 0) && (this->stce_10077 != stce_9841.tape()))
      throw dco::exception::create<std::runtime_error>("impossible binding tape - wrong tape in variable!");
  }
protected:
  ~stce_10100() {
    if (stce_9755 && stce_10102 != 0) {
      DCO_LOG(dco::logERROR) << "not all input adjoints incremented in external adjoint object.";
    }
    if (stce_9755 && stce_10103 != 0) {
      DCO_LOG(dco::logERROR) << "not all output adjoints read in external adjoint object.";
    }
  }
public:
  stce_10100(const std::pair<DCO_TAPE_INT, DCO_TAPE_INT> &stce_10107):
    stce_10081<stce_10080, stce_10074>(),
    stce_10101(this),
    stce_10102(0),
    stce_10103(0) {
    stce_9853.reserve(stce_10107.first);
    stce_9854.reserve(stce_10107.second);
  }
  stce_10100(): stce_10081<stce_10080, stce_10074>(),
    stce_10101(this),
    stce_10102(0), stce_10103(0) { }
  typename stce_10080::VALUE_TYPE register_input(const stce_10080 &stce_9841) {
    stce_10106(stce_9841);
    const DCO_TAPE_INT stce_9830 = stce_9841._tape_index();
    stce_9853.push_back(stce_9830);
    stce_10101::register_input(stce_9841);
    return stce_9841._value();
  }
  void register_input(const stce_10080 *const stce_9841, typename stce_10080::VALUE_TYPE *stce_10108, const DCO_TAPE_INT stce_10094) {
    stce_9853.reserve(stce_9853.size() + stce_10094);
    for (int stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
      stce_10106(stce_9841[stce_9829]);
      const DCO_TAPE_INT stce_9830 = stce_9841[stce_9829]._tape_index();
      stce_9853.push_back(stce_9830);
      stce_10108[stce_9829] = stce_9841[stce_9829]._value();
      stce_10101::register_input(stce_9841[stce_9829]);
    }
  }
  void register_input(const std::vector<stce_10080> &stce_9841, std::vector<typename stce_10080::VALUE_TYPE> &stce_10108) {
    assert(stce_9841.size() == stce_10108.size());
    register_input(&(stce_9841[0]), &(stce_10108[0]), stce_9841.size());
  }
  std::vector<typename stce_10080::VALUE_TYPE> register_input(const std::vector<stce_10080> &stce_9841) {
    std::vector<typename stce_10080::VALUE_TYPE> stce_10108(stce_9841.size());
    register_input(stce_9841, stce_10108);
    return stce_10108;
  }
  void register_output(stce_10080 *stce_10109, const size_t stce_10094) {
    if (this->stce_10077 == NULL) {
      throw dco::exception::create<std::runtime_error>("impossible binding output - no tape available");
    } else {
      register_output(NULL, stce_10109, stce_10094);
    }
  }
  void register_output(const typename stce_10080::VALUE_TYPE *const stce_10110, stce_10080 *stce_10109, const size_t stce_10094) {
    if (this->stce_10077 == NULL) {
      throw dco::exception::create<std::runtime_error>("impossible binding output - no tape available");
    } else {
      stce_9854.reserve(stce_9854.size() + stce_10094);
      for (size_t stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
        if (stce_10110) {
          stce_10109[stce_9829] = stce_10110[stce_9829];
        }
        this->stce_10077->register_variable(stce_10109[stce_9829]);
        stce_9854.push_back(stce_10109[stce_9829]._tape_index());
        stce_10101::register_output(stce_10109[stce_9829]);
      }
    }
  }
  void register_output(const std::vector<typename stce_10080::VALUE_TYPE> &stce_10110, std::vector<stce_10080> &stce_10109) {
    assert(stce_10110.size() == stce_10109.size());
    register_output(&(stce_10110[0]), &(stce_10109[0]), stce_10110.size());
  }
  std::vector<stce_10080> register_output(const std::vector<typename stce_10080::VALUE_TYPE> &stce_10110) {
    std::vector<stce_10080> stce_10109(stce_10110.size());
    register_output(stce_10110, stce_10109);
    return stce_10109;
  }
  void register_output(std::vector<stce_10080> &stce_10109) {
    register_output(&(stce_10109[0]), stce_10109.size());
  }
  stce_10080 register_output(const typename stce_10080::VALUE_TYPE &stce_10111, stce_10074 *tape = NULL) {
    stce_10080 stce_10112;
    if (tape != NULL) {
      if (this->stce_10077 != NULL && this->stce_10077 != tape) {
        throw dco::exception::create<std::runtime_error>("impossible binding output in external function (register_output) - tape of inputs and outputs differ!");
      }
      this->stce_10077 = tape;
    }
    if (this->stce_10077 != NULL) {
      stce_10112 = stce_10111;
      this->stce_10077->register_variable(stce_10112);
    } else
      throw dco::exception::create<std::runtime_error>("impossible binding output in external function - no tape available");
    stce_9854.push_back(stce_10112._tape_index());
    stce_10101::register_output(stce_10112);
    return stce_10112;
  }
  inline typename stce_10074::stce_10082 get_output_adjoint() {
    DCO_TAPE_INT stce_9830 = stce_10103;
    stce_10103++;
    if (stce_10103 == static_cast<DCO_TAPE_INT>(stce_9854.size()))
      stce_10103 = 0;
    typename stce_10074::stce_10082 back = this->stce_10078->stce_10071(stce_9854[static_cast<size_t>(stce_9830)]);
    return back;
  }
  void get_output_adjoint(typename stce_10080::VALUE_TYPE *stce_9882, const size_t stce_10094) {
    DCO_TAPE_INT stce_9830 = stce_10103;
    for (size_t stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
      stce_9882[stce_9829] = get_output_adjoint();
      stce_9830++;
    }
    if (stce_10103 == static_cast<DCO_TAPE_INT>(stce_9854.size())) {
      stce_10103 = 0;
    }
  }
  void get_output_adjoint(std::vector<typename stce_10080::VALUE_TYPE> &stce_9882) {
    assert(stce_9882.size());
    get_output_adjoint(&(stce_9882[0]), stce_9882.size());
  }
  void increment_input_adjoint(const typename stce_10080::VALUE_TYPE *const stce_10073, const DCO_TAPE_INT stce_10094) {
    for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
      this->stce_10078->stce_10072(stce_9853[stce_10102 + stce_9829], stce_10073[stce_9829]);
    }
    stce_10102 += stce_10094;
    if (stce_10102 == static_cast<DCO_TAPE_INT>(stce_9853.size()))
      stce_10102 = 0;
  }
  void increment_input_adjoint(const std::vector<typename stce_10080::VALUE_TYPE> &stce_10073) {
    assert(stce_10073.size() != 0);
    increment_input_adjoint(&(stce_10073[0]), static_cast<DCO_TAPE_INT>(stce_10073.size()));
  }
  bool stce_10113() {
    if (stce_10102 == 0) return true;
    else return false;
  }
  bool stce_10114() {
    if (stce_10103 == 0) return true;
    else return false;
  }
  void increment_input_adjoint(const typename stce_10074::stce_10082 &stce_10073) {
    DCO_TAPE_INT stce_9830 = stce_10102;
    stce_10102++;
    if (stce_10102 == static_cast<DCO_TAPE_INT>(stce_9853.size()))
      stce_10102 = 0;
    this->stce_10078->stce_10072(stce_9853[static_cast<size_t>(stce_9830)], stce_10073);
  }
};
}
}
       
namespace dco {
namespace ACM {
template<class stce_10074, class stce_9975>
  struct baseclass : dco::helper::stce_10076<stce_10074, typename stce_10074::stce_10082> {
private:
  std::vector<stce_9975*> stce_10115;
  std::vector<stce_9975*> stce_10116;
  std::vector<DCO_TAPE_INT> stce_10117;
  std::vector<DCO_TAPE_INT> stce_10118;
  std::vector<void*> _data;
  int stce_10119;
public:
  typedef stce_9975 stce_10120;
  typedef stce_10074 tape_t;
  baseclass(tape_t *tape) : stce_10119(0) {
    this->set_tape(tape);
  }
  size_t register_input(stce_9975 &stce_9841) {
    size_t stce_9837 = stce_10115.size();
    stce_10115.push_back(&stce_9841);
    stce_10118.push_back(stce_9841._tape_index());
    return stce_9837;
  }
  size_t register_input(std::vector<stce_9975> &stce_10121) {
    size_t stce_9837 = stce_10115.size();
    for(int stce_9829=0;stce_9829<stce_10121.size();++stce_9829) {
      const stce_9975 &stce_10122 = stce_10121[stce_9829];
      this->register_input(stce_10122);
    }
    return stce_9837;
  }
  size_t register_output(stce_9975 &stce_9841) {
    if(stce_9841._tape_index()==0) {
      this->get_tape()->register_variable(stce_9841);
    }
    size_t stce_9837 = stce_10116.size();
    stce_10116.push_back(&stce_9841);
    stce_10117.push_back(stce_9841._tape_index());
    return stce_9837;
  }
  int register_output(std::vector<stce_9975> &stce_10121) {
    int stce_9837 = stce_10116.size();
    for(int stce_9829=0;stce_9829<stce_10121.size();++stce_9829) {
      if(stce_10121[stce_9829]._tape_index()==0) {
        this->get_tape()->register_variable(stce_10121[stce_9829]);
      }
      stce_10117.push_back(stce_10121[stce_9829]._tape_index());
      stce_10116.push_back(&stce_10121[stce_9829]);
    }
    return stce_9837;
  }
  template<typename stce_9786> int write_data(const stce_9786 &data) {
    stce_9786* stce_10123 = new stce_9786(data);
    int stce_9837 = _data.size();
    _data.push_back(stce_10123);
    return stce_9837;
  }
  template<typename stce_9786> const stce_9786& read_data(const int stce_10124=-1) {
    int stce_9830=0;
    if(stce_10124==-1) {
      stce_9830 = stce_10119%_data.size(); stce_10119++;
    }
    void* stce_10125 = _data[stce_9830];
    return *static_cast<stce_9786*>(stce_10125);
  }
  size_t stce_10126() {
    return stce_10115.size();
  }
  size_t stce_10127() {
    return stce_10115.size();
  }
  typename stce_9975::VALUE_TYPE& input_value(const int stce_9830) {
    return stce_10115[stce_9830]->_value();
  }
  typename stce_9975::VALUE_TYPE& output_value(const int stce_9830) {
    return stce_10116[stce_9830]->_value();
  }
  typename stce_10074::stce_10082& input_adjoint(const int stce_9830) {
    return this->get_tape()->_adjoint( stce_10118[stce_9830] );
  }
  typename stce_10074::stce_10082& output_adjoint(const int stce_9830) {
    return this->get_tape()->_adjoint( stce_10117[stce_9830] );
  }
  virtual void evaluate_primal() {
    std::cout << "WARNING: you need to implement evaluate_primal!!!!\n";
  }
  virtual void evaluate_augmented_primal() {
    std::cout << "WARNING: you need to implement evaluate_augmented_primal!!!!\n";
  }
  virtual void evaluate_adjoint() {
    std::cout << "WARNING: you need to implement evaluate_adjoint!!!!\n";
  }
};
template<class stce_10074, class stce_9975>
  static void stce_10128( baseclass<stce_10074, stce_9975> *stce_10129 ) {
  stce_10129->evaluate_adjoint();
}
}
}
namespace dco {
namespace stce_10130 {
    template<class stce_9986>
    struct stce_10131 {
        std::vector<stce_9986*> stce_10115;
        std::vector<stce_9986*> stce_10116;
        std::vector<void*> _data;
        int stce_10119;
        stce_10131() : stce_10119(0) {}
        virtual ~stce_10131() {}
        template<typename stce_9786> int write_data(const stce_9786 &data) {
            stce_9786* stce_10123 = new stce_9786(data);
            int stce_9837 = _data.size();
            _data.push_back(stce_10123);
            return stce_9837;
        }
        template<typename stce_9786> const stce_9786& read_data(const int stce_10124=-1) {
            int stce_9830=0;
            if(stce_10124==-1) {
                stce_9830 = stce_10119%_data.size(); stce_10119++;
            }
            void* stce_10125 = _data[stce_9830];
            return *static_cast<stce_9786*>(stce_10125);
        }
        size_t stce_10126() {
            return stce_10115.size();
        }
        size_t stce_10127() {
            return stce_10116.size();
        }
        stce_9986& input_value(const int stce_9830) {
            return *stce_10115[stce_9830];
        }
        stce_9986& output_value(const int stce_9830) {
            return *stce_10116[stce_9830];
        }
        virtual void evaluate_primal() {
          std::cout << "WARNING: you need to implement evaluate_primal!!!!\n";
        }
    };
    template<class stce_9986>
    struct stce_10132 : stce_10131<stce_9986> {
        int register_input(stce_9986 &stce_9841) {
            int stce_9837 = this->stce_10115.size();
            this->stce_10115.push_back(&stce_9841);
            return stce_9837;
        }
        int register_input(std::vector<stce_9986> &stce_10121) {
            int stce_9837 = this->stce_10115.size();
            for(int stce_9829=0;stce_9829<stce_10121.size();++stce_9829) {
                const stce_9986 &stce_10122 = stce_10121[stce_9829];
                this->register_input(stce_10122);
            }
            return stce_9837;
        }
        int register_output(stce_9986 &stce_9841) {
            int stce_9837 = this->stce_10116.size();
            this->stce_10116.push_back(&stce_9841);
            return stce_9837;
        }
        int register_output(std::vector<stce_9986> &stce_10121) {
            int stce_9837 = this->stce_10116.size();
            for(int stce_9829=0;stce_9829<stce_10121.size();++stce_9829) {
                register_output(stce_10121[stce_9829]);
            }
            return stce_9837;
        }
    };
    template<class stce_10074, class stce_9975>
    struct stce_10133 : public dco::helper::stce_10076<stce_10074, typename stce_10074::stce_10082>, public stce_10131<stce_9975> {
    private:
        std::vector<DCO_TAPE_INT> stce_10117;
        std::vector<DCO_TAPE_INT> stce_10118;
    public:
        typedef stce_9975 stce_10120;
        typedef stce_10074 tape_t;
        stce_10133(tape_t *tape) {
            this->set_tape(tape);
        }
        stce_9975& stce_10134(const int stce_9830) {
            return *(this->stce_10115[stce_9830]);
        }
        stce_9975& stce_10135(const int stce_9830) {
            return *(this->stce_10116[stce_9830]);
        }
        typename stce_9975::VALUE_TYPE& input_value(const int stce_9830) {
            return this->stce_10115[stce_9830]->_value();
        }
        typename stce_9975::VALUE_TYPE& output_value(const int stce_9830) {
            return this->stce_10116[stce_9830]->_value();
        }
        typename stce_10074::stce_10082& input_adjoint(const int stce_9830) {
            return this->get_tape()->_adjoint( stce_10118[stce_9830] );
        }
        typename stce_10074::stce_10082& output_adjoint(const int stce_9830) {
            return this->get_tape()->_adjoint( stce_10117[stce_9830] );
        }
        int register_input(stce_9975 &stce_9841) {
            int stce_9837 = this->stce_10115.size();
            this->stce_10115.push_back(&stce_9841);
            this->stce_10118.push_back(stce_9841._tape_index());
            return stce_9837;
        }
        int register_input(std::vector<stce_9975> &stce_10121) {
            int stce_9837 = this->stce_10115.size();
            for(int stce_9829=0;stce_9829<stce_10121.size();++stce_9829) {
                const stce_9975 &stce_10122 = stce_10121[stce_9829];
                this->register_input(stce_10122);
            }
            return stce_9837;
        }
        int register_output(stce_9975 &stce_9841) {
            if(stce_9841._tape_index()==0) {
                this->get_tape()->register_variable(stce_9841);
            }
            int stce_9837 = this->stce_10116.size();
            this->stce_10116.push_back(&stce_9841);
            this->stce_10117.push_back(stce_9841._tape_index());
            return stce_9837;
        }
        int register_output(std::vector<stce_9975> &stce_10121) {
            int stce_9837 = this->stce_10116.size();
            for(int stce_9829=0;stce_9829<stce_10121.size();++stce_9829) {
                if(stce_10121[stce_9829]._tape_index()==0) {
                    this->get_tape()->register_variable(stce_10121[stce_9829]);
                }
                this->stce_10117.push_back(stce_10121[stce_9829]._tape_index());
                this->stce_10116.push_back(&stce_10121[stce_9829]);
            }
            return stce_9837;
        }
        virtual void evaluate_augmented_primal() {
            std::cout << "WARNING: you need to implement evaluate_augmented_primal!!!!\n";
        }
        virtual void evaluate_adjoint() {
            std::cout << "WARNING: you need to implement evaluate_adjoint!!!!\n";
        }
    };
    template<class stce_10074, class stce_9975>
    static void stce_10128( stce_10133<stce_10074, stce_9975> *stce_10129 ) {
        stce_10129->evaluate_adjoint();
    }
}
}
       
       
       
namespace dco {
namespace internal {
template <typename stce_10136>
  class stce_10137 {
protected:
  stce_10136& stce_10138;
public:
  stce_10137(stce_10136& stce_10139) : stce_10138(stce_10139) { }
  stce_10136& stce_10139() { return stce_10138; }
};
}
}
namespace dco {
namespace internal {
template<memory_model::stce_9770 memory_model,
         typename stce_10140,
         memory_model::stce_9770 stce_10141=memory_model::stce_9771> class stce_10142 {};
}
}
namespace dco {
namespace internal {
template<class stce_10140, memory_model::stce_9770 stce_10143>
  class stce_10142<memory_model::BLOB_TAPE_SPLINT, stce_10140, stce_10143>
  : public stce_10137<stce_10140>, object_logger
{
  typedef stce_10137<stce_10140> stce_10144;
  typedef typename stce_9794<stce_10140>::type stce_9766;
public:
  struct entry {
    stce_9766* stce_10145;
    DCO_TAPE_INT* stce_10146;
    __attribute__((always_inline)) inline entry(stce_9766* stce_10147,
                            DCO_TAPE_INT* stce_10148)
      : stce_10145(stce_10147),
        stce_10146(stce_10148) {};
    __attribute__((always_inline)) inline DCO_TAPE_INT arg() {
      return (*stce_10146 < 0) ? std::abs(stce_9752(*stce_10146))-1 : stce_9752(*stce_10146);
    }
    __attribute__((always_inline)) inline stce_9766& pval() { return *stce_10145; }
    __attribute__((always_inline)) inline stce_9766 pval() const { return *stce_10145; }
    __attribute__((always_inline)) inline bool stce_10149() { return *stce_10146 < 0; }
    __attribute__((always_inline)) inline static bool stce_10149(DCO_TAPE_INT* stce_10150) { return *stce_10150 < 0; }
    __attribute__((always_inline)) inline static DCO_TAPE_INT stce_10151(DCO_TAPE_INT stce_10045) { return -stce_10045-1; }
    static size_t size_of() { return sizeof(stce_9766) + sizeof(DCO_TAPE_INT); }
  };
  class iterator {
public:
    DCO_TAPE_INT stce_10152;
    entry stce_10153;
    template<class stce_10154>
      void stce_10155(stce_10154 &stce_10156) const {
      assert(sizeof(iterator) <= sizeof(stce_10154)) ;
      stce_10157(&stce_10156, this, sizeof(iterator));
    }
    template<class stce_10154>
      void stce_10158(const stce_10154 &stce_10156) {
      assert(sizeof(iterator) <= sizeof(stce_10154)) ;
      stce_10157(static_cast<void*>(this), &stce_10156, sizeof(iterator));
    }
    __attribute__((always_inline)) inline const entry* operator->() const { return &stce_10153; }
    __attribute__((always_inline)) inline entry* operator->() { return &stce_10153; }
    __attribute__((always_inline)) inline void stce_10159() {
      stce_10153.stce_10145++;
      stce_10153.stce_10146--;
      if (stce_9754 && static_cast<void*>(stce_10153.stce_10145) >= static_cast<void*>(stce_10153.stce_10146)) {
        throw dco::exception::create<std::runtime_error>("Blob tape (splint) container overflow. Allocate bigger blob or use chunk tape container instead.");
      }
    }
    __attribute__((always_inline)) inline DCO_TAPE_INT stce_10160() { return ++stce_10152; }
    __attribute__((always_inline)) inline explicit iterator() : stce_10152(-1), stce_10153(NULL, NULL) { }
    __attribute__((always_inline)) inline explicit iterator(stce_9766* stce_10147, DCO_TAPE_INT* stce_10148, DCO_TAPE_INT index = DCO_TAPE_INT(-1)) :
      stce_10152(index), stce_10153(stce_10147, stce_10148) { }
    __attribute__((always_inline)) inline DCO_TAPE_INT index() const { return stce_10152; }
    __attribute__((always_inline)) inline DCO_TAPE_INT& index() { return stce_10152; }
    bool stce_10149() const {
      return entry::stce_10149(stce_10153.stce_10146);
    }
    __attribute__((always_inline)) inline void operator--() {
      if(entry::stce_10149(stce_10153.stce_10146))
        stce_10152--;
      stce_10153.stce_10145--;
      stce_10153.stce_10146++;
    }
    __attribute__((always_inline)) inline iterator operator--(int) {
      iterator stce_10054(*this);
      operator--();
      return stce_10054;
    }
    __attribute__((always_inline)) inline bool operator==(const iterator& stce_9926) const {
      return stce_10153.stce_10145 == stce_9926.stce_10153.stce_10145;
    }
    __attribute__((always_inline)) inline bool operator!=(const iterator& stce_9926) const {
      return !operator==(stce_9926);
    }
    __attribute__((always_inline)) inline bool operator<(const iterator& stce_9926) const {
      return stce_10153.stce_10145 < stce_9926.stce_10153.stce_10145;
    }
    __attribute__((always_inline)) inline bool operator>=(const iterator& stce_9926) const {
      return !operator<(stce_9926);
    }
    __attribute__((always_inline)) inline bool operator>(const iterator& stce_9926) const {
      return stce_10153.stce_10145 > stce_9926.stce_10153.stce_10145;
    }
    __attribute__((always_inline)) inline bool operator<=(const iterator& stce_9926) const {
      return !operator>(stce_9926);
    }
  };
  class stce_9917
  {
    iterator& stce_10161;
    DCO_TAPE_INT stce_10152;
public:
    stce_9917(iterator& current) :
      stce_10161(current), stce_10152(0){
    }
    iterator& current() { return stce_10161; }
    stce_9917& operator=(stce_9917 stce_9926) {
      swap(stce_9926);
      return *this;
    }
    DCO_TAPE_INT index() const {
      return stce_10152;
    }
    void swap(stce_9917& stce_9926)
    {
      std::swap(stce_10161, stce_9926.stce_10161);
      std::swap(stce_10152, stce_9926.stce_10152);
    }
    __attribute__((always_inline)) inline void insert(const DCO_TAPE_INT& stce_10045, const stce_9766& pval) {
      if(stce_10152 == 0) {
        stce_10161.stce_10159();
        new (stce_10161.stce_10153.stce_10145) stce_9766(pval);
        new (stce_10161.stce_10153.stce_10146) DCO_TAPE_INT(entry::stce_10151(stce_10045));
        stce_10152 = stce_10161.stce_10160();
        DCO_LOG(logDEBUG2) << "inserting first entry, index now = " << stce_10152 << "; pval = " << pval << ", target = " << stce_10045;
        return;
      }
      if(stce_9758 && stce_10045 == *(stce_10161.stce_10153.stce_10146))
        {
          *(stce_10161.stce_10153.stce_10145) += pval;
          return;
        }
      stce_10161.stce_10159();
      new (stce_10161.stce_10153.stce_10145) stce_9766(pval);
      new (stce_10161.stce_10153.stce_10146) DCO_TAPE_INT(stce_10045);
    }
private:
    stce_9917(const stce_9917& stce_9926);
  };
protected:
  void init(tape_options const& stce_10162, DCO_TAPE_INT stce_10163 = 0) {
    size_t stce_10164;
    if(stce_10162.write_to_file())
      stce_10164 = stce_10162.blob_size_in_byte();
    else
      stce_10164 = get_allocation_size(stce_10162.blob_size_in_byte());
    size_t stce_10165 = stce_10164 / entry::size_of();
    DCO_LOG(logDEBUG1) << "BLOB SPLINT container: user requested allocation size = " << stce_10164
                       << "; size of element = " << entry::size_of()
                       << "; max elements in memory = " << stce_10165;
    if (static_cast<double>(stce_10165) / static_cast<double>(std::numeric_limits<DCO_INTEGRAL_TAPE_INT>::max()) > 1.0) {
      stce_10165 = std::numeric_limits<DCO_TAPE_INT>::max();
      DCO_LOG(logDEBUG1) << "BLOB SPLINT container: too many elements requested for currently used DCO_TAPE_INT. Setting to: " << stce_10165 * 1.e-6 << "M elements";
    }
    DCO_TAPE_INT stce_10166 = static_cast<DCO_TAPE_INT>(stce_10165);
    size_t stce_10167 = stce_10166 * entry::size_of();
    DCO_LOG(logDEBUG) << "BLOB SPLINT container: trying allocation for = " << stce_10167 / 1024 << "K ~=~ " << stce_9752(stce_10166) * 1.e-6 << "M elements";
    stce_10138 = stce_10031(&stce_10167);
    if (!stce_10138) {
      throw dco::exception::stce_9913<std::bad_alloc>
        ("Could not allocate memory. Use environment variables (DCO_MEM_RATIO, DCO_MAX_ALLOCATION) and see documentation.");
    }
    stce_10166 = static_cast<DCO_TAPE_INT>(stce_10167 / entry::size_of());
    DCO_LOG(logINFO) << "BLOB SPLINT container: actually allocated size = " << stce_10167 / 1024 << "K ~=~ " << stce_9752(stce_10166) * 1.e-6 << "M elements";
    stce_10168 = static_cast<stce_9766*>(stce_10138);
    stce_10169 = static_cast<DCO_TAPE_INT*>(stce_10138) + stce_10167 / sizeof(DCO_TAPE_INT) - 1;
    stce_10161 = iterator(stce_10168, stce_10169, stce_10163);
    stce_10170 = iterator(stce_10168, stce_10169, stce_10163);
    stce_10171 = iterator(stce_10168 + stce_9752(stce_10166) - 1, stce_10169 - stce_9752(stce_10166) + 1, std::numeric_limits<DCO_TAPE_INT>::max());
    new (stce_10161.stce_10153.stce_10145) stce_9766(0.0);
    new (stce_10161.stce_10153.stce_10146) DCO_TAPE_INT(entry::stce_10151(0));
  }
public:
  explicit stce_10142(tape_options const& stce_10162, stce_10140 &tape) :
    stce_10144(tape),
    object_logger("BLOB SPLINT container"),
    stce_10138(0), stce_10168(0), stce_10169(0),
    stce_10161(), stce_10170(), stce_10171() {
    assert(stce_10143!=memory_model::stce_9775);
    init(stce_10162);
  }
  explicit stce_10142(iterator& stce_9815, stce_10140 &tape) :
    stce_10144(tape),
    object_logger("BLOB SPLINT container"),
    stce_10138(0), stce_10168(0), stce_10169(0),
    stce_10161(stce_9815), stce_10170(), stce_10171() {
    assert(stce_10143==memory_model::stce_9775);
  }
  ~stce_10142() {
    stce_10035(stce_10138);
  }
  __attribute__((always_inline)) inline iterator& stce_9921() { return stce_10161; }
  iterator start() { return stce_10170; }
  iterator start() const { return stce_10170; }
  iterator rend() { return start(); }
  iterator current() const { return stce_10161; }
  iterator current() { return stce_10161; }
  iterator rbegin() const { return current(); }
  iterator capacity() const { return stce_10171; }
  bool empty() const { return stce_10161 == start(); }
  DCO_TAPE_INT size(const iterator& stce_9860, const iterator& stce_9861) const {
    if(stce_9860 < stce_9861)
      throw dco::exception::create<std::runtime_error>("size from < to");
    return static_cast<long>(stce_9860->stce_10145 - stce_9861->stce_10145);
  }
  DCO_TAPE_INT size(iterator stce_9860) const {
    return size(stce_9860, start());
  }
  DCO_TAPE_INT size() const {
    return size(current(), start());
  }
  DCO_TAPE_INT stce_10172() const {
    return size(stce_10171, start());
  }
  size_t size_in_byte() const { return size(current(), start())*entry::size_of(); }
  size_t stce_10173() const { return size(stce_10171 , start())*entry::size_of(); }
  void erase(const iterator& stce_10150) {
    if(!stce_10168)
      return;
    if(stce_10150 > stce_10171)
      throw dco::exception::create<std::runtime_error>("pos behind stack!");
    if(stce_10150 > stce_10161)
      throw dco::exception::create<std::runtime_error>("reset to position out of tape!");
    stce_10161 = stce_10150;
  }
  void stce_10174(const iterator&) { }
  void stce_10175 ( ) { }
  DCO_TAPE_INT stce_10176() const { return stce_10161.index()+1; }
private:
  stce_10142(const stce_10142& stce_9926);
  void *stce_10138;
  stce_9766 * stce_10168;
  DCO_TAPE_INT * stce_10169;
protected:
  typename stce_9816
    <iterator, stce_10143==memory_model::stce_9775>::type stce_10161;
  iterator stce_10170;
  iterator stce_10171;
};
}
}
       
       
namespace dco {
namespace internal {
template<typename stce_10140, memory_model::stce_9770 stce_10143>
  class stce_10142<memory_model::BLOB_TAPE, stce_10140, stce_10143> :
    public stce_10137<stce_10140>, public object_logger {
  typedef stce_10137<stce_10140> stce_10144;
  typedef stce_10140 tape_t;
  typedef typename stce_9794<stce_10140>::type stce_9766;
public:
  enum stce_10177 {
    stce_10178,
    stce_10179
  };
      struct entry {
        DCO_TAPE_INT stce_9943;
        stce_9766 stce_9919;
      public:
        static size_t size_of() {
          return sizeof(entry);
        }
        entry(DCO_TAPE_INT arg = 0, stce_9766 pval = 0.) :
          stce_9943(arg), stce_9919(pval) {
        }
        inline DCO_TAPE_INT arg() const {
          return (stce_9943 < 0)?std::abs(stce_9752(stce_9943)) -1:stce_9752(stce_9943);
        }
        inline static DCO_TAPE_INT stce_10151(const DCO_TAPE_INT& arg) {
          return -arg-1;
        }
        inline bool stce_10149() const {
          return stce_9943 < 0;
        }
        inline void stce_10180(const stce_9766& stce_10095) {
          stce_9919 += stce_10095;
        }
        inline stce_9766 pval() const
        {
          return stce_9919;
        }
      }
      ;
      class iterator {
        entry* stce_10181;
        DCO_TAPE_INT stce_10152;
        entry const* stce_10182;
      public:
        void stce_10183(DCO_TAPE_INT stce_9830)
        {
          stce_10152 = stce_9830;
        }
        void stce_10159() {
          stce_10181++;
          if (stce_9754 && stce_10181 > stce_10182) {
            throw dco::exception::create<std::runtime_error>("Blob tape container overflow. Allocate bigger blob or use chunk tape container instead.");
          }
        }
        DCO_TAPE_INT index() const { return stce_10152; }
        DCO_TAPE_INT& index() { return stce_10152; }
        DCO_TAPE_INT stce_10160() { return ++stce_10152; }
        void stce_10184(entry const* end) { stce_10182 = end; }
        explicit iterator(entry* stce_10150=0, DCO_TAPE_INT index = DCO_TAPE_INT(-1)) : stce_10181(stce_10150), stce_10152(index) { }
        void operator--() {
          if(stce_10181->stce_10149())
            stce_10152--;
          stce_10181--;
        }
        bool stce_10149() const {
          return stce_10181->stce_10149();
        }
        iterator operator--(int) {
          iterator stce_10054(*this);
          operator--();
          return stce_10054;
        }
        entry* operator*() { return stce_10181; }
        entry* operator->() { return stce_10181; }
        entry* operator*() const { return stce_10181; }
        entry* operator->() const { return stce_10181; }
        bool operator==(const iterator& stce_9926) const { return stce_10181 == stce_9926.stce_10181; }
        bool operator!=(const iterator& stce_9926) const { return !operator==(stce_9926); }
        bool operator< (const iterator& stce_9926) const { return stce_10181 < stce_9926.stce_10181; }
        bool operator>=(const iterator& stce_9926) const { return !operator<(stce_9926); }
        bool operator> (const iterator& stce_9926) const { return stce_10181 > stce_9926.stce_10181; }
        bool operator<=(const iterator& stce_9926) const { return !operator>(stce_9926); }
        friend std::ostream& operator<< ( std::ostream &stce_10185, const iterator &stce_10156 ) {
          stce_10185 << "(" << stce_10156.stce_10181 << ":" << stce_10156.stce_10152 << ")";
          return stce_10185;
        }
      };
      class stce_9917 {
        iterator& stce_10161;
        DCO_TAPE_INT stce_10152;
      public:
        stce_9917(iterator& current) : stce_10161(current), stce_10152(0) {}
        iterator& current() { return stce_10161; }
        stce_9917& operator=(stce_9917 stce_9926) {
          swap(stce_9926);
          return *this;
        }
        DCO_TAPE_INT index() const { return stce_10152; }
        void swap(stce_9917& stce_9926) {
          std::swap(stce_10161, stce_9926.stce_10161);
          std::swap(stce_10152, stce_9926.stce_10152);
        }
        __attribute__((always_inline)) inline void insert(const DCO_TAPE_INT& stce_10045, const stce_9766& pval) {
          if(stce_10152 == 0) {
              stce_10161.stce_10159();
              new (*stce_10161) entry(entry::stce_10151(stce_10045),pval);
              stce_10152 = stce_10161.stce_10160();
              return;
            }
          if(stce_9758 && stce_10045 == stce_10161->arg()) {
              stce_10161->stce_10180(pval);
              return;
            }
          stce_10161.stce_10159();
          new (*stce_10161) entry(stce_10045,pval);
        }
      private:
        stce_9917(const stce_9917& stce_9926);
      };
      explicit stce_10142(stce_10142 &stce_9860,
                              stce_10177 stce_10186)
        : stce_10144(stce_9860),
          object_logger("BLOB container"),
          stce_10138(0),
          stce_10161(), stce_10170(), stce_10171(),
          stce_10187(stce_10186),
          stce_10188(stce_9860.stce_10188) {
        if (stce_10186 == stce_10178) {
            stce_10138 = stce_9860.stce_10138;
            stce_10161 = iterator(stce_10138, stce_9860.current().index());
            stce_10170 = stce_10161;
            stce_10171 = stce_9860.stce_10171;
            stce_10161.stce_10184(*stce_10171);
          } else if (stce_10186 == stce_10179) {
          DCO_TAPE_INT stce_10165 = stce_10189(stce_10188);
          stce_10161 = iterator(stce_10138, (stce_9860.current().index()));
          stce_10170 = iterator(stce_10138 , (stce_9860.current().index()));
          stce_10171 = iterator(stce_10138 + static_cast<DCO_INTEGRAL_TAPE_INT>(stce_10165) - 1, std::numeric_limits<DCO_INTEGRAL_TAPE_INT>::max());
          stce_10161.stce_10184(*stce_10171);
          new (*stce_10161) entry(entry::stce_10151(0));
        }
      }
  DCO_TAPE_INT stce_10189(const tape_options& stce_10162) {
    size_t stce_10167 = get_allocation_size(stce_10162.blob_size_in_byte());
    stce_10138 = static_cast<entry*>(stce_10031(&stce_10167));
    if (!stce_10138) {
      throw dco::exception::stce_9913<std::bad_alloc>
        ("Could not allocate memory. Use environment variables (DCO_MEM_RATIO, DCO_MAX_ALLOCATION) and see documentation.");
    }
    size_t stce_10190 = stce_10167 / entry::size_of();
    if (static_cast<double>(stce_10190) / static_cast<double>(std::numeric_limits<DCO_INTEGRAL_TAPE_INT>::max()) > 1.0) {
      stce_10190 = std::numeric_limits<DCO_TAPE_INT>::max();
    }
    DCO_TAPE_INT stce_10165 = static_cast<DCO_TAPE_INT>(stce_10190);
    DCO_LOG(logINFO) << "BLOB container; size = " << stce_10162.blob_size_in_byte() << "b ~=~ "
                     << stce_10162.blob_size_in_byte() / entry::size_of() << "elements";
    DCO_LOG(logINFO) << "BLOB container; tried allocation size = " << stce_10167
                     << "b ~=~ " << stce_10167 / entry::size_of() << "elements";
    DCO_LOG(logDEBUG) << "sizeof(entry) = " << sizeof(entry) << " == " << entry::size_of();
    return stce_10165;
  }
  void init(tape_options const& stce_10162) {
    DCO_TAPE_INT stce_10165 = stce_10189(stce_10162);
    stce_10161 = iterator(stce_10138, 0);
    stce_10170 = iterator(stce_10138, 0);
    stce_10171 = iterator(stce_10138 + static_cast<DCO_INTEGRAL_TAPE_INT>(stce_10165) - 1, std::numeric_limits<DCO_INTEGRAL_TAPE_INT>::max());
    stce_10161.stce_10184(*stce_10171);
    new (*stce_10161) entry(entry::stce_10151(0));
  }
  explicit stce_10142(tape_options const& stce_10162,
                          tape_t& tape)
    : stce_10144(tape),
      object_logger("BLOB container"),
      stce_10138(0),
      stce_10161(), stce_10170(), stce_10171(),
      stce_10187(stce_10179),
      stce_10188(stce_10162) {
    assert(stce_10143!=memory_model::stce_9775);
    init(stce_10162);
  }
  explicit stce_10142(iterator& stce_9815,
                          tape_t& tape)
    : stce_10144(tape),
      object_logger("BLOB container"),
      stce_10138(0),
      stce_10161(stce_9815), stce_10170(), stce_10171(),
      stce_10187(stce_10179),
      stce_10188(tape.stce_10162()) {
    assert(stce_10143==memory_model::stce_9775);
  }
      ~stce_10142() {
    if (stce_10187 == stce_10179)
                stce_10035(stce_10138);
      }
      __attribute__((always_inline)) inline iterator& stce_9921() { return stce_10161; }
      entry* &stce_10191() { return stce_10138; }
      iterator current() const { return stce_10161; }
      iterator current() { return stce_10161; }
      iterator& stce_10192() { return stce_10161; }
      iterator start() { return stce_10170; }
      iterator start() const { return stce_10170; }
      iterator rend() { return start(); }
      iterator rbegin() const { return current(); }
      iterator capacity() const { return stce_10171; }
      bool empty() const { return stce_10161 == start(); }
      DCO_TAPE_INT size(const iterator& stce_9860, const iterator& stce_9861) const {
        if(stce_9860 < stce_9861)
          throw dco::exception::create<std::runtime_error>("size from < to");
        return static_cast<DCO_TAPE_INT>(*stce_9860 - *stce_9861);
      }
  tape_options stce_10162() const { return stce_10188; }
  DCO_TAPE_INT size(iterator stce_9860) const { return size(stce_9860, start()); }
  DCO_TAPE_INT size() const { return size(current(), start()); }
  DCO_TAPE_INT stce_10172() const { return size(stce_10171, start()); }
      size_t size_in_byte() const { return size(current(), start())*sizeof(entry); }
      size_t stce_10173() const { return size(stce_10171 , start())*sizeof(entry); }
      void erase(const iterator& stce_10150) {
        if(!stce_10138)
          return;
        if(stce_9755 && stce_10150 > stce_10171)
          throw dco::exception::create<std::runtime_error>("pos behind stack!");
        if(stce_10150 > stce_10161)
          throw dco::exception::create<std::runtime_error>("reset to position out of tape!");
        stce_10161 = stce_10150;
        stce_10161.stce_10184(*stce_10171);
      }
        void erase() {
        if(!stce_10138)
          return;
        stce_10161 = stce_10170;
        stce_10161.stce_10184(*stce_10171);
      }
      void stce_10174(const iterator&) { }
      void stce_10175 ( ) { }
      DCO_TAPE_INT stce_10176() { return stce_10161.index()+1; }
private:
  stce_10142(const stce_10142& stce_9926);
  entry * stce_10138;
  typename stce_9816
    <iterator, stce_10143==memory_model::stce_9775>::type stce_10161;
  iterator stce_10170;
  iterator stce_10171;
  stce_10177 stce_10187;
  tape_options stce_10188;
};
}
}
namespace dco {
namespace internal {
template<typename stce_10140, memory_model::stce_9770 stce_10143>
  class stce_10142<memory_model::CHUNK_TAPE, stce_10140, stce_10143>
  : public stce_10137<stce_10140>, object_logger
{
  typedef stce_10137<stce_10140> stce_10144;
private:
  typedef typename stce_9794<stce_10140>::type stce_9766;
protected:
  typedef stce_10142<memory_model::BLOB_TAPE, stce_10140> stce_10193;
  static void stce_10194(const std::string& stce_10195, char* data, long size)
  {
    DCO_LOG(logDEBUG1) << "save_data_to_file: bytes to write = " << size << ", fname = " << stce_10195;
    std::ofstream out(stce_10195.c_str(), std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);
    if (!out.is_open())
      throw dco::exception::create<std::runtime_error>(
          "could not open file, perhaps index corruption?");
    if (!out.good())
      throw dco::exception::create<std::runtime_error>(
          "file could be opened, but got corrupted afterwards.");
    out.write(data, size);
    if (!out.good())
      throw dco::exception::create<std::runtime_error>(
          "reading the chunk failed, container corrupted.");
    out.close();
  }
  static DCO_INTEGRAL_TAPE_INT stce_10196(const std::string& stce_10195, char* data)
  {
    struct stat stce_10197;
    if (stat(stce_10195.c_str(), &stce_10197) == -1)
      throw dco::exception::create<std::runtime_error>(
          "could not check file size, perhaps index corruption?");
    DCO_INTEGRAL_TAPE_INT size = static_cast<DCO_INTEGRAL_TAPE_INT>(stce_10197.st_size);
    std::ifstream in(stce_10195.c_str(), std::ios_base::in | std::ios_base::binary);
    if (!in.is_open())
      throw dco::exception::create<std::runtime_error>(
          "could not open file, perhaps index corruption?");
    if (!in.good())
      throw dco::exception::create<std::runtime_error>(
          "file could be opened, but got corrupted afterwards.");
    in.read(data, size);
    if (!in.good())
      throw dco::exception::create<std::runtime_error>(
          "reading the chunk failed, container corrupted.");
    return size;
  }
public:
  class stce_10198 : public stce_10193 {
    DCO_TAPE_INT stce_10199;
    tape_options stce_10188;
    bool stce_10200;
    stce_10198 *stce_10201, *stce_10202;
    DCO_TAPE_INT stce_10203;
public:
    typedef typename stce_10193::entry entry;
    typedef typename stce_10193::iterator iterator;
    DCO_TAPE_INT stce_10204() const { return stce_10199; }
    static std::string stce_10205(const stce_10198& stce_10193) {
      std::stringstream stce_10195;
      stce_10195 << stce_10193.stce_10206().filename() << '.' << stce_10193.stce_10204();
      return stce_10195.str();
    }
    stce_10198(stce_10198& stce_9860, typename stce_10193::stce_10177 stce_10207)
      : stce_10193(stce_9860, stce_10207), stce_10199(stce_9860.stce_10204()+1), stce_10188(stce_9860.stce_10206()), stce_10200(true),
        stce_10201(0), stce_10202(0), stce_10203(-1) { }
    stce_10198(tape_options stce_10162, stce_10140 &tape)
      : stce_10193(stce_10162, tape), stce_10199(0), stce_10188(stce_10162), stce_10200(true),
        stce_10201(0), stce_10202(0), stce_10203(-1) { }
    ~stce_10198() {
      if(stce_10206().write_to_file()) std::remove(stce_10205(*this).c_str());
    }
    DCO_TAPE_INT size(const iterator& stce_9860, const iterator& stce_9861) const {
      return stce_10193::size(stce_9860, stce_9861);
    }
    DCO_TAPE_INT size(iterator stce_9860) const {
      if(!*this->start())
        throw dco::exception::create<std::runtime_error>("given iterator does not point "
            "into given tape!");
      return stce_10193::size(stce_9860, this->start());
    }
    DCO_TAPE_INT size() const {
        return stce_10193::size(this->current(), this->start());
    }
    DCO_TAPE_INT stce_10172() const {
      if(!stce_10208())
        return 0;
      return stce_10193::stce_10172();
    }
    size_t size_in_byte() const {
      return stce_10193::size(this->current(), this->start()) * stce_10193::entry::size_of();
    }
    size_t stce_10173() const {
      if(!stce_10208()) { return 0; }
      return stce_10193::stce_10172() * stce_10193::entry::size_of();
    }
    tape_options stce_10206() const {
      return stce_10188;
    }
    bool stce_10208() const { return stce_10200; }
    void stce_10209(bool stce_10210) { stce_10200 = stce_10210; }
    void stce_10211() { stce_10203 = size(); }
    void stce_10212() { stce_10203 = -1; }
    bool stce_10213() { return stce_10203 != size(); }
    stce_10198* &stce_10214() { return stce_10201; }
    stce_10198* &stce_10215() { return stce_10202; }
  };
  typedef typename stce_10193::entry entry;
  typedef stce_10198* stce_10216;
  typedef typename stce_10193::iterator stce_10217;
  static void stce_10218(stce_10198& stce_10193) {
    if(stce_10193.stce_10206().write_to_file()) {
      assert(!stce_10193.stce_10208());
      DCO_LOG(logDEBUG1) << "wake_up: waking up chunk " << stce_10193.stce_10204() << " | " << stce_10193.stce_10191() << " from sleep.";
      DCO_INTEGRAL_TAPE_INT size = stce_10196(stce_10198::stce_10205(stce_10193),
                                                       reinterpret_cast<char*>(stce_10193.stce_10191()));
      (void)size;
      stce_10193.stce_10209(true);
    } else {
      DCO_LOG(logDEBUG1) << "wake_up: waking already awake chunk " << stce_10193.stce_10204() << " | " << stce_10193.stce_10191();
    }
  }
  static void sleep(stce_10198& stce_10193) {
    DCO_LOG(logDEBUG1) << "sleep: chunk " << stce_10193.stce_10204() << " | " << stce_10193.stce_10191() << " dirty = " << stce_10193.stce_10213();
    if(stce_10193.stce_10206().write_to_file() && stce_10193.stce_10213()) {
      assert(stce_10193.stce_10208());
      DCO_LOG(logDEBUG1) << "sleep: writing chunk " << stce_10193.stce_10204() << " | " << stce_10193.stce_10191();
      const long stce_10219 = stce_9752(stce_10193.size()+1) * sizeof(entry);
      DCO_LOG(logDEBUG1) << "sleep: chunk.size() = " << stce_10193.size() << ", bytes to write = " << stce_10219 << ", ptr diff = " << stce_10193.current() << ", " << stce_10193.start() << " = " << *stce_10193.current() - *stce_10193.start() << " " << sizeof(entry);
      entry* data = stce_10193.stce_10191();
      stce_10194(stce_10198::stce_10205(stce_10193),
                        reinterpret_cast<char*>(data), stce_10219);
      stce_10193.stce_10211();
    } else {
      if (!stce_10193.stce_10213()) {
        DCO_LOG(logDEBUG1) << "sleep: not writing data for chunk: " << stce_10193.stce_10204();
      }
    }
  }
  static void stce_10220(stce_10198& stce_10193, stce_10198& stce_10221) {
    if (!stce_10193.stce_10208()) {
      sleep(stce_10221);
      stce_10221.stce_10209(false);
      stce_10218(stce_10193);
    }
  }
  void stce_10220(stce_10198& stce_10193) {
    if (!stce_10193.stce_10208()) {
      DCO_LOG(logDEBUG1) << "activate: chunk " << stce_10193.stce_10204() << " not awake yet";
      stce_10198* stce_10221 = stce_10161.stce_10222();
      while (stce_10221 && !stce_10221->stce_10208()) { stce_10221 = stce_10221->stce_10214(); }
      if (stce_10221) {
        DCO_LOG(logDEBUG1) << "activate: currently awake " << stce_10221->stce_10204();
        stce_10220(stce_10193, *stce_10221);
      } else {
        stce_10218(stce_10193);
      }
    }
  }
  class iterator {
private:
    stce_10216 stce_10223;
    stce_10217 stce_10224;
public:
    stce_10216 stce_10222() const {
      return stce_10223;
    }
    stce_10217 stce_10225() const {
      return stce_10224;
    }
    explicit iterator(const stce_10217& stce_10226, stce_10216 stce_10222) :
        stce_10223(stce_10222), stce_10224(stce_10226) {
    }
    explicit iterator(stce_10216 stce_10222) :
        stce_10223(stce_10222), stce_10224(stce_10222->current()) {
    }
    iterator(const iterator& stce_9926) :
        stce_10223(stce_9926.stce_10223), stce_10224(stce_9926.stce_10224) {
    }
    iterator() : stce_10223(), stce_10224() {}
    bool stce_10149() const {
      return stce_10224.stce_10149();
    }
    void operator--() {
      DCO_LOG(logDEBUG4) << stce_10224 << " =?= " << stce_10223->start();
      --stce_10224;
      if (stce_10224 == stce_10223->start()) {
          if(stce_10223->stce_10204()==0) return;
        stce_10223 = stce_10223->stce_10214();
        stce_10220(*stce_10223, *(stce_10223->stce_10215()));
        stce_10224 = stce_10223->current();
      }
    }
    DCO_TAPE_INT index() const { return stce_10224.index(); }
    DCO_TAPE_INT& index() { return stce_10224.index(); }
    DCO_TAPE_INT stce_10160() { stce_10223->stce_10192().stce_10160(); return stce_10224.stce_10160(); }
    stce_10216& stce_10193() { return stce_10223; }
    stce_10217& stce_10227() { return stce_10224; }
    iterator operator--(int) {
      iterator stce_10054(*this);
      operator--();
      return stce_10054;
    }
    entry* operator* () { return *stce_10224; }
    entry* operator* () const { return *stce_10224; }
    entry* operator->() { return *stce_10224; }
    entry* operator->() const { return *stce_10224; }
    bool operator==(const iterator& stce_9926) const {
      return stce_10224 == stce_9926.stce_10224 && stce_10223 == stce_9926.stce_10223;
    }
    bool operator!=(const iterator& stce_9926) const {
      return !operator==(stce_9926);
    }
    bool operator<(const iterator& stce_9926) const {
      return
          (stce_10223 == stce_9926.stce_10223) ?
              stce_10224 < stce_9926.stce_10224 : stce_10224.index() < stce_9926.stce_10224.index();
    }
    bool operator>=(const iterator& stce_9926) const {
      return !operator<(stce_9926);
    }
    bool operator>(const iterator& stce_9926) const {
      return
          (stce_10223 == stce_9926.stce_10223) ?
              stce_10224 > stce_9926.stce_10224 : stce_10224.index() > stce_9926.stce_10224.index();
    }
    bool operator<=(const iterator& stce_9926) const {
      return !operator>(stce_9926);
    }
  };
  class stce_9917
  {
    stce_10216& stce_10228;
    stce_10217& stce_10229;
    stce_10217 stce_10224;
    stce_10217 stce_10230;
    DCO_TAPE_INT stce_10152;
public:
    stce_9917(iterator& stce_9815) :
      stce_10228(stce_9815.stce_10193()),
      stce_10229(stce_9815.stce_10227()),
      stce_10224(stce_10228->current()),
      stce_10230(stce_10228->capacity()),
      stce_10152(0) {
    }
    stce_10217& current() { return stce_10224; }
    ~stce_9917() {
      if(static_cast<DCO_INTEGRAL_TAPE_INT>(stce_10152)) {
        stce_10229 = stce_10228->stce_10192() = stce_10224;
      }
    }
    DCO_TAPE_INT index() const { return stce_10152; }
    DCO_TAPE_INT& index() { return stce_10152; }
    __attribute__((always_inline)) inline void insert(const DCO_TAPE_INT& stce_10045, const stce_9766& pval) {
      if (stce_10152 == 0) {
        stce_10224.stce_10159();
        new (*stce_10224) entry(entry::stce_10151(stce_10045), pval);
        stce_10152 = stce_10224.stce_10160();
        DCO_LOG(logDEBUG2) << "inserting first entry, index now = " << stce_10152 << "; pval = " << pval << ", target = " << stce_10045 << " in chunk " << stce_10228 << " into memory at = " << stce_10224;
        return;
      }
      if(stce_9758 && stce_10224->arg() == stce_10045){
        DCO_LOG(logDEBUG3) << "merging a parallel edge";
        stce_10224->stce_10180(pval);
        return;
      }
      if (*stce_10224 >= *stce_10230) {
        DCO_LOG(logDEBUG1) << "insert: chunk full, updating current and getting next";
        stce_10229 = stce_10228->stce_10192() = stce_10224;
        stce_10231(stce_10228);
        stce_10224 = stce_10228->current();
        stce_10230 = stce_10228->capacity();
      }
      stce_10224.stce_10159();
      new (*stce_10224) entry(stce_10045, pval);
      DCO_LOG(logDEBUG2) << "inserting next entry, index now = " << stce_10152 << "; pval = " << pval << ", target = " << stce_10045 << " in chunk " << stce_10228 << " into memory at = " << stce_10224;
    }
  private:
    stce_9917(const stce_9917& stce_9926);
  };
public:
  void init(tape_options const& stce_10162) {
    tape_options stce_10232 = stce_10162;
    stce_10232.blob_size_in_byte() = stce_10162.chunk_size_in_byte();
    stce_10233 = new stce_10198(stce_10232, stce_10144::stce_10139());
    stce_10161 = iterator(stce_10233);
  }
  explicit stce_10142(tape_options const& stce_10162, stce_10140& tape) :
    stce_10144(tape),
    object_logger("CHUNK container (w/ options)"),
    stce_10233(0),
    stce_10161() {
    assert(stce_10143!=memory_model::stce_9775);
    init(stce_10162);
  }
  explicit stce_10142(iterator &stce_9815, stce_10140& tape) :
    stce_10144(tape),
    object_logger("CHUNK container (w/ options)"),
    stce_10233(0), stce_10161(stce_9815) {
    assert(stce_10143==memory_model::stce_9775);
  }
  ~stce_10142() {
    stce_10198* stce_10156 = stce_10233;
    while (stce_10156) {
      stce_10198* stce_10234 = stce_10156;
      stce_10156 = stce_10156->stce_10215();
      delete stce_10234;
    }
  }
  static void stce_10231(stce_10216& stce_9815)
  {
    DCO_LOG(logDEBUG1) << "current chunk: id = " << stce_9815->stce_10204() << ", index = " << stce_9815->current().index()
                       << ", data pointer = " << stce_9815->stce_10191();
    delete stce_9815->stce_10215();
    if (stce_9815->stce_10206().write_to_file()) {
      sleep(*stce_9815);
      stce_9815->stce_10209(false);
      stce_9815->stce_10215() = new stce_10198(*stce_9815, stce_10198::stce_10178);
    } else {
      stce_9815->stce_10215() = new stce_10198(*stce_9815, stce_10198::stce_10179);
    }
    stce_9815->stce_10215()->stce_10214() = stce_9815;
    stce_9815 = stce_9815->stce_10215();
    DCO_LOG(logDEBUG1) << "next chunk: id = " << stce_9815->stce_10204() << ", index = " << stce_9815->current().index()
                       << ", data pointer = " << stce_9815->stce_10191();
  }
  __attribute__((always_inline)) inline iterator& stce_9921()
  {
    if (stce_10161.stce_10193()->current() >= stce_10161.stce_10193()->capacity()) {
      DCO_LOG(logDEBUG1) << "insert_ref: chunk full, getting next";
      stce_10231(stce_10161.stce_10193());
    }
    return stce_10161;
  }
  iterator start() { return iterator(stce_10233->start(), stce_10233); }
  iterator start() const { return iterator(stce_10233->start(), stce_10233); }
  iterator rend() { return start(); }
  iterator current() const { return stce_10161; }
  iterator rbegin() const { return current(); }
  bool empty() const { return current() == start(); }
  DCO_TAPE_INT size(const iterator& stce_9860, const iterator& stce_9861) const {
    const stce_10198& stce_10235 = *(stce_9860.stce_10222());
    if(stce_9860.stce_10222() == stce_9861.stce_10222())
      return stce_10235.size(stce_9860.stce_10225(), stce_9861.stce_10225());
    DCO_TAPE_INT size = 0;
    stce_10216 stce_10236 = stce_9860.stce_10222();
    size += stce_10236->size(stce_9860.stce_10225());
    DCO_LOG(logDEBUG2) << "size of last chunk = " << size;
    for (stce_10236 = stce_10236->stce_10214(); stce_10236 != stce_9861.stce_10222(); stce_10236 = stce_10236->stce_10214()) {
      if(stce_10236) {
        size += stce_10236->size();
        DCO_LOG(logDEBUG2) << "adding " << stce_10236->size() << " => size = " << size;
      }
    }
    const stce_10198& stce_10237 = *(stce_9861.stce_10222());
    size += stce_10237.size(stce_10237.current(), stce_9861.stce_10225());
    DCO_LOG(logDEBUG2) << "very first bit: adding " << stce_10237.size(stce_10237.current(), stce_9861.stce_10225()) << " => size = " << size;
    return size;
  }
  DCO_TAPE_INT size(iterator stce_9860) const { return size(stce_9860, start()); }
  DCO_TAPE_INT size() const { return size(current(), start()); }
  mem_long_t size_in_byte() const {
    return size(current(), start())*entry::size_of();
  }
  DCO_TAPE_INT stce_10172() const {
    DCO_TAPE_INT size = 0;
    for(stce_10216 stce_10238 = stce_10233; stce_10238 != NULL; stce_10238 = stce_10238->stce_10215())
      if(stce_10238)
        size += stce_10238->stce_10172();
    return size;
  }
  mem_long_t stce_10173() const {
    mem_long_t size = 0;
    for(stce_10216 stce_10238 = stce_10233; stce_10238 != NULL; stce_10238 = stce_10238->stce_10215())
      if(stce_10238) size += stce_10238->stce_10173();
    return size;
  }
  void erase(const iterator& stce_10150) {
    if (stce_10150 > current())
      throw dco::exception::create<std::invalid_argument>("erase pos out of tape!");
    stce_10216 stce_10239(stce_10233);
    while (stce_10239 != stce_10150.stce_10222())
      stce_10239 = stce_10239->stce_10215();
    stce_10161.stce_10193() = stce_10239;
    stce_10220(*stce_10161.stce_10193());
    stce_10161.stce_10193()->stce_10212();
    stce_10216 stce_10156 = stce_10239->stce_10215();
    stce_10239->stce_10215() = 0;
    while (stce_10156) {
      stce_10216 stce_10234 = stce_10156;
      stce_10156 = stce_10156->stce_10215();
      delete stce_10234;
    }
    stce_10161.stce_10193()->erase(stce_10150.stce_10225());
    stce_10161.stce_10227() = stce_10161.stce_10193()->current();
  }
  void stce_10174(const iterator& stce_9815) {
    DCO_LOG(logDEBUG1) << "chunk tape: prepare_interpretation; activate: " << stce_9815.stce_10222()->stce_10204() << ", is awake = " << stce_9815.stce_10222()->stce_10208();
    stce_10220(*stce_9815.stce_10222());
  }
  void stce_10175() {
    DCO_LOG(logDEBUG1) << "chunk tape: prepare_recording";
    stce_10220(*stce_10161.stce_10193());
  }
  DCO_TAPE_INT stce_10176() { return current().index()+1; }
private:
  stce_10198* stce_10233;
  typename stce_9816
    <iterator, stce_10143==memory_model::stce_9775>::type stce_10161;
};
}
}
       
namespace dco {
namespace internal {
template<class stce_10140>
  class stce_10142<memory_model::stce_9774, stce_10140>
  : public stce_10142<memory_model::CHUNK_TAPE, stce_10140>
{
  typedef stce_10140 tape_t;
  typedef typename stce_9794<stce_10140>::type stce_9766;
public:
  typedef stce_10142<memory_model::CHUNK_TAPE, stce_10140> stce_10240;
  typedef typename stce_10240::entry entry;
  typedef typename stce_10240::stce_9917 stce_9917;
  typedef typename stce_10240::iterator iterator;
  bool stce_10241, stce_10005, stce_10242, stce_10243;
  double stce_10244, stce_10245, stce_9999;
  std::vector<DCO_TAPE_INT> stce_10008;
  std::vector<stce_9766> stce_10009;
  std::vector<const void*> stce_10246;
  DCO_TAPE_INT stce_10247, stce_10248;
  struct stce_10249 {
    static std::string id() { return " gtas "; }
  };
  typedef logging<stce_10249> logger;
  explicit stce_10142(tape_options stce_10162, stce_10140 &tape) :
    stce_10240(stce_10162, tape),
    stce_10241(false),
    stce_10005(false),
    stce_10242(false),
    stce_10243(false),
    stce_10244(1e-6),
    stce_10245(1e-6),
    stce_9999(0.0),
    stce_10247(0),
    stce_10248(-1) {}
  void stce_10250(log_level_enum level) {
    if (stce_10243 && level <= dco::logWARNING) {
      throw dco::exception::create<std::runtime_error>("Warning occured.");
    }
  }
  void stce_10004() {
    if (stce_10248 == stce_10247) {
      stce_10247++;
      throw dco::exception::create<std::runtime_error>("Event counter reached.");
    }
    stce_10247++;
  };
  void stce_10003(stce_9766 stce_9930, double stce_10002)
  {
    if(stce_10242) {
      double stce_10251 = ::fabs(stce_9930 - stce_10002);
      log_level_enum stce_10252 = dco::logWARNING;
      if (stce_10251 < stce_10244) {stce_10252 = dco::logINFO;}
      logger(stce_10252).get_stream() << stce_10247 << ": fd vs. tangent check";
      logger(stce_10252).get_stream() << " delta = " << stce_10251 << " tangent = " << stce_9930
          << " finite differences = " << stce_10002;
      stce_10250(stce_10252);
    }
  }
  void stce_10010(double stce_10007, double stce_10253, double stce_10254)
  {
    const double stce_10255 = ::fabs(stce_10007-stce_10253);
    const double stce_10256 = ::fabs(stce_10007-stce_10254);
    log_level_enum stce_10252 = dco::logWARNING;
    if (stce_10255 < stce_10245) {stce_10252 = dco::logINFO;}
    logger(stce_10252).get_stream() << stce_10247 << ": Tangent/adjoint identity check:";
    logger(stce_10252).get_stream() << " tangent vs. adjoint = " << stce_10255;
    logger(stce_10252).get_stream() << "  <x_(1),x^(1)> = " << stce_10007;
    logger(stce_10252).get_stream() << "  <y_(1),y^(1)> = " << stce_10253;
    stce_10250(stce_10252);
    stce_10252 = dco::logWARNING;
    if (stce_10256 < stce_10244) {stce_10252 = dco::logINFO;}
    logger(stce_10252).get_stream() << " finite differences vs. adjoint = " << stce_10256;
    logger(stce_10252).get_stream() << "  <y_(1),y_{FD}^(1)> = " << stce_10254;
    stce_10250(stce_10252);
  }
  void stce_10257(const iterator &stce_9861) {
    assert(stce_10240::start() == stce_9861);
    stce_10240::stce_10257(stce_9861);
    stce_10008.clear();
    stce_10009.clear();
    stce_10246.clear();
    stce_9999=0;
  }
  void stce_9974(std::string stce_10258, const bool stce_9837, const bool stce_9971, const bool stce_9973)
  {
    if (!stce_10241) {
      stce_10004();
      return;
    }
    log_level_enum stce_10252 = dco::logWARNING;
    if (stce_9837 == stce_9971 && stce_9837 == stce_9973) {
      stce_10252 = dco::logINFO;
    }
    logger(stce_10252).get_stream() << stce_10247 << ": Branch check:" << stce_10258
        << std::boolalpha << " at current value returns " << stce_9837 << ", at minus h " << stce_9971
        << ", and at plus h " << stce_9973;
    stce_10250(stce_10252);
    stce_10004();
  }
  template <class data_t>
  void stce_10000(const data_t* data, const double &stce_9930) {
    if(stce_9999 == 0) {
        stce_10246.push_back(data);
        stce_10008.push_back(data->_tape_index());
        stce_10009.push_back(stce_9930);
    }
  }
  template <typename stce_10259>
  void stce_10260() {
      double stce_10261=0;
      for(size_t stce_9829=0;stce_9829<stce_10246.size();++stce_9829) {
        const stce_10259 &stce_9815 = *static_cast<const stce_10259*>(stce_10246[stce_9829]);
        stce_10261 = std::max(stce_10261, std::fabs(stce_9815.stce_9997[0]));
      }
      const double stce_10262 = 1e-14;
      stce_9999 = std::sqrt(stce_10262) * stce_10261;
      for(size_t stce_9829=0;stce_9829<stce_10246.size();++stce_9829) {
        const stce_10259 &stce_9815 = *static_cast<const stce_10259*>(stce_10246[stce_9829]);
        stce_9815.stce_9997[0] -= stce_9999 * stce_9815.stce_9929;
        stce_9815.stce_9997[1] += stce_9999 * stce_9815.stce_9929;
      }
  }
};
}
}
       
namespace dco {
namespace internal {
template<class stce_10140, memory_model::stce_9770 stce_10141>
  class stce_10142<memory_model::stce_9775, stce_10140, stce_10141>
  : public stce_10142<stce_10141, stce_10140, memory_model::stce_9775> {
  typedef typename stce_9794<stce_10140>::type stce_9766;
public:
  typedef stce_10142<stce_10141, stce_10140, memory_model::stce_9775> stce_10240;
  explicit stce_10142(tape_options stce_10162, stce_10140 &tape) :
    stce_10240(stce_10161, tape), stce_10161() {
    stce_10240::init(stce_10162);
  }
  struct iterator : public stce_10240::iterator {
    iterator() : stce_10263(1), stce_10264(0) {}
    iterator(const typename stce_10240::iterator& stce_10265) : stce_10240::iterator(stce_10265), stce_10263(-1), stce_10264(-1) {}
    DCO_TAPE_INT stce_10263;
    DCO_TAPE_INT stce_10264;
    const DCO_TAPE_INT& max_dist() const { return stce_10263; }
    void stce_10266(const DCO_TAPE_INT &stce_10267) {
      stce_10263 = stce_10267+1;
      DCO_LOG(logDEBUG3) << " updating max dist: " << stce_10263;
    }
    const DCO_TAPE_INT &stce_10268() const { return stce_10264; }
    DCO_TAPE_INT stce_10160() {
      stce_10264++;
      return stce_10240::iterator::stce_10160();
    }
  };
  class stce_9917 : public stce_10240::stce_9917 {
    iterator &stce_10161;
public:
    stce_9917(iterator& current) : stce_10240::stce_9917(current), stce_10161(current) { }
    iterator& current() { return stce_10161; }
    __attribute__((always_inline)) inline void insert(const DCO_TAPE_INT& stce_10045, const stce_9766& pval) {
      DCO_LOG(logDEBUG4) << "target = " << stce_10045 << ", parametercounter = " << current().stce_10268() << ", max dist = " << current().max_dist();
      DCO_TAPE_INT stce_10269 = current().index();
      stce_10240::stce_9917::insert(stce_10045, pval);
      if(stce_10045 > current().stce_10268()) {
        DCO_TAPE_INT stce_10270 = stce_10269 - stce_10045+1;
        DCO_LOG(logDEBUG4) << "curdist = " << stce_10270;
        if(stce_10270 > current().max_dist()) {
          current().stce_10266(stce_10270);
        }
      }
    }
private:
    stce_9917(const stce_9917& stce_9926);
  };
  __attribute__((always_inline)) inline iterator& stce_9921() {
    return static_cast<iterator&>(stce_10240::stce_9921());
  }
  iterator current() const { return stce_10161; }
  iterator current() { return stce_10161; }
  iterator& stce_10192() { return stce_10161; }
  iterator rbegin() const { return stce_10161; }
  DCO_TAPE_INT stce_10176() {
    using ::log;
    using ::pow;
    DCO_TAPE_INT max_dist = stce_10161.max_dist();
    bool stce_10271 = (max_dist & (max_dist - 1)) == 0;
    if (HAS_BITWISE_MODULO && !stce_10271) {
      DCO_TAPE_INT stce_10087 = static_cast<DCO_TAPE_INT>(floor(log(static_cast<double>(max_dist))/log(2.0)))+1;
      stce_10161.stce_10266( (1 << stce_9752(stce_10087)) - 1 );
      DCO_LOG(logDEBUG2) << "rounding adjoint vector size to " << stce_10161.max_dist();
    }
    return stce_10161.stce_10268() + stce_10161.max_dist() + 1;
  }
private:
  iterator stce_10161;
};
}
namespace helper {
template<typename stce_9792, memory_model::stce_9770 stce_9793, typename external_adjoint_object_t>
  struct stce_10069<internal::tape<memory_model::stce_9775, stce_9792, stce_9793>, external_adjoint_object_t> {
  typedef internal::tape<memory_model::stce_9775, stce_9792, stce_9793> tape_t;
  DCO_TAPE_INT stce_10272;
  DCO_TAPE_INT stce_10273;
  tape_t* &stce_9945;
  stce_10069(external_adjoint_object_t* stce_10274) :
    stce_10272(std::numeric_limits<DCO_TAPE_INT>::max()),
    stce_10273(0),
    stce_9945(stce_10274->tape()) {}
  template <typename stce_9786> void register_input(const stce_9786& stce_9841) {
    DCO_TAPE_INT stce_9830 = stce_9841._tape_index();
    if(stce_9830 > stce_9945->current().stce_10268()) {
      stce_10272 = std::min(stce_10272, stce_9830);
    }
  }
  template <typename stce_9786> void register_output(const stce_9786& stce_10112) {
    stce_10273 = std::max(stce_10273, stce_10112._tape_index());
    if(stce_10272 != std::numeric_limits<DCO_TAPE_INT>::max()) {
        DCO_TAPE_INT max_dist = stce_10273 - stce_10272;
        if(max_dist > stce_9945->stce_10192().max_dist()) {
          stce_9945->stce_10192().stce_10266(max_dist);
        }
    }
  }
};
}
namespace internal {
template<typename stce_9795, typename stce_10140, memory_model::stce_9770 stce_10141>
  class stce_9797<stce_9795, internal::stce_10142<memory_model::stce_9775, stce_10140, stce_10141> >
  : public stce_9797<stce_9795, internal::stce_10142<stce_10141, stce_10140, memory_model::stce_9775> >
{
  typedef internal::stce_10142<memory_model::stce_9775, stce_10140, stce_10141> stce_10275;
  typedef typename stce_10275::stce_10240 stce_10240;
  typedef stce_9797<stce_9795, stce_10240> stce_10276;
  typedef typename stce_10276::stce_9983 stce_9983;
public:
  stce_10275 &stce_9945;
  stce_9797(DCO_TAPE_INT size, stce_10275 &tape) :
    stce_10276(size, static_cast<stce_10240&>(tape)), stce_9945(tape),stce_10277(true), stce_10278(0) { }
  void init() {
    stce_10279 = stce_9945.current().stce_10268();
    if (HAS_BITWISE_MODULO) {
      stce_10280 = stce_9945.current().max_dist()-1;
    } else {
      stce_10280 = stce_9945.current().max_dist();
    }
    stce_10281 = this->stce_10282.data() + stce_9752(stce_10279) + 1;
  }
  bool stce_10283(DCO_TAPE_INT stce_10284) const {
    if(stce_10284> stce_9945.current().stce_10268()) {
      stce_10284 -= stce_9945.current().stce_10268();
      stce_10284 %= stce_9945.current().max_dist();
      stce_10284 += stce_9945.current().stce_10268() + 1;
    }
    return static_cast<DCO_TAPE_INT>(this->stce_10282.size()) > stce_10284;
  }
  DCO_TAPE_INT stce_10285(DCO_TAPE_INT stce_9830) const {
    if(stce_9830 > stce_9945.current().stce_10268()) {
      stce_9830 -= stce_9945.current().stce_10268();
      stce_9830 %= stce_9945.current().max_dist();
      stce_9830 += stce_9945.current().stce_10268() + 1;
    }
    return stce_9830;
  }
  typename stce_10276::stce_9983& operator[](DCO_TAPE_INT stce_9830) {
    DCO_LOG(logDEBUG4) << "getting " << stce_9830;
    stce_9830 = stce_10285(stce_9830);
    DCO_LOG(logDEBUG4) << "matched to " << stce_9830;
    return stce_10276::operator[](stce_9830);
  }
  typename stce_10276::stce_9983 operator[](DCO_TAPE_INT stce_9830) const {
    DCO_LOG(logDEBUG4) << "getting " << stce_9830;
    stce_9830 = stce_10285(stce_9830);
    DCO_LOG(logDEBUG4) << "matched to " << stce_9830;
    return stce_10276::operator[](stce_9830);
  }
  stce_9983 at(DCO_TAPE_INT stce_9830) const { return stce_10283(stce_9830)?(*this)[stce_9830]:0; }
  stce_9983 &at(const DCO_TAPE_INT stce_9830) {
    if(!stce_10283(stce_9830))
      this->resize(stce_9945.stce_10176());
    return (*this)[stce_9830];
  }
  void stce_10286(DCO_TAPE_INT stce_9830) {
    if(!stce_10283(stce_9830)) {
      DCO_LOG(logDEBUG2) << "Resizing internal adjoint vector to " << stce_9830;
      this->resize(stce_9945.stce_10176());
    }
  }
  template <typename iterator_t>
  void stce_10287(iterator_t& stce_9815) {
    DCO_TAPE_INT stce_9861 = stce_9815->arg();
    stce_9983 pval = stce_9815->pval();
    DCO_TAPE_INT stce_9860 = stce_9815.index();
    if (stce_10277) {
      DCO_LOG(logDEBUG3) << "setting " << stce_9860 << " to 0 ";
      stce_9983& stce_10288 = HAS_BITWISE_MODULO ?
                           stce_10281[ (stce_9860 - stce_10279) & stce_10280 ] :
                           stce_10281[ (stce_9860 - stce_10279) % stce_10280 ];
      this->stce_10278 = stce_10288;
      stce_10288 = 0;
    }
    if(stce_9861 > stce_10279) {
      stce_9983& stce_10288 = HAS_BITWISE_MODULO ?
                           stce_10281[ (stce_9861 - stce_10279) & stce_10280 ] :
                           stce_10281[ (stce_9861 - stce_10279) % stce_10280 ];
      stce_10288 += pval*this->stce_10278;
    } else {
      (*this)[stce_9861] += pval*this->stce_10278;
    }
    stce_10277 = stce_9815.stce_10149();
  }
private:
  stce_9983* stce_10281;
  DCO_TAPE_INT stce_10279;
  DCO_TAPE_INT stce_10280;
  bool stce_10277;
  stce_9983 stce_10278;
};
}
}
       
namespace dco {
namespace internal {
class stce_10289 {
  DCO_TAPE_INT operator[] (DCO_TAPE_INT) const { return 0; }
  DCO_TAPE_INT at (DCO_TAPE_INT) const { return 0; }
};
template<class stce_9795, typename stce_10136>
  class stce_9797 : public stce_10137<stce_10136>
{
  typedef stce_10137<stce_10136> stce_10144;
public:
    typedef stce_9795 stce_9983;
protected:
  std::vector<stce_9983> stce_10282;
  DCO_TAPE_INT stce_10290;
public:
  typedef stce_9983 value_t;
  void init() {}
  stce_9797(DCO_TAPE_INT size, stce_10136 &tape) :
    stce_10144(tape), stce_10282(), stce_10290(size) { }
  DCO_TAPE_INT size() const { return stce_10290; }
  bool stce_10283(DCO_TAPE_INT stce_10284) const {
     return static_cast<DCO_TAPE_INT>(stce_10282.size()) > stce_10284;
  }
  long stce_10291() const {
    return static_cast<DCO_TAPE_INT>(stce_10282.capacity() * sizeof(stce_9983));
  }
  stce_9983 stce_10292(DCO_TAPE_INT stce_9830) {
    if(stce_9755) { return stce_10282.at(stce_9830); }
    else { return stce_10282[stce_9830]; }
  }
  stce_9983& operator[](DCO_TAPE_INT stce_9830) {
    if(stce_9755) { return stce_10282.at(stce_9830); }
    else { return stce_10282[stce_9830]; }
  }
  stce_9983 operator[](DCO_TAPE_INT stce_9830) const {
    if(stce_9755) { return stce_10282.at(stce_9830); }
    else { return stce_10282[stce_9830]; }
  }
  void stce_10287(typename stce_10136::iterator& stce_9815) {
    (*this)[stce_9815->arg()] += stce_9815->pval()*(*this)[stce_9815.index()];
  }
  void resize(DCO_TAPE_INT stce_10293) {
    DCO_LOG(logDEBUG2) << "Resizing internal adjoint vector to " << stce_10293;
    stce_10290 = stce_10293;
  }
  void stce_10286(DCO_TAPE_INT stce_9830) {
    if(!stce_10283(stce_9830)) {
      DCO_LOG(logDEBUG2) << "Resizing internal adjoint vector to " << stce_9830;
      stce_10282.resize(stce_9830);
    }
  }
  stce_9983 at(DCO_TAPE_INT stce_9830) const { return stce_10283(stce_9830)?stce_10282[stce_9830]:0; }
  stce_9983 &at(const DCO_TAPE_INT stce_9830) {
    if(!stce_10282.stce_10283(stce_9830)) { stce_10282.resize(stce_9830+1); }
    return stce_10282[stce_9830];
  }
  void stce_10294(DCO_TAPE_INT stce_9860, DCO_TAPE_INT stce_9861) {
    if(stce_9860 < stce_9861) { std::swap(stce_9860, stce_9861); }
    if(!stce_10283(stce_9861)) { return; }
    stce_10295();
    for (DCO_TAPE_INT stce_9829 = std::min(stce_9860, stce_10290-1); stce_9829 > stce_9861; --stce_9829) {
      stce_10282[stce_9829] = 0;
    }
  }
  void stce_10295() {
    stce_10282.resize(stce_10290);
  }
};
}
template<class stce_10136, class stce_9799, int stce_10296=-1>
  struct adjoint_vector : dco::stce_10070<typename stce_10136::tape_t::stce_9983>, public internal::stce_10137<stce_10136> {
    static const int stce_9835 = stce_10296 < 0 ? -stce_10296 : stce_10296;
  typedef internal::stce_10137<stce_10136> stce_10144;
public:
  typedef dco::helper::stce_9824<stce_9799, stce_9835> value_t;
  typedef stce_9799 adjoint_real_t;
  typedef typename stce_10136::stce_9983 stce_9983;
private:
  value_t* stce_10282;
  DCO_TAPE_INT stce_10297;
public:
  int stce_10298;
  void init() {}
  adjoint_vector(stce_10136 *tape) : stce_10144(*tape), stce_10282(0), stce_10297(0), stce_10298(0) { }
  virtual ~adjoint_vector() {
    if (stce_10282) delete [] stce_10282;
  }
  template<class stce_9798>
    value_t& derivative(const stce_9798 &stce_9877) {
    stce_10299();
    const DCO_TAPE_INT stce_9830 = stce_9877._tape_index();
    return stce_10282[stce_9830];
  }
  void interpret_adjoint() {
    typename stce_10136::iterator stce_9861 = stce_10144::stce_10139().start();
    internal::interpretation_settings stce_10300;
    stce_10301(stce_10144::stce_10139().current(), stce_9861, stce_10300);
  }
  void interpret_adjoint_to(const typename stce_10136::iterator &stce_9861) {
    internal::interpretation_settings stce_10300;
    if (stce_9861 > stce_10144::stce_10139().current())
      throw dco::exception::create<std::runtime_error>("adjoint interpretation: from < to.", "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_adjoint_vector.hpp", 148);
    else
      stce_10301(stce_10144::stce_10139().current(), stce_9861, stce_10300);
  }
  void interpret_adjoint_from(const typename stce_10136::iterator &stce_9860) {
    typename stce_10136::iterator stce_9861 = stce_10144::stce_10139().start();
    assert(!(stce_9860 < stce_9861));
    typename internal::interpretation_settings stce_10300;
    stce_10301(stce_9860, stce_9861, stce_10300);
  }
  void interpret_adjoint_from_to(const typename stce_10136::iterator &stce_9860, const typename stce_10136::iterator &stce_9861) {
    typename internal::interpretation_settings stce_10300;
    if (stce_9861 > stce_9860)
      throw dco::exception::create<std::runtime_error>("adjoint interpretation: from < to.", "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_adjoint_vector.hpp", 164);
    else
      stce_10301(stce_9860, stce_9861, stce_10300);
  }
  void interpret_adjoint_and_zero_adjoints_to(const typename stce_10136::iterator &stce_9861) {
    typename stce_10136::iterator stce_9860(stce_10144::stce_10139().current());
    typename internal::interpretation_settings stce_10300;
    stce_10300.reset = false;
    stce_10300.stce_10056 = true;
    stce_10301(stce_9860, stce_9861, stce_10300);
  }
  void interpret_adjoint_and_zero_adjoints_from_to(const typename stce_10136::iterator &stce_9860, const typename stce_10136::iterator &stce_9861) {
    typename internal::interpretation_settings stce_10300;
    stce_10300.reset = false;
    stce_10300.stce_10056 = true;
    stce_10301(stce_9860, stce_9861, stce_10300);
  }
  void zero_adjoints() {
    typename stce_10136::iterator stce_9861 = stce_10144::stce_10139().start();
    stce_10302(stce_10144::stce_10139().current(), stce_9861);
  }
  void zero_adjoints_to(const typename stce_10136::iterator &stce_9861) {
    stce_10302(stce_10144::stce_10139().current(), stce_9861);
  }
  void zero_adjoints_from(const typename stce_10136::iterator &stce_9860) {
    typename stce_10136::iterator stce_9861 = stce_10144::stce_10139().start();
    stce_10302(stce_9860, stce_9861);
  }
  void zero_adjoints_from_to(const typename stce_10136::iterator &stce_9860, const typename stce_10136::iterator &stce_9861) {
    stce_10302(stce_9860, stce_9861);
  }
  value_t& operator[](DCO_TAPE_INT stce_9830) {
    return stce_10282[stce_9830];
  }
  value_t at(DCO_TAPE_INT stce_9830) const { return (stce_9830 < stce_10297)?stce_10282[stce_9830]:0; }
  value_t& at(DCO_TAPE_INT stce_9830) { stce_10286(stce_9830); return stce_10282[stce_9830]; }
  void stce_10286(DCO_TAPE_INT stce_9830) {
    if(stce_10297 < stce_9830) {
      stce_10297 = stce_9830;
    }
    stce_10299();
  }
  void stce_10287(typename stce_10136::iterator& stce_9815) {
    (*this)[stce_9815->arg()] += stce_9815->pval()*(*this)[stce_9815.index()];
  }
  value_t& stce_10039(const DCO_TAPE_INT stce_9830) {
    return stce_10282[stce_9830];
  }
  stce_9799& stce_10039(const DCO_TAPE_INT stce_9830, int stce_10303) {
    return stce_10282[stce_9830][stce_10303];
  }
  template<class stce_10304>
    void stce_10072(const DCO_TAPE_INT &stce_9830, const stce_10304 &stce_9979, const value_t &stce_10073) {
    stce_10282[stce_9830] += (stce_9979 * stce_10073);
  }
  virtual typename stce_10136::stce_9983 stce_10071(const DCO_TAPE_INT stce_9830) {
    return static_cast<typename stce_10136::stce_9983>(stce_10039(stce_9830)[stce_10298]);
  }
  virtual void stce_10072(const DCO_TAPE_INT stce_9830, const typename stce_10136::stce_9983 &stce_10073) {
    stce_10282[stce_9830][stce_10298] += stce_10073;
  }
  void stce_10302(const typename stce_10136::iterator &stce_9860, const typename stce_10136::iterator &stce_9861) {
    stce_10294(stce_9860.index(), stce_9861.index());
  }
  void stce_10294(DCO_TAPE_INT stce_9860, DCO_TAPE_INT stce_9861) {
    if(stce_9860 < stce_9861) { std::swap(stce_9860, stce_9861); }
    if(stce_10282==0) { return; }
    for(DCO_TAPE_INT stce_9829=stce_9861+1;stce_9829<=stce_9860;stce_9829++) {
      stce_10282[stce_9829] = 0;
    }
  }
  void stce_10301(const typename stce_10136::iterator &stce_9860,
                                   const typename stce_10136::iterator &stce_9861,
                                   typename internal::interpretation_settings stce_10300)
  {
    stce_10299();
    stce_10144::stce_10139().stce_10305(*this, stce_9860, stce_9861, stce_10300);
  }
  void stce_10299() {
    if(stce_10282==0) {
      stce_10297 = stce_10144::stce_10139().current().index()+1;
      stce_10282 = new value_t[size_t(stce_10297)]();
    }
    else {
      DCO_TAPE_INT stce_10306 = stce_10297 = stce_10144::stce_10139().current().index()+1;
      if(stce_10306 > stce_10297) {
        value_t *stce_10307 = new value_t[size_t(stce_10306)]();
        for(DCO_TAPE_INT stce_9829=0;stce_9829<stce_10297;++stce_9829)
          stce_10307[stce_9829] = stce_10282[stce_9829];
        delete [] stce_10282;
        stce_10282 = stce_10307;
      }
    }
  }
};
}
       
namespace dco {
template< typename tape_t >
  void stce_10308(tape_t tape, const std::string &filename = "tape.dot") {
  stce_10308(tape->current(), tape.start(), internal::stce_10289(), stce_9846(filename));
}
template< typename tape_t, typename adjoint_vector>
  void stce_10308(tape_t tape, const adjoint_vector& stce_10309, const std::string &filename = "tape.dot") {
  stce_10308(tape.current(), tape.start(), stce_10309, stce_9846(filename));
}
template< typename stce_10310 >
  void stce_10308(stce_10310 stce_9815, stce_10310 stce_9861, const std::string &filename = "tape.dot") {
  stce_10308(stce_9815, stce_9861 , internal::stce_10289(), stce_9846(filename));
}
template< typename stce_10310 >
  void stce_10308(stce_10310 stce_9815, stce_10310 stce_9861, stce_9846 &stce_9847) {
  stce_10308(stce_9815, stce_9861, internal::stce_10289(), stce_9847);
}
template< typename stce_10310, typename adjoint_vector >
  void stce_10308(stce_10310 stce_9815, stce_10310 stce_9861, const adjoint_vector& stce_10309,
                 const std::string &filename = "tape.dot") {
  stce_10308(stce_9815, stce_9861 , stce_10309, stce_9846(filename));
}
template< typename stce_10310, typename adjoint_vector >
  void stce_10308(stce_10310 stce_9815, stce_10310 stce_9861, const adjoint_vector& stce_10309,
                 stce_9846 &stce_9847) {
  if (stce_9815.index() < stce_9861.index()) {
    std::swap(stce_9815, stce_9861);
  }
  DCO_TAPE_INT stce_10311 = stce_9815.index();
  stce_9847.stce_9856(stce_10311,stce_10309.at(stce_10311));
  for (; stce_9815 != stce_9861; --stce_9815) {
    stce_9847.stce_9859(stce_9815->arg(), stce_10311, stce_9815->pval());
    if(stce_9815->stce_10149()) {
      stce_10311--;
      stce_9847.stce_9856(stce_10311,stce_10309.at(stce_10311));
    }
  }
}
template< typename stce_10310, typename adjoint_vector, typename stce_10312 >
  void stce_10308(stce_10310 stce_9815, stce_10310 stce_9861, const adjoint_vector& stce_10309,
                 const stce_10312& stce_10313, stce_9846 &stce_9847) {
  if (stce_9815.index() < stce_9861.index()) {
    std::swap(stce_9815, stce_9861);
  }
  stce_10308(stce_9815, stce_9861, stce_10309, stce_9847);
  typedef typename stce_10312::const_reverse_iterator stce_10314;
  for (stce_10314 stce_10156 = stce_10313.rbegin(); stce_10156 != stce_10313.rend() && stce_9861 <= stce_10156->stce_10315(); ++stce_10156) {
    if (stce_9815 < stce_10156->stce_10315())
      continue;
    stce_10156->stce_10316()->stce_10079(stce_9847, stce_10156->stce_10315().index() + 1);
  }
}
template <typename stce_10310>
  void stce_10317(stce_10310 stce_9815, stce_10310 stce_9861,
                 std::string const& filename) {
  if (stce_9815.index() < stce_9861.index()) { std::swap(stce_9815, stce_9861); }
  std::ofstream stce_9911(filename.c_str());
  stce_9911 << stce_9815.index() << "\n";
  for (; stce_9815 != stce_9861; --stce_9815) {
    stce_9911 << stce_9815->arg() << ", " << stce_9815->pval() << "; " ;
    if(stce_9815->stce_10149())
      stce_9911 << "\n";
  }
}
template <bool USE_SPARSE_INTERPRET, typename stce_10310, typename adjoint_vector>
  inline void interpret(stce_10310 stce_9815, const stce_10310& stce_9861,
                                            adjoint_vector& stce_10309) {
  stce_9815.index()--;
  stce_10029();;
  stce_10309.stce_10286(stce_10036(stce_9752(stce_9815.index())));
  stce_10309.init();
  for (; stce_9815 != stce_9861; --stce_9815) {
    if(USE_SPARSE_INTERPRET && folding::stce_9914(stce_10309[stce_9815.index()])) { continue; }
    DCO_LOG(logDEBUG3) << stce_10309[stce_9815->arg()] << " <=> adj[" << stce_9815->arg() << "] += " << stce_9815->pval() * stce_10309[stce_9815.index()] << " from idx " << stce_9815.index();
    stce_10309.stce_10287(stce_9815);
  }
}
template <bool USE_SPARSE_INTERPRET, typename stce_10310, typename adjoint_vector>
  inline void stce_10318(stce_10310 stce_9815,
                                           const stce_10310& stce_9861,
                                           adjoint_vector& stce_10309) {
  stce_9815.index()--;
  stce_10029();;
  stce_10309.stce_10286(stce_10036(stce_9752(stce_9815.index())));
  for (; stce_9815 != stce_9861; --stce_9815) {
    if(USE_SPARSE_INTERPRET && folding::stce_9914(stce_10309[stce_9815.index()])) { continue; }
    DCO_LOG(logDEBUG3) << stce_10309[stce_9815->arg()] << " <=> adj[" << stce_9815->arg() << "] += " << stce_9815->pval() * stce_10309[stce_9815.index()] << " from idx " << stce_9815.index();
    stce_10309.stce_10287(stce_9815);
    if(stce_9815->stce_10149()) {
      stce_10309[stce_9815.index()] = 0;
    }
  }
}
template< typename stce_10310, typename adjoint_vector >
  inline void stce_10319(stce_10310 stce_9815, const stce_10310& stce_9861, adjoint_vector& stce_10309) {
  stce_10309.stce_10286(stce_9815.index());
  for (; stce_9815 != stce_9861; --stce_9815) {
    std::cout << stce_9815.index() << ": " << stce_9815->arg() << ", " << stce_9815->pval() << ", " << stce_10309[stce_9815.index()] << std::endl;
  }
}
template< typename tape_t >
  inline void stce_10319(const tape_t *tape) {
  typename tape_t::iterator stce_9815 = tape->get_position();
  for (; stce_9815 != tape->begin(); --stce_9815) {
    std::cout << stce_9815.index() << ": " << stce_9815->arg() << ", " << stce_9815->pval() << std::endl;
  }
}
}
       
       
       
namespace dco {
namespace internal {
template <typename stce_10320> struct stce_10321 {
  typedef stce_10320 type;
  typedef stce_10320& stce_10322;
  typedef const stce_10320 stce_10323;
};
template <typename stce_9786 > struct stce_10321<std::vector<stce_9786> > {
  typedef std::vector<stce_9786> & type;
  typedef std::vector<stce_9786> & stce_10322;
  typedef std::vector<stce_9786> const& stce_10323;
};
template <stce_9801 stce_10324, typename stce_9786, typename stce_10325 = void>
  struct stce_10326 { };
template <typename stce_9786> struct stce_10326<stce_9802, stce_9786> {
  typedef typename dco::mode<stce_9786>::value_t value_t;
  typedef typename trait_value<stce_9786>::stce_9806 stce_9806;
  typedef typename trait_value<stce_9786>::stce_10327 stce_10327;
  stce_9806 get( stce_9786& stce_9841) { return trait_value<stce_9786>::value(stce_9841); };
  stce_10327 get(const stce_9786& stce_9841) const { return trait_value<stce_9786>::value(stce_9841); };
};
template <typename stce_9786> struct stce_10326<stce_9805, stce_9786> {
  typedef typename dco::mode<stce_9786>::passive_t value_t;
  typedef typename stce_9789<stce_9786>::stce_9806 stce_9806;
  typedef typename stce_9789<stce_9786>::stce_10327 stce_10327;
  stce_9806 get( stce_9786& stce_9841) { return stce_9789<stce_9786>::value(stce_9841); };
  stce_10327 get(const stce_9786& stce_9841) const { return stce_9789<stce_9786>::value(stce_9841); };
};
template <typename stce_9786> struct stce_10326<stce_9803, stce_9786> {
  typedef typename dco::mode<stce_9786>::derivative_t value_t;
  typedef typename stce_9788<stce_9786>::stce_9806 stce_9806;
  typedef typename stce_9788<stce_9786>::stce_10327 stce_10327;
  stce_9806 get( stce_9786& stce_9841) { return stce_9788<stce_9786>::value(stce_9841); };
  stce_10327 get(const stce_9786& stce_9841) const { return stce_9788<stce_9786>::value(stce_9841); };
};
template <typename stce_9786, typename stce_10325> struct stce_10326<stce_9804, stce_9786, stce_10325> {
  typedef typename dco::mode<stce_9786>::derivative_t value_t;
  typedef typename stce_9788<stce_9786, stce_10325>::stce_10328 stce_9806;
  typedef typename stce_9788<stce_9786, stce_10325>::stce_10329 stce_10327;
  stce_10325* stce_10282;
  stce_10326(stce_10325* adjoints) : stce_10282(adjoints) {}
  stce_9806 get( stce_9786& stce_9841) { return stce_9788<stce_9786, stce_10325>::value(stce_9841, stce_10282); };
  stce_10327 get(const stce_9786& stce_9841) const { return stce_9788<stce_9786, stce_10325>::value(stce_9841, stce_10282); };
};
template<stce_9801 stce_10324, class stce_9786, typename stce_10320, typename stce_9787 = void>
  struct stce_10330 : stce_10326<stce_10324, stce_9786, stce_9787> {
  typedef typename stce_10321<stce_10320>::type stce_10275;
  typedef typename stce_10321<stce_10320>::stce_10322 stce_10331;
  typedef typename stce_10321<stce_10320>::stce_10323 stce_10332;
  typedef stce_10326<stce_10324, stce_9786, stce_9787> stce_10333;
  typedef typename stce_10333::value_t value_t;
  stce_10275 stce_10334;
  stce_10330(stce_10332 stce_9840) : stce_10334(const_cast<stce_10331>(stce_9840)) {}
  stce_10330(stce_10275 stce_9840, stce_9787* stce_10335) : stce_10333(stce_10335), stce_10334(stce_9840) {}
  stce_10330 &operator=(const std::vector<value_t> &stce_9886) {
    for (size_t stce_9829 = 0; stce_9829 < stce_10334.size(); ++stce_9829) {
      stce_10333::get(stce_10334[stce_9829]) = stce_9886[stce_9829];
    }
    return *this;
  }
  operator std::vector<value_t>() const {
    std::vector<value_t> stce_9837(stce_10334.size());
    for (size_t stce_9829 = 0; stce_9829 < stce_10334.size(); ++stce_9829) {
      stce_9837[stce_9829] = stce_10333::get(stce_10334[stce_9829]);
    }
    return stce_9837;
  }
  typename stce_10333::stce_9806 operator[](size_t stce_9829) { return stce_10333::get(stce_10334[stce_9829]); }
  typename stce_10333::stce_10327 operator[](size_t stce_9829) const {
    return stce_10333::get(stce_10334[stce_9829]);
  }
  size_t size() const { return stce_10334.size(); }
};
}
}
namespace dco {
template <typename stce_9786> inline typename internal::trait_value<stce_9786>::stce_9806
  value( stce_9786 &stce_9841) { return internal::trait_value<stce_9786>::value(stce_9841); }
template <typename stce_9786> inline typename internal::trait_value<stce_9786>::stce_10327
  value(const stce_9786 &stce_9841) { return internal::trait_value<stce_9786>::value(stce_9841); }
namespace internal {
template <typename stce_9786, typename enable_if>
  struct trait_value {
  typedef typename dco::remove_const<stce_9786>::type type;
  typedef type& stce_9806;
  typedef const type& stce_10327;
  static inline stce_9806 value( type &value) { return value; }
  static inline stce_10327 value(const type &value) { return value; }
};
template <typename stce_9786>
  struct trait_value<stce_9786, typename dco::enable_if<dco::mode<stce_9786>::is_dco_type>::type> {
  typedef typename dco::remove_const<stce_9786>::type type;
  typedef typename dco::mode<type>::value_t& stce_9806;
  typedef typename dco::mode<type>::value_t stce_10327;
  static inline stce_9806 value( type &value) { return value._value(); }
  static inline stce_10327 value(const type &value) { return value._value(); }
};
template <typename stce_9786>
  struct trait_value<std::vector<stce_9786>, typename dco::enable_if<dco::mode<stce_9786>::is_dco_type>::type> {
  typedef std::vector<stce_9786> stce_10275;
  typedef stce_10330<stce_9802, stce_9786, stce_10275> stce_9806;
  typedef const stce_10330<stce_9802, stce_9786, stce_10275> stce_10327;
  static inline stce_9806 value( stce_10275 &stce_9840) { return stce_9806(stce_9840); }
  static inline stce_10327 value(const stce_10275 &stce_9840) { return stce_10327(stce_9840); }
};
template<stce_9801 stce_10324, class stce_9786, typename stce_10336, typename stce_10337>
  struct trait_value<stce_10330<stce_10324, stce_9786, stce_10336, stce_10337>, void > {
  typedef stce_10330<stce_10324, stce_9786, stce_10336, stce_10337> stce_10275;
  typedef stce_10330<stce_9802, typename stce_10275::value_t, stce_10275> stce_9806;
  typedef stce_10330<stce_9802, typename stce_10275::value_t, stce_10275> stce_10327;
  static inline stce_9806 value(const stce_10275 &stce_9840) { return stce_9806(stce_9840); }
};
}
}
       
namespace dco {
template <typename stce_9786> inline typename internal::stce_9789<stce_9786>::stce_9806
  passive_value( stce_9786 &stce_9841) { return internal::stce_9789<stce_9786>::value(stce_9841); }
template <typename stce_9786> inline typename internal::stce_9789<stce_9786>::stce_10327
  passive_value(const stce_9786 &stce_9841) { return internal::stce_9789<stce_9786>::value(stce_9841); }
namespace internal {
template <typename stce_9786, typename enable_if>
  struct stce_9789 {
  typedef typename dco::remove_const<stce_9786>::type type;
  typedef type& stce_9806;
  typedef type stce_10327;
  static inline stce_9806 value( type &value) { return value; }
  static inline stce_10327 value(const type &value) { return value; }
};
template <typename stce_9786>
  struct stce_9789<stce_9786, typename dco::enable_if<dco::mode<stce_9786>::is_dco_type>::type> {
  typedef typename dco::remove_const<stce_9786>::type type;
  typedef typename dco::mode<type>::value_t value_t;
  typedef typename dco::mode<type>::passive_t passive_t;
  typedef passive_t& stce_9806;
  typedef passive_t stce_10327;
  static inline stce_9806 value( type &value) { return stce_9789<value_t>::value(dco::value(value)); }
  static inline stce_10327 value(const type &value) { return stce_9789<value_t>::value(dco::value(value)); }
};
template <typename stce_9786>
  struct stce_9789<std::vector<stce_9786>, typename dco::enable_if<!dco::mode<stce_9786>::is_dco_type>::type> {
  typedef std::vector<stce_9786> stce_10275;
  typedef stce_10275& stce_9806;
  typedef const stce_10275& stce_10327;
  static inline stce_9806 value( stce_10275 &value) { return value; }
  static inline stce_10327 value(const stce_10275 &value) { return value; }
};
template <typename stce_9786>
  struct stce_9789<std::vector<stce_9786>, typename dco::enable_if<dco::mode<stce_9786>::is_dco_type>::type> {
  typedef std::vector<stce_9786> stce_10275;
  typedef stce_10330<stce_9805, stce_9786, stce_10275> stce_9806;
  typedef const stce_10330<stce_9805, stce_9786, stce_10275> stce_10327;
  static inline stce_9806 value( stce_10275 &stce_9840) { return stce_9806(stce_9840); }
  static inline stce_10327 value(const stce_10275 &stce_9840) { return stce_10327(stce_9840); }
};
template<stce_9801 stce_10324, class stce_9786, typename stce_10336, typename stce_10337>
  struct stce_9789<stce_10330<stce_10324, stce_9786, stce_10336, stce_10337>, void > {
  typedef stce_10330<stce_10324, stce_9786, stce_10336, stce_10337> stce_10275;
  typedef stce_10330<stce_9805, typename stce_10275::value_t, stce_10275> stce_9806;
  typedef stce_10330<stce_9805, typename stce_10275::value_t, stce_10275> stce_10327;
  static inline stce_9806 value(const stce_10275 &stce_9840) { return stce_9806(stce_9840); }
};
}
}
       
namespace dco {
template <typename stce_9786> inline typename internal::stce_9788<stce_9786>::stce_9806
  derivative( stce_9786 &stce_9841) { return internal::stce_9788<stce_9786>::value(stce_9841); }
template <typename stce_9786> inline typename internal::stce_9788<stce_9786>::stce_10327
  derivative(const stce_9786 &stce_9841) { return internal::stce_9788<stce_9786>::value(stce_9841); }
template <typename stce_9786, typename stce_9787> inline typename internal::stce_9788<stce_9786, stce_9787>::stce_10328
  derivative( stce_9786 &stce_9841, stce_9787& adjoints) { return internal::stce_9788<stce_9786, stce_9787>::value(stce_9841, &adjoints); }
template <typename stce_9786, typename stce_9787> inline typename internal::stce_9788<stce_9786, stce_9787>::stce_10329
  derivative(const stce_9786 &stce_9841, stce_9787& adjoints) { return internal::stce_9788<stce_9786, stce_9787>::value(stce_9841, &adjoints); }
namespace internal {
template <typename stce_9786, typename stce_9787, typename enable_if>
  struct stce_9788 {
  typedef typename dco::remove_const<stce_9786>::type type;
  typedef type stce_9806;
  typedef type stce_10327;
  static inline stce_9806 value( type &) { return type(); }
  static inline stce_10327 value(const type &) { return type(); }
  static inline stce_9806 value( type &, stce_9787*) { return type(); }
  static inline stce_10327 value(const type &, stce_9787*) { return type(); }
};
template <typename stce_9786>
  struct stce_9788<stce_9786, void, typename dco::enable_if<dco::mode<stce_9786>::is_dco_type>::type> {
  typedef typename dco::remove_const<stce_9786>::type type;
  typedef typename dco::mode<type>::data_t::derivative_t& stce_9806;
  typedef typename dco::mode<type>::data_t::derivative_t& stce_10327;
  static inline stce_9806 value( type &value) { return value.stce_9984(); }
  static inline stce_10327 value(const type &value) { return const_cast<type&>(value).stce_9984(); }
};
template <typename stce_9786, typename stce_10338, typename stce_10339, int stce_10340>
  struct stce_9788<stce_9786, adjoint_vector<stce_10338, stce_10339, stce_10340>, typename dco::enable_if<(stce_10340 == -1)>::type> {
  typedef adjoint_vector<stce_10338, stce_10339, stce_10340> stce_10337;
  typedef typename dco::remove_const<stce_9786>::type type;
  typedef stce_10339& stce_10328;
  typedef stce_10339& stce_10329;
  static inline stce_10328 value( type &value, stce_10337* adjoints) { return adjoints->derivative(value)[0]; }
  static inline stce_10329 value(const type &value, stce_10337* adjoints) { return adjoints->derivative(value)[0]; }
};
template <typename stce_9786, typename stce_10338, typename stce_10339, int stce_10340>
  struct stce_9788<stce_9786, adjoint_vector<stce_10338, stce_10339, stce_10340>, typename dco::enable_if<(stce_10340 > 0)>::type> {
  typedef adjoint_vector<stce_10338, stce_10339, stce_10340> stce_10337;
  typedef typename dco::remove_const<stce_9786>::type type;
  typedef typename stce_10337::value_t& stce_10328;
  typedef typename stce_10337::value_t& stce_10329;
  static inline stce_10328 value( type &value, stce_10337* adjoints) { return adjoints->derivative(value); }
  static inline stce_10329 value(const type &value, stce_10337* adjoints) { return adjoints->derivative(value); }
};
template <typename stce_9786>
  struct stce_9788<std::vector<stce_9786>, void, typename dco::enable_if<!dco::mode<stce_9786>::is_dco_type>::type> {
  typedef std::vector<stce_9786> stce_10275;
  typedef stce_10275 stce_9806;
  typedef stce_10275 stce_10327;
  static inline stce_9806 value( stce_10275 &value) { return stce_9806(value.size(), 0.0); }
  static inline stce_10327 value(const stce_10275 &value) { return stce_9806(value.size(), 0.0); }
};
template <typename stce_9786>
  struct stce_9788<std::vector<stce_9786>, void, typename dco::enable_if<dco::mode<stce_9786>::is_dco_type>::type> {
  typedef std::vector<stce_9786> stce_10275;
  typedef stce_10330<stce_9803, stce_9786, stce_10275> stce_9806;
  typedef stce_10330<stce_9803, stce_9786, stce_10275> stce_10327;
  static inline stce_9806 value( stce_10275 &stce_9840) { return stce_9806(stce_9840); }
  static inline stce_10327 value(const stce_10275 &stce_9840) { return stce_10327(stce_9840); }
};
template <typename stce_9786, typename stce_10338, typename stce_10339, int stce_10340>
  struct stce_9788<std::vector<stce_9786>, adjoint_vector<stce_10338, stce_10339, stce_10340>, typename dco::enable_if<(stce_10340 == -1)>::type> {
  typedef adjoint_vector<stce_10338, stce_10339, stce_10340> stce_10337;
  typedef std::vector<stce_9786> stce_10275;
  typedef stce_10330<stce_9803, stce_9786, stce_10275> stce_9806;
  typedef stce_10330<stce_9803, stce_9786, stce_10275> stce_10327;
  typedef stce_10330<stce_9804, stce_9786, stce_10275, stce_10337> stce_10328;
  typedef stce_10330<stce_9804, stce_9786, stce_10275, stce_10337> stce_10341;
  static inline stce_9806 value( stce_10275 &stce_9840) { return stce_9806(stce_9840); }
  static inline stce_10327 value(const stce_10275 &stce_9840) { return stce_10327(stce_9840); }
  static inline stce_10328 value( stce_10275 &stce_9840, stce_10337* adjoints) { return stce_10328(stce_9840, adjoints); }
  static inline stce_10341 value(const stce_10275 &stce_9840, stce_10337* adjoints) { return stce_10341(stce_9840, adjoints); }
};
template<stce_9801 stce_10324, class stce_9786, typename stce_10336, typename stce_10337>
  struct stce_9788<stce_10330<stce_10324, stce_9786, stce_10336, stce_10337>, void > {
  typedef stce_10330<stce_10324, stce_9786, stce_10336, stce_10337> stce_10275;
  typedef stce_10330<stce_9803, typename stce_10275::value_t, stce_10275> stce_9806;
  typedef stce_10330<stce_9803, typename stce_10275::value_t, stce_10275> stce_10327;
  static inline stce_9806 value(const stce_10275 &stce_9840) { return stce_9806(stce_9840); }
};
}
}
       
namespace dco {
template <typename stce_9786>
  typename internal::stce_9790<stce_9786>::stce_9806 tape(const stce_9786 &stce_9841) {
  return internal::stce_9790<stce_9786>::value(stce_9841);
}
template <typename stce_9786>
  typename internal::stce_9791<stce_9786>::stce_9806 tape_index(const stce_9786 &stce_9841) {
  return internal::stce_9791<stce_9786>::value(stce_9841);
}
namespace internal {
template<typename stce_9786, typename enable_if> struct stce_9790 {
  typedef void* stce_9806;
  static stce_9806 value(const stce_9786&) { return static_cast<void *>(0); }
};
template<typename stce_9786> struct stce_9790<stce_9786, typename dco::enable_if<dco::mode<stce_9786>::is_adjoint_type>::type> {
  typedef typename dco::mode<stce_9786>::tape_t* stce_9806;
  static stce_9806 value(const stce_9786& value) { return value.tape(); }
};
template<typename stce_9786> struct stce_9790<std::vector<stce_9786>, typename dco::enable_if<dco::mode<stce_9786>::is_adjoint_type>::type> {
  typedef typename dco::mode<stce_9786>::tape_t* stce_9806;
  static stce_9806 value(const std::vector<stce_9786>& value) {
    stce_9806 tape = 0;
    for (size_t stce_9829 = 0; stce_9829 < value.size(); stce_9829++) {
      if (stce_9790<stce_9786>::value(value[stce_9829]) != 0) {
        tape = stce_9790<stce_9786>::value(value[stce_9829]);
        break;
      }
    }
    return tape;
  }
};
template<typename stce_9786, typename enable_if> struct stce_9791 {
  typedef DCO_TAPE_INT stce_9806;
  static DCO_TAPE_INT value(const stce_9786&) { return 0; }
};
template<typename stce_9786> struct stce_9791<stce_9786, typename dco::enable_if<dco::mode<stce_9786>::is_adjoint_type>::type> {
  typedef DCO_TAPE_INT stce_9806;
  static DCO_TAPE_INT value(const stce_9786& value) { return value._tape_index(); }
};
template<typename stce_9786> struct stce_9791<std::vector<stce_9786>, void> {
  typedef std::vector<DCO_TAPE_INT> stce_9806;
  static stce_9806 value(const std::vector<stce_9786>& value) {
    stce_9806 tape_index(value.size());
    for (size_t stce_9829 = 0; stce_9829 < value.size(); stce_9829++) {
      tape_index[stce_9829] = value[stce_9829]._tape_index();
    }
    return tape_index;
  }
};
}
}
       
namespace dco {
template <typename tape_t>
struct stce_10342 {
  typedef std::map<std::string, mem_long_t> stce_10343;
  stce_10343 stce_10344;
  tape_t* stce_9945;
  stce_10342(tape_t* tape) : stce_9945(tape) {
    stce_10344["max_tape_memory"] = 0;
    stce_10344["max_adjoint_memory"] = 0;
    stce_10344["max_checkpoint_memory"] = 0;
    stce_10344["max_total_memory"] = 0;
  }
  mem_long_t operator[](std::string stce_10345) { return stce_10344[stce_10345]; }
  void stce_10346() {
    mem_long_t stce_10347 = dco::size_of(stce_9945, tape_t::size_of_stack);
    if (stce_10347 > stce_10344["max_tape_memory"]) {
      stce_10344["max_tape_memory"] = stce_10347;
    }
    stce_10347 = dco::size_of(stce_9945, tape_t::size_of_internal_adjoint_vector);
    if (stce_10347 > stce_10344["max_adjoint_memory"]) {
      stce_10344["max_adjoint_memory"] = stce_10347;
    }
    stce_10347 = dco::size_of(stce_9945, tape_t::size_of_checkpoints);
    if (stce_10347 > stce_10344["max_checkpoint_memory"]) {
      stce_10344["max_checkpoint_memory"] = stce_10347;
    }
    stce_10344["max_total_memory"] = 0;
    for (stce_10343::iterator stce_9829 = stce_10344.begin(); stce_9829 != stce_10344.end(); ++stce_9829) {
      stce_10344["max_total_memory"] += stce_9829->second;
    }
  }
  mem_long_t stce_10348() { return stce_10344["max_tape_memory"]; }
};
}
namespace dco {
namespace internal {
template<memory_model::stce_9770 MEMORY_MODEL,
         typename stce_9792,
         memory_model::stce_9770 stce_9793>
  class tape : public dco::stce_10070<typename stce_9792::ext_adjoint_real_t>,
               public stce_10142<MEMORY_MODEL,
                                     tape<MEMORY_MODEL, stce_9792, stce_9793>,
                                     stce_9793> {
public:
  typedef stce_9792 stce_10349;
  typedef stce_10142<MEMORY_MODEL, tape, stce_9793> stce_10275;
  typedef typename stce_10275::iterator iterator_t;
  typedef iterator_t position_t;
  typedef typename stce_9792::adjoint_real_t stce_9983;
  typedef typename stce_9792::stce_9766 stce_9976;
  typedef typename stce_9792::ext_adjoint_real_t stce_10082;
  typedef typename stce_9792::derivative_t derivative_t;
  typedef tape<MEMORY_MODEL, stce_9792, stce_9793> tape_t;
  typedef stce_9797<typename stce_9792::adjoint_real_t, stce_10275> stce_10350;
  stce_10350 stce_10282;
  stce_10342<tape> stce_10351;
  stce_10342<tape>& stce_10352() { return stce_10351; }
  const stce_10342<tape>& stce_10352() const { return stce_10351; }
  tape& get_tape() { return *this; }
private:
  int stce_10353;
  bool stce_10354;
  bool stce_10355;
  DCO_TAPE_INT stce_10298;
  tape_options stce_10188;
  bool stce_10356;
  void stce_10302(const iterator_t &stce_9860, const iterator_t &stce_9861 = stce_10275::start()) {
    stce_10282.stce_10294(stce_9860.index(), stce_9861.index());
  }
  void stce_10357() {
    stce_10282.resize(this->stce_10176());
    stce_10282.stce_10295();
  }
  template <typename stce_10310, typename adjoint_vector>
    inline void stce_10358(stce_10310 stce_9815, const stce_10310& stce_9861,
                                      adjoint_vector& stce_10309) {
    if (stce_10356) { dco::interpret< true >(stce_9815, stce_9861, stce_10309); }
    else { dco::interpret< false>(stce_9815, stce_9861, stce_10309); }
  }
  template <typename stce_10310, typename adjoint_vector>
    inline void stce_10359(stce_10310 stce_9815, const stce_10310& stce_9861,
                                      adjoint_vector& stce_10309) {
    if (stce_10356) { dco::stce_10318< true >(stce_9815, stce_9861, stce_10309); }
    else { dco::stce_10318< false>(stce_9815, stce_9861, stce_10309); }
  }
public:
  const tape_options& stce_10162() const { return stce_10188; }
  bool stce_10360() { return this->start() == this->current(); }
  tape(tape_options stce_10162 = tape_options()) :
    stce_10275(stce_10162, *this), stce_10282(1, *this),
    stce_10351(this),
    stce_10353(dco::build_info::COMPATIBILITY_VERSION),
    stce_10354(false), stce_10355(true), stce_10298(0), stce_10188(stce_10162),
    stce_10356(false) { }
  ~tape() {
    for(typename std::vector<stce_10361>::iterator stce_10156 = stce_10362.begin();
        stce_10156 != stce_10362.end(); ++stce_10156) {
      stce_10156->stce_10363();
    }
  }
  bool is_compatible(const int stce_10364) {
    return stce_10364 == stce_10353;
  }
  bool is_active() { return stce_9792::stce_9760 ? stce_10355 : true; }
  typedef helper::stce_10076<tape, stce_10082> stce_10365;
  static void stce_10366(stce_10365* stce_10367) { stce_10367->get_tape()->stce_10356 = true; }
  static void stce_10368(stce_10365* stce_10367) { stce_10367->get_tape()->stce_10356 = false; }
  void set_sparse_interpret() {
    if (stce_10356 == false) {
      stce_10365* stce_10367 = this->create_callback_object<stce_10365>();
      this->insert_callback(stce_10368, stce_10367);
    }
    stce_10356 = true;
  }
  void unset_sparse_interpret() {
    if (stce_10356 == true) {
      stce_10365* stce_10367 = this->create_callback_object<stce_10365>();
      this->insert_callback(stce_10366, stce_10367);
    }
    stce_10356 = false;
  }
  bool& sparse_interpret() { return stce_10356; }
  void switch_to_active() {
    if (!stce_9792::stce_9760) {
      throw dco::exception::create<std::runtime_error>("activity check disabled but switched", "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_tape.hpp", 161);
    }
    if (!stce_10355) { stce_10355 = true; }
  }
  void switch_to_passive() {
    if (!stce_9792::stce_9760) {
      throw dco::exception::create<std::runtime_error>("activity check disabled but switched", "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_tape.hpp", 168);
    }
    if (stce_10355) { stce_10355 = false; }
  }
  template <bool stce_10369>
  DCO_TAPE_INT stce_10370() {
    if(!stce_10369 && this->empty()) { return stce_10275::stce_9921().stce_10160(); }
    else {
      typename stce_10275::stce_9917 stce_10371(stce_10275::stce_9921());
      stce_10371.insert(0, 0);
      return stce_10371.index();
    }
  }
  template <typename stce_9924> void register_variable(stce_9924& stce_9841) {
    stce_9924::register_variable(stce_9841, stce_10370< false>(), this);
  }
  template <typename stce_9924> void register_variable(std::vector<stce_9924>& stce_9841) {
    this->register_variable(stce_9841.data(), stce_9841.size());
  }
  template <typename stce_10372> void register_variable(stce_10372 first, stce_10372 stce_10373) {
    while(first != stce_10373) {
      this->register_variable(*first++);
    }
  }
  template <typename stce_10372, typename stce_10374>
    void register_variable(stce_10372 stce_10375, stce_10372 stce_10376, stce_10374 stce_10377) {
    while(stce_10375 != stce_10376) {
      *stce_10377 = *stce_10375++;
      this->register_variable(*stce_10377++);
    }
  }
  template <typename stce_9924> void register_variable(stce_9924* stce_9841, size_t stce_10094) {
    for (size_t stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
      stce_9924::register_variable(stce_9841[stce_9829], stce_10370< false>(), this);
    }
  }
  template<typename stce_9924> void register_output_variable(stce_9924 &stce_9841) { stce_9841 = 1*stce_9841; }
  template<typename stce_9924> void register_output_variable(std::vector<stce_9924>& stce_9841) {
    this->register_output_variable(stce_9841.data(), stce_9841.size());
  }
  template<typename stce_9924> void register_output_variable(stce_9924* stce_9841, size_t stce_10094) {
    for (size_t stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
      this->register_output_variable(stce_9841[stce_9829]);
    }
  }
  template <typename stce_10372> void register_output_variable(stce_10372 first, stce_10372 stce_10373) {
    while(first != stce_10373) {
      this->register_output_variable(*first++);
    }
  }
  template <stce_9801 stce_10378, typename stce_9786, typename stce_10320>
    void register_output_variable(stce_10330<stce_10378, stce_9786, stce_10320> stce_9841) {
    for (size_t stce_9829 = 0; stce_9829 < stce_9841.size(); ++stce_9829) {
      this->register_output_variable(stce_9841[stce_9829]);
    }
  }
  void reset_to(const iterator_t &stce_9861) {
    stce_10257(stce_9861);
    stce_10379(stce_9861);
  }
  void reset() { reset_to(stce_10275::start()); }
  inline iterator_t current() const { return get_position(); }
  inline iterator_t begin() const { return stce_10275::start(); }
  stce_10350& adjoints() { return stce_10282; }
  void interpret_adjoint(interpretation_settings stce_10300 = interpretation_settings()) {
    iterator_t stce_9861 = stce_10275::start();
    stce_10301(get_position(), stce_9861, stce_10300);
  }
  void interpret_adjoint_to(const iterator_t &stce_9861) {
    interpretation_settings stce_10300;
    stce_10301(get_position(), stce_9861, stce_10300);
  }
  void interpret_adjoint_from(const iterator_t &stce_9860) {
    iterator_t stce_9861 = stce_10275::start();
    interpretation_settings stce_10300;
    stce_10301(stce_9860, stce_9861, stce_10300);
  }
  void interpret_adjoint_from_to(const iterator_t &stce_9860, const iterator_t &stce_9861) {
    interpretation_settings stce_10300;
    stce_10301(stce_9860, stce_9861, stce_10300);
  }
  void interpret_adjoint_and_reset_to(const iterator_t &stce_9861) {
    iterator_t stce_9860(get_position());
    interpretation_settings stce_10300;
    stce_10300.reset = true;
    stce_10300.stce_10056 = true;
    stce_10301(stce_9860, stce_9861, stce_10300);
    stce_10257(stce_9861);
    stce_10379(stce_9861);
  }
  void interpret_adjoint_and_zero_adjoints_to(const iterator_t &stce_9861) {
    iterator_t stce_9860(get_position());
    interpretation_settings stce_10300;
    stce_10300.reset = false;
    stce_10300.stce_10056 = true;
    stce_10301(stce_9860, stce_9861, stce_10300);
  }
  void interpret_adjoint_and_zero_adjoints_from_to(const iterator_t &stce_9860, const iterator_t &stce_9861) {
    interpretation_settings stce_10300;
    stce_10300.reset = false;
    stce_10300.stce_10056 = true;
    stce_10301(stce_9860, stce_9861, stce_10300);
  }
  void zero_adjoints() {
    iterator_t stce_9861 = stce_10275::start();
    stce_10302(get_position(), stce_9861);
  }
  void zero_adjoints_to(const iterator_t &stce_9861) {
    stce_10302(get_position(), stce_9861);
  }
  void zero_adjoints_from(const iterator_t &stce_9860) {
    iterator_t stce_9861 = stce_10275::start();
    stce_10302(stce_9860, stce_9861);
  }
  void zero_adjoints_from_to(const iterator_t &stce_9860, const iterator_t &stce_9861) {
    stce_10302(stce_9860, stce_9861);
  }
  void stce_10257(const iterator_t &stce_9861) {
    bool stce_10380 = stce_10282.stce_10283(stce_9861.index());
    stce_10282.resize(stce_9861.index()+1);
    if(stce_10380) { stce_10282.stce_10295(); }
    stce_10275::erase(stce_9861);
  }
public:
  iterator_t get_position() const {
    return stce_10275::current();
  }
  stce_10082 &stce_10381(const DCO_TAPE_INT tape_index) {
    stce_9983 &stce_10073 = _adjoint(tape_index);
    stce_10082 *stce_10382 = reinterpret_cast<stce_10082 *>(&stce_10073);
    return stce_10382[stce_10298];
  }
  stce_10082 stce_10381(const DCO_TAPE_INT tape_index) const {
    stce_9983 stce_10073(_adjoint(tape_index));
    stce_10082 *stce_10382 = reinterpret_cast<stce_10082 *>(&stce_10073);
    return stce_10382[stce_10298];
  }
  stce_9983 &_adjoint(DCO_TAPE_INT tape_index) {
    if(!stce_10282.stce_10283(get_position().index()+1))
      stce_10357();
    return stce_10282[tape_index];
  }
  stce_9983 _adjoint(const DCO_TAPE_INT tape_index) const {
    return stce_10282.stce_10283(tape_index)?stce_10282[tape_index]: 0;
  }
  enum stce_10383 {
    size_of_stack = 1,
    size_of_allocated_stack = 2,
    size_of_internal_adjoint_vector = 4,
    size_of_checkpoints = 8,
    stce_10066 = size_of_stack | size_of_internal_adjoint_vector
  };
  mem_long_t stce_10384() const {
    mem_long_t stce_10385 = 0;
    for (size_t stce_9829 = 0; stce_9829 < stce_10362.size(); stce_9829++) {
      stce_10385 += stce_10362[stce_9829].stce_10316()->size_in_byte();
    }
    return stce_10385;
  }
  mem_long_t stce_10067(const int stce_10386 = stce_10066) const {
    mem_long_t stce_10387 = 0;
    if (stce_10386 & size_of_allocated_stack) {
      stce_10387 += static_cast<size_t>(stce_10275::stce_10173());
    }
    else if (stce_10386 & size_of_stack) {
      stce_10387 += static_cast<size_t>(stce_10275::size_in_byte());
    }
    if (stce_10386 & size_of_internal_adjoint_vector) {
      stce_10387 += stce_10282.stce_10291();
    }
    if (stce_10386 & size_of_checkpoints) {
      stce_10387 += stce_10384();
    }
    return stce_10387;
  }
  static tape_t *create(tape_options stce_10162 = tape_options()) {
    tape_t* tape = new tape_t(stce_10162);
    return tape;
  }
  static void remove(tape_t *&tape) {
    if (tape == 0)
      return;
    delete tape;
    tape = 0;
  }
  virtual stce_10082 stce_10071(const DCO_TAPE_INT stce_9830) {
    return stce_10381(stce_9830);
  }
  virtual void stce_10072(const DCO_TAPE_INT stce_9830, const stce_10082 &stce_10073) {
    stce_10381(stce_9830) += stce_10073;
  }
  typedef dco::helper::stce_10076<tape_t, typename stce_9792::ext_adjoint_real_t> stce_10388;
  template <typename stce_10389>
    class stce_10390 {
public:
    typedef void (*stce_10391)(tape_t &stce_10392, const interpretation_settings &stce_9890, stce_10389 *stce_10393);
    typedef void (*stce_10394)(tape_t &stce_10392, stce_10389 *stce_10393);
    typedef void (*stce_10395)(stce_10389 *stce_10393);
  };
  class stce_10396 {
public:
    virtual void stce_10397(tape_t &stce_10392, const interpretation_settings &stce_9890, stce_10388 *stce_10393) = 0;
    virtual ~stce_10396() {};
  };
  template <typename stce_10389>
    class stce_10398 : public stce_10396 {
private:
    union {
      typename stce_10390<stce_10389>::stce_10395 stce_10399;
      typename stce_10390<stce_10389>::stce_10394 stce_10400;
      typename stce_10390<stce_10389>::stce_10391 stce_10401;
    } stce_10399;
    int stce_10402;
public:
    stce_10398(typename stce_10390<stce_10389>::stce_10395 stce_10403) : stce_10402(0) {
      stce_10399.stce_10399 = stce_10403;
    }
    stce_10398(typename stce_10390<stce_10389>::stce_10394 stce_10403) : stce_10402(1) {
      stce_10399.stce_10400 = stce_10403;
    }
    stce_10398(typename stce_10390<stce_10389>::stce_10391 stce_10403) : stce_10402(2) {
      stce_10399.stce_10401 = stce_10403;
    }
    void stce_10397(tape_t &stce_10392, const interpretation_settings &stce_9890, stce_10388 *stce_10393) {
      stce_10389 *stce_10404 = static_cast<stce_10389 *>(stce_10393);
      switch (stce_10402) {
      case 0:
        stce_10399.stce_10399(stce_10404);
        break;
      case 1:
        stce_10399.stce_10400(stce_10392, stce_10404);
        break;
      case 2:
        stce_10399.stce_10401(stce_10392, stce_9890, stce_10404);
        break;
      default:
        if (helper::stce_9817) {
          throw dco::exception::create<std::runtime_error>("unknown callback to run.");
        }
        break;
      }
    }
    ~stce_10398() {}
  };
  class stce_10361 {
    stce_10388 *stce_10393;
    stce_10396 *stce_10405;
    iterator_t stce_10406;
public:
    stce_10361(const iterator_t& stce_10150) :
      stce_10393(0), stce_10405(0), stce_10406(stce_10150){
    }
    bool operator<(const stce_10361& stce_9926) const
    {
      return stce_10406 < stce_9926.stce_10406;
    }
    void stce_10363() {
      delete stce_10393;
      if (stce_10405)
        delete stce_10405;
    }
    template<typename stce_10389, typename stce_10407>
      void stce_10408(stce_10407 stce_10403) {
      if (stce_10405)
        throw dco::exception::create<std::runtime_error>(
                                                         "currently not supported to insert external_adjoint_object_bases twice.");
      stce_10405 = new stce_10398<stce_10389>(stce_10403);
    }
    stce_10388 *&stce_10316() {
      return stce_10393;
    }
    stce_10388 * stce_10316() const {
      return stce_10393;
    }
    iterator_t &stce_10315() {
      return stce_10406;
    }
    iterator_t stce_10315() const {
      return stce_10406;
    }
    void stce_10397(tape_t &stce_10392, const interpretation_settings &stce_9890) {
      if (stce_10405)
        stce_10405->stce_10397(stce_10392, stce_9890, stce_10393);
    }
  };
  std::vector<stce_10361> stce_10362;
  template <class stce_10409>
   void stce_10410(stce_10409 *stce_10393) {
       stce_10361 stce_10411(stce_10275::current());
       stce_10362.push_back(stce_10411);
       stce_10362.back().stce_10316() = stce_10393;
       stce_10393->set_tape(this);
       typedef typename stce_10409::stce_10120 stce_10412;
       typedef dco::stce_10130::stce_10133<tape_t,stce_10412> stce_10413;
       stce_10413* stce_10129 = stce_10393;
       insert_callback(&dco::stce_10130::stce_10128<tape_t,stce_10412>, stce_10129);
   }
  template<class stce_10409, typename stce_10407>
    void insert_callback(stce_10407 stce_10405, stce_10409 *stce_10367) {
    if (stce_10362.back().stce_10316() == stce_10367) {
      stce_10362.back().template stce_10408<stce_10409>(stce_10405);
      stce_10362.back().stce_10315() = stce_10275::current();
      stce_10370< true>();
    } else {
      throw dco::exception::create<std::runtime_error>("please always insert most recently created external function object.");
    }
  }
  template<class stce_10409, typename FCN_PARAMETERS>
    stce_10409 *create_callback_object(const FCN_PARAMETERS &parameters) {
    const stce_9842<FCN_PARAMETERS> stce_10414(parameters);
    stce_10361 stce_10411(stce_10275::current());
    stce_10362.push_back(stce_10411);
    stce_10409 *stce_10393 = stce_10414.template create<stce_10409>();
    stce_10362.back().stce_10316() = stce_10393;
    stce_10393->set_tape(this);
    return stce_10393;
  }
  template<class stce_10409>
    stce_10409 *create_callback_object() {
    void *stce_9993;
    return create_callback_object<stce_10409>(stce_9993);
  }
  template <class stce_10409>
    inline stce_10409* create_ACM_object() {
    stce_10409* stce_9837 = create_callback_object<stce_10409>();
    typedef typename stce_10409::stce_10120 stce_10412;
    typedef dco::ACM::baseclass<tape_t,stce_10412> stce_10415;
    stce_10415* stce_10129 = stce_9837;
    insert_callback(&dco::ACM::stce_10128<tape_t,stce_10412>, stce_10129);
    return stce_9837;
  }
  template <class stce_10409, typename FCN_PARAMETERS>
    inline stce_10409 *create_ACM_object(const FCN_PARAMETERS &parameters) {
    const stce_9842<FCN_PARAMETERS> stce_10414(parameters);
    stce_10361 stce_10411(stce_10275::current());
    stce_10362.push_back(stce_10411);
    stce_10409 *stce_10393 = stce_10414.template create<stce_10409>();
    stce_10362.back().stce_10316() = stce_10393;
    stce_10393->set_tape(this);
    typedef typename stce_10409::stce_10120 stce_10412;
    typedef dco::ACM::baseclass<tape_t,stce_10412> stce_10415;
    stce_10415* stce_10129 = stce_10393;
    insert_callback(&dco::ACM::stce_10128<tape_t,stce_10412>, stce_10129);
    return stce_10393;
  }
    template <class stce_10409>
    void insert_ACM_object(stce_10409 *stce_10393) {
        stce_10361 stce_10411(stce_10275::current());
        stce_10362.push_back(stce_10411);
        stce_10362.back().stce_10316() = stce_10393;
        stce_10393->set_tape(this);
        typedef typename stce_10409::stce_10120 stce_10412;
        typedef dco::ACM::baseclass<tape_t,stce_10412> stce_10415;
        stce_10415* stce_10129 = stce_10393;
        insert_callback(&dco::ACM::stce_10128<tape_t,stce_10412>, stce_10129);
    }
  void stce_10379(const iterator_t &stce_9861) {
    typedef typename std::vector<stce_10361>::iterator stce_10314;
    stce_10314 stce_10045 = stce_10362.begin();
    for (; stce_10045 != stce_10362.end() && stce_10045->stce_10315().index() < stce_9861.index(); ++stce_10045) {}
    for (stce_10314 stce_10156 = stce_10045; stce_10156 != stce_10362.end(); ++stce_10156)
      stce_10156->stce_10363();
    stce_10362.erase(stce_10045, stce_10362.end());
  }
public:
  void write_to_dot(const std::string &filename = "tape.dot")
  {
    write_to_dot(stce_10275::current(), stce_10275::start(), filename);
  }
  void write_to_dot(iterator_t stce_9860)
  {
    write_to_dot(stce_9860, stce_10275::start());
  }
  void write_to_dot(iterator_t stce_9815, iterator_t stce_9861,
                   const std::string &filename = "tape.dot") {
    stce_9846 stce_9847(filename);
    stce_10308(stce_9815, stce_9861 , stce_10282, stce_10362, stce_9847);
  }
  void write_to_csv(const std::string &filename = "tape.csv")
  {
    write_to_csv(stce_10275::current(), stce_10275::start(), filename);
  }
  void write_to_csv(iterator_t stce_9860)
  {
    write_to_csv(stce_9860, stce_10275::start());
  }
  void write_to_csv(iterator_t stce_9815, iterator_t stce_9861,
                   const std::string &filename = "tape.csv") {
    if(stce_10362.size() != 0)
      throw dco::exception::create<std::runtime_error>
        ("write_to_csv not implemented for callbacks.", "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_tape.hpp", 679);
    stce_10317(stce_9815, stce_9861, filename);
  }
    void stce_10416(const iterator_t& stce_9815, stce_10361& stce_10417,
                          const interpretation_settings &stce_10300)
  {
    if(stce_10417.stce_10315() != stce_9815)
      throw dco::exception::create<std::runtime_error>("invoking callback at wrong position", "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_tape.hpp", 688);
    if (stce_10300.reset) {
      stce_10257(stce_9815);
      stce_10370< true>();
    }
    int stce_9835 = dco::helper::stce_9834<typename stce_9792::adjoint_real_t>::stce_9835;
    for (DCO_TAPE_INT stce_10418 = 0; stce_10418 < stce_9835; stce_10418++) {
      stce_10298 = stce_10418;
      stce_10417.stce_10316()->stce_10078 = this;
      stce_10417.stce_10397(*this, stce_10300);
    }
  }
  template<class stce_10419>
    void stce_10416(stce_10419 &stce_10382, const iterator_t& stce_9815, stce_10361& stce_10417,
                          const interpretation_settings &stce_10300)
  {
    if(stce_10417.stce_10315() != stce_9815)
      throw dco::exception::create<std::runtime_error>("invoking callback at wrong position", "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_tape.hpp", 712);
    if (stce_10300.reset) {
      stce_10257(stce_9815);
      stce_10370< true>();
    }
    int stce_9835 = dco::helper::stce_9834<typename stce_9792::adjoint_real_t>::stce_9835;
    for (DCO_TAPE_INT stce_10418 = 0; stce_10418 < stce_9835; stce_10418++) {
      stce_10298 = stce_10418;
      stce_10417.stce_10316()->stce_10078 = &stce_10382;
      stce_10417.stce_10397(*this, stce_10300);
    }
  }
  struct stce_10420 {
    tape* stce_9945;
    bool stce_10356;
    stce_10420(tape* stce_9897) : stce_9945(stce_9897), stce_10356(stce_9897->sparse_interpret()) {}
    ~stce_10420() { stce_9945->sparse_interpret() = stce_10356; }
  };
  void stce_10301(iterator_t stce_9815, const iterator_t &stce_9861,
                                   const interpretation_settings &stce_10300) {
    stce_10420 stce_10421(this);
    if (stce_9815 > stce_10275::current()) {
      throw dco::exception::create<std::runtime_error>
        ("you try to use a tape position outside of the current tape.", "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_tape.hpp", 747);
    }
    if (stce_9861 > stce_9815) {
      throw dco::exception::create<std::runtime_error>
        ("adjoint interpretation: from < to.", "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_tape.hpp", 752);
    }
    stce_10357();
    typedef typename std::vector<stce_10361>::reverse_iterator stce_10314;
    for (stce_10314 stce_10156 = stce_10362.rbegin();
         stce_10156 != stce_10362.rend() && stce_9861 <= stce_10156->stce_10315();
         ++stce_10156) {
      const iterator_t& stce_10422 = stce_10156->stce_10315();
      if(stce_9815 < stce_10422) { continue; }
      stce_10275::stce_10174(stce_9815);
      stce_10358(stce_9815, stce_10422, stce_10282);
      stce_9815 = stce_10422;
      std::size_t stce_10423 = std::distance(stce_10362.rbegin(), stce_10156);
      if(stce_10300.stce_10056)
        stce_10282.stce_10294(stce_9815.index(), stce_10422.index());
      stce_10275::stce_10175();
      stce_10416(stce_9815, *stce_10156, stce_10300);
      stce_10156 = stce_10362.rbegin() + stce_10423;
      if (stce_10300.reset) {
        stce_10257(stce_9815);
        stce_10156->stce_10363();
        stce_10362.erase( --(stce_10156.base()) );
      }
    }
    stce_10275::stce_10174(stce_9815);
    stce_10358(stce_9815, stce_9861, stce_10282);
    if(stce_10300.stce_10056)
      stce_10282.stce_10294(stce_9815.index(), stce_9861.index());
    stce_10275::stce_10175();
  }
  template<class stce_10419>
    void stce_10305(stce_10419 &stce_10382,
                                       iterator_t stce_9815, const iterator_t &stce_9861,
                                       const interpretation_settings &stce_10300)
  {
    if (stce_9861 > stce_9815) {
      throw dco::exception::create<std::runtime_error>("adjoint interpretation: from < to.", "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_tape.hpp", 817);
    }
    if (stce_9815 > stce_10275::current()) {
      throw dco::exception::create<std::runtime_error>("you try to use a tape position outside of the current tape.", "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_tape.hpp", 820);
    }
    typedef typename std::vector<stce_10361>::reverse_iterator stce_10314;
    for (stce_10314 stce_10156 = stce_10362.rbegin(); stce_10156 != stce_10362.rend() && stce_9861 <= stce_10156->stce_10315();
         ++stce_10156) {
      const iterator_t& stce_10422 = stce_10156->stce_10315();
      if(stce_9815 < stce_10422)
        continue;
      stce_10275::stce_10174(stce_9815);
      if(stce_10300.stce_10056)
        stce_10359(stce_9815, stce_10422, stce_10382);
      else {
        stce_10358(stce_9815, stce_10422, stce_10382);
      }
      stce_9815 = stce_10422;
      std::size_t stce_10423 = std::distance(stce_10362.rbegin(), stce_10156);
      stce_10416(stce_10382, stce_9815, *stce_10156, stce_10300);
      if (stce_9815 != stce_10275::start()) {
        --stce_9815;
      }
      stce_10156 = stce_10362.rbegin() + stce_10423;
      if (stce_10300.reset) {
        stce_10257(stce_9815);
        stce_10156->stce_10363();
        stce_10362.erase( --(stce_10156.base()) );
      }
    }
    stce_10275::stce_10174(stce_9815);
    if(stce_10300.stce_10056) { stce_10359(stce_9815, stce_9861, stce_10382); }
    else { stce_10358(stce_9815, stce_9861, stce_10382); }
    if (stce_10300.reset) { stce_10257(stce_9815); }
    stce_10275::stce_10175();
  }
};
}
}
       
namespace dco {
  namespace stce_10424 {
    template<class stce_9776>struct stce_10425{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { return static_cast<stce_9776>(stce_9904 + stce_9905); } template<class stce_10426,class stce_10022>static inline const stce_9776 stce_10020(const stce_9776 &_value , const stce_10426 &stce_9904 , const stce_10022 &stce_9905 ) { (void) _value; (void)stce_9904; (void)stce_9905; return static_cast<stce_9776>(1.0); } template<class stce_10426,class stce_10022>static inline const stce_9776 stce_10021(const stce_9776 &_value , const stce_10426 &stce_9904 , const stce_10022 &stce_9905 ) { (void) _value; (void)stce_9904; (void)stce_9905; return static_cast<stce_9776>(1.0); } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << stce_9950 << "+" << stce_9951; return stce_9837.str(); } static std::string stce_9906() { return "1.0"; } static std::string stce_9908() { return "1.0"; } };
    template<class stce_9776>struct stce_10427{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { return static_cast<stce_9776>(stce_9904 - stce_9905); } template<class stce_10426,class stce_10022>static inline const stce_9776 stce_10020(const stce_9776 &_value , const stce_10426 &stce_9904 , const stce_10022 &stce_9905 ) { (void) _value; (void)stce_9904; (void)stce_9905; return static_cast<stce_9776>(1.0); } template<class stce_10426,class stce_10022>static inline const stce_9776 stce_10021(const stce_9776 &_value , const stce_10426 &stce_9904 , const stce_10022 &stce_9905 ) { (void) _value; (void)stce_9904; (void)stce_9905; return static_cast<stce_9776>(-1.0); } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << stce_9950 << "-" << stce_9951; return stce_9837.str(); } static std::string stce_9906() { return "1.0"; } static std::string stce_9908() { return "-1.0"; } };
    template<class stce_9776>struct stce_10428{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { return static_cast<stce_9776>(stce_9904 * stce_9905); } template<class stce_10426,class stce_10022>static inline const stce_9776 stce_10020(const stce_9776 &_value , const stce_10426 &stce_9904 , const stce_10022 &stce_9905 ) { (void) _value; (void)stce_9904; (void)stce_9905; return static_cast<stce_9776>(stce_9905._value()); } template<class stce_10426,class stce_10022>static inline const stce_9776 stce_10021(const stce_9776 &_value , const stce_10426 &stce_9904 , const stce_10022 &stce_9905 ) { (void) _value; (void)stce_9904; (void)stce_9905; return static_cast<stce_9776>(stce_9904._value()); } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << stce_9950 << "*" << stce_9951; return stce_9837.str(); } static std::string stce_9906() { return "arg2._value()"; } static std::string stce_9908() { return "arg1._value()"; } };
    template<class stce_9776>struct stce_10429{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { return static_cast<stce_9776>(stce_9904 / stce_9905); } template<class stce_10426,class stce_10022>static inline const stce_9776 stce_10020(const stce_9776 &_value , const stce_10426 &stce_9904 , const stce_10022 &stce_9905 ) { (void) _value; (void)stce_9904; (void)stce_9905; return static_cast<stce_9776>(1.0 / stce_9905._value()); } template<class stce_10426,class stce_10022>static inline const stce_9776 stce_10021(const stce_9776 &_value , const stce_10426 &stce_9904 , const stce_10022 &stce_9905 ) { (void) _value; (void)stce_9904; (void)stce_9905; return static_cast<stce_9776>(-_value / stce_9905._value()); } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << stce_9950 << "/" << stce_9951; return stce_9837.str(); } static std::string stce_9906() { return "1.0 / arg2._value()"; } static std::string stce_9908() { return "-_value / arg2._value()"; } };
    template<class stce_9776>struct stce_10430{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { return stce_9904 + stce_9905; } template<class stce_10426>static inline const stce_9776 stce_10020(const stce_9776 &_value , const stce_10426 &stce_9904 , const stce_9776 &stce_9905 ) { (void)_value; (void)stce_9904; (void)stce_9905; return 1.0; } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << stce_9950 << "+" << stce_9951; return stce_9837.str(); } static std::string stce_9906() { return "1.0"; } }; template<class stce_9776>struct stce_10431{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { return stce_9904 + stce_9905; } template<class stce_10022>static inline const stce_9776 stce_10021(const stce_9776 &_value , const stce_9776 &stce_9904 , const stce_10022 &stce_9905 ) { (void) _value; (void)stce_9904; (void)stce_9905; return 1.0; } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << stce_9950 << "+" << stce_9951; return stce_9837.str(); } static std::string stce_9908() { return "1.0"; } };
    template<class stce_9776>struct stce_10432{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { return stce_9904 - stce_9905; } template<class stce_10426>static inline const stce_9776 stce_10020(const stce_9776 &_value , const stce_10426 &stce_9904 , const stce_9776 &stce_9905 ) { (void)_value; (void)stce_9904; (void)stce_9905; return 1.0; } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << stce_9950 << "-" << stce_9951; return stce_9837.str(); } static std::string stce_9906() { return "1.0"; } }; template<class stce_9776>struct stce_10433{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { return stce_9904 - stce_9905; } template<class stce_10022>static inline const stce_9776 stce_10021(const stce_9776 &_value , const stce_9776 &stce_9904 , const stce_10022 &stce_9905 ) { (void) _value; (void)stce_9904; (void)stce_9905; return -1.0; } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << stce_9950 << "-" << stce_9951; return stce_9837.str(); } static std::string stce_9908() { return "-1.0"; } };
    template<class stce_9776>struct stce_10434{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { return stce_9904 * stce_9905; } template<class stce_10426>static inline const stce_9776 stce_10020(const stce_9776 &_value , const stce_10426 &stce_9904 , const stce_9776 &stce_9905 ) { (void)_value; (void)stce_9904; (void)stce_9905; return stce_9905; } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << stce_9950 << "*" << stce_9951; return stce_9837.str(); } static std::string stce_9906() { return "arg2"; } }; template<class stce_9776>struct stce_10435{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { return stce_9904 * stce_9905; } template<class stce_10022>static inline const stce_9776 stce_10021(const stce_9776 &_value , const stce_9776 &stce_9904 , const stce_10022 &stce_9905 ) { (void) _value; (void)stce_9904; (void)stce_9905; return stce_9904; } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << stce_9950 << "*" << stce_9951; return stce_9837.str(); } static std::string stce_9908() { return "arg1"; } };
    template<class stce_9776>struct stce_10436{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { return stce_9904 / stce_9905; } template<class stce_10426>static inline const stce_9776 stce_10020(const stce_9776 &_value , const stce_10426 &stce_9904 , const stce_9776 &stce_9905 ) { (void)_value; (void)stce_9904; (void)stce_9905; return 1.0 / stce_9905; } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << stce_9950 << "/" << stce_9951; return stce_9837.str(); } static std::string stce_9906() { return "1.0 / arg2"; } }; template<class stce_9776>struct stce_10437{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { return stce_9904 / stce_9905; } template<class stce_10022>static inline const stce_9776 stce_10021(const stce_9776 &_value , const stce_9776 &stce_9904 , const stce_10022 &stce_9905 ) { (void) _value; (void)stce_9904; (void)stce_9905; return -(stce_9904 / stce_9905._value()) / stce_9905._value(); } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << stce_9950 << "/" << stce_9951; return stce_9837.str(); } static std::string stce_9908() { return "-(arg1 / arg2._value()) / arg2._value()"; } };
    using std::sin; template<class stce_9776>struct stce_10438{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return sin(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(cos(stce_9841._value())); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "sin" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("cos(x._value())"); return stce_9837; } };
    using std::cos; template<class stce_9776>struct stce_10439{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return cos(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(-sin(stce_9841._value())); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "cos" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("-sin(x._value())"); return stce_9837; } };
    using std::tan; template<class stce_9776>struct stce_10440{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return tan(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>((1.0 + (tan(stce_9841._value())*tan(stce_9841._value())))); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "tan" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("(1.0 + (tan(x._value())*tan(x._value())))"); return stce_9837; } };
    using std::cosh; template<class stce_9776>struct stce_10441{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return cosh(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(sinh(stce_9841._value())); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "cosh" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("sinh(x._value())"); return stce_9837; } };
    using std::sinh; template<class stce_9776>struct stce_10442{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return sinh(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(cosh(stce_9841._value())); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "sinh" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("cosh(x._value())"); return stce_9837; } };
    using std::asin; template<class stce_9776>struct stce_10443{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return asin(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(1 / sqrt(1.0 - stce_9841._value()*stce_9841._value())); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "asin" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("1 / sqrt(1.0 - x._value()*x._value())"); return stce_9837; } };
    using std::acos; template<class stce_9776>struct stce_10444{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return acos(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(-1 / sqrt(1.0 - stce_9841._value()*stce_9841._value())); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "acos" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("-1 / sqrt(1.0 - x._value()*x._value())"); return stce_9837; } };
    using std::exp; template<class stce_9776>struct stce_10445{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return exp(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(exp(stce_9841._value())); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "exp" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("exp(x._value())"); return stce_9837; } };
    using std::atan; template<class stce_9776>struct stce_10446{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return atan(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(1.0 / (1.0 + stce_9841._value()*stce_9841._value())); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "atan" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("1.0 / (1.0 + x._value()*x._value())"); return stce_9837; } };
    using std::tanh; template<class stce_9776>struct stce_10447{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return tanh(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(1.0 - tanh(stce_9841._value())*tanh(stce_9841._value())); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "tanh" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("1.0 - tanh(x._value())*tanh(x._value())"); return stce_9837; } };
    using std::sqrt; template<class stce_9776>struct stce_10448{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return sqrt(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(1.0 / (2.0 * sqrt(stce_9841._value()))); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "sqrt" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("1.0 / (2.0 * sqrt(x._value()))"); return stce_9837; } };
    using std::log; template<class stce_9776>struct stce_10449{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return log(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(1.0 / stce_9841._value()); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "log" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("1.0 / x._value()"); return stce_9837; } };
    using ::erf; template<class stce_9776>struct stce_10450{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return erf(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(2.0 / sqrt(3.14159265358979323846264338327950288) * exp(-stce_9841._value() * stce_9841._value())); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "erf" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("2.0 / sqrt(3.14159265358979323846264338327950288) * exp(-x._value() * x._value())"); return stce_9837; } };
    using ::erfc; template<class stce_9776>struct stce_10451{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return erfc(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(-2.0 / sqrt(3.14159265358979323846264338327950288) * exp(-stce_9841._value() * stce_9841._value())); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "erfc" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("-2.0 / sqrt(3.14159265358979323846264338327950288) * exp(-x._value() * x._value())"); return stce_9837; } };
    using ::asinh; template<class stce_9776>struct stce_10452{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return asinh(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(1. / sqrt(1. + (stce_9841._value()*stce_9841._value()))); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "asinh" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("1. / sqrt(1. + (x._value()*x._value()))"); return stce_9837; } };
    using ::acosh; template<class stce_9776>struct stce_10453{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return acosh(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(1. / sqrt((stce_9841._value()*stce_9841._value()) - 1.)); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "acosh" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("1. / sqrt((x._value()*x._value()) - 1.)"); return stce_9837; } };
    using ::expm1; template<class stce_9776>struct stce_10454{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return expm1(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(exp(stce_9841._value())); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "expm1" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("exp(x._value())"); return stce_9837; } };
    using ::atanh; template<class stce_9776>struct stce_10455{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return atanh(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(1. / (1. - (stce_9841._value()*stce_9841._value()))); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "atanh" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("1. / (1. - (x._value()*x._value()))"); return stce_9837; } };
    using ::log1p; template<class stce_9776>struct stce_10456{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return log1p(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(1.0 / (stce_9841._value() + 1)); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "log1p" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("1.0 / (x._value() + 1)"); return stce_9837; } };
    using ::log10; template<class stce_9776>struct stce_10457{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &arg) { return log10(arg); } template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9841) { (void)_value; return static_cast<stce_9776>(1.0 / (stce_9841._value()*log(10))); } static std::string stce_10018(const std::string& stce_9841) { std::stringstream stce_9837; stce_9837 << "log10" << "("<< stce_9841 << ")"; return stce_9837.str(); } static std::string stce_10019() { std::string stce_9837 = std::string("1.0 / (x._value()*log(10))"); return stce_9837; } };
    template<class stce_9776>struct stce_10458 {
      template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &stce_9904) {
        return -stce_9904;
      }
      template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9904 ) {
        (void)_value;
        (void)stce_9904;
        return -1.0;
      }
      static std::string stce_10018(const std::string& stce_9841) {
        return "-1 *" + stce_9841;
      }
      static std::string stce_10019() {
        return "-1";
      }
    };
    template<class stce_9776>struct stce_10459 {
      template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &stce_9904) {
        return stce_9904;
      }
      template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9904 ) {
        (void)_value;
        (void)stce_9904;
        return 1.0;
      }
      static std::string stce_10018(const std::string& stce_9841) {
        return stce_9841;
      }
      static std::string stce_10019() {
        return "1";
      }
    };
    using ::fabs;
    template<class stce_9776>struct stce_10460 {
      template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &stce_9904) {
        return fabs(stce_9904);
      }
      template<class stce_9786>static inline const stce_9776 stce_10017(const stce_9776 &_value , const stce_9786 &stce_9904 ) {
        (void) _value;
        if (stce_9904._value() < 0) return -1.0;
        else return 1.0;
      }
      static std::string stce_10018(const std::string& stce_9841) {
        return "fabs(" + stce_9841 + ")";
      }
      static std::string stce_10019() {
        return "((x._value() >= 0) ? 1 : -1 )";
      }
    };
    template<class stce_9776>struct stce_10461{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { (void) stce_9904; (void) stce_9905; return atan2(stce_9904,stce_9905); } template<class stce_10426,class stce_10022>static inline const stce_9776 stce_10020(const stce_9776 _value , const stce_10426 &stce_9904, const stce_10022 &stce_9905) { (void) _value; (void) stce_9904; (void) stce_9905; return stce_9905._value() / (stce_9905._value() * stce_9905._value() + stce_9904._value() * stce_9904._value()); } template<class stce_10426,class stce_10022>static inline const stce_9776 stce_10021(const stce_9776 _value , const stce_10426 &stce_9904, const stce_10022 &stce_9905) { (void) _value; (void) stce_9904; (void) stce_9905; return -stce_9904._value() / (stce_9905._value() * stce_9905._value() + stce_9904._value() * stce_9904._value()); } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << "atan2" << "(" << stce_9950 << "," << stce_9951 <<")"; return stce_9837.str(); } static std::string stce_9906() { return "arg2._value() / (arg2._value() * arg2._value() + arg1._value() * arg1._value())"; } static std::string stce_9908() { return "-arg1._value() / (arg2._value() * arg2._value() + arg1._value() * arg1._value())"; } }; template<class stce_9776>struct stce_10462{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &stce_9904, const stce_9776 &stce_9905) { (void) stce_9904; (void) stce_9905; return atan2(stce_9904,stce_9905); } template<class stce_9786>static inline const stce_9776 stce_10020(const stce_9776 _value , const stce_9786 &stce_9904, const stce_9776 &stce_9905) { (void) _value; (void) stce_9904; (void) stce_9905; return stce_9905 / (stce_9905 * stce_9905 + stce_9904._value() * stce_9904._value()); } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << "atan2" << "(" << stce_9950 << "," << stce_9951 <<")"; return stce_9837.str(); } static std::string stce_9906() { return "arg2 / (arg2 * arg2 + arg1._value() * arg1._value())"; } }; template<class stce_9776>struct stce_10463{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9776 &stce_9904, const stce_9786 &stce_9905) { (void) stce_9904; (void) stce_9905; return atan2(stce_9904,stce_9905); } template<class stce_9786>static inline const stce_9776 stce_10021(const stce_9776 _value , const stce_9776 &stce_9904, const stce_9786 &stce_9905) { (void) _value; (void) stce_9904; (void) stce_9905; return -stce_9904 / (stce_9905._value() * stce_9905._value() + stce_9904 *stce_9904); } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << "atan2" << "(" << stce_9950 << "," << stce_9951 <<")"; return stce_9837.str(); } static std::string stce_9908() { return "-arg1 / (arg2._value() * arg2._value() + arg1 *arg1)"; } };
    template<class stce_9776>
    struct stce_10464 {
      template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) {
        return pow(stce_9904, stce_9905);
      }
      template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10020(const stce_9776 _value , const stce_10426 &stce_9904, const stce_10022 &stce_9905) {
        (void)_value;
        return stce_9905._value() * pow(stce_9904._value(), stce_9905._value() - 1.0);
      }
      template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10021(const stce_9776 _value , const stce_10426 &stce_9904, const stce_10022 &stce_9905) {
        (void) _value;
        if (stce_9904 <= 0)
          return 0;
        else
          return log(stce_9904._value()) * pow(stce_9904._value(), stce_9905._value());
      }
      static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) {
        return "pow(" + stce_9950 + "," + stce_9951 + ")";
      }
      static std::string stce_9906() {
        return "arg2._value() * pow(arg1._value(), arg2._value() - 1.0)";
      }
      static std::string stce_9908() {
        return "arg2._value() * pow(arg1._value(), arg2._value() - 1.0)";
      }
    };
    template<class stce_9776>
    struct stce_10465 {
      template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &stce_9904, const stce_9776 &stce_9905) {
        return pow(stce_9904, stce_9905);
      }
      template<class stce_9786>static inline const stce_9776 stce_10020(const stce_9776 _value , const stce_9786 &stce_9904, const stce_9776 &stce_9905) {
        (void) _value;
        return stce_9905 * pow(stce_9904._value(), stce_9905 - 1.0);
      }
      static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) {
        return "pow(" + stce_9950 + "," + stce_9951 + ")";
      }
      static std::string stce_9906() {
        return "arg2 * pow(arg1._value(), arg2 - 1.0)";
      }
    };
    template<class stce_9776>
    struct stce_10466 {
      template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9776 &stce_9904, const stce_9786 &stce_9905) {
        return pow(stce_9904, stce_9905);
      }
      template<class stce_9786>static inline const stce_9776 stce_10021(const stce_9776 _value , const stce_9776 &stce_9904, const stce_9786 &stce_9905) {
        (void) _value;
        return log(stce_9904) * pow(stce_9904, stce_9905._value());
      }
      static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) {
        return "pow(" + stce_9950 + "," + stce_9951 + ")";
      }
      static std::string stce_9906() {
        return "log(arg1) * pow(arg1, arg2._value())";
      }
    };
    template<class stce_9776>struct stce_10467{ template<class stce_10426, class stce_10022>static inline const stce_9776 stce_10016(const stce_10426 &stce_9904, const stce_10022 &stce_9905) { (void) stce_9904; (void) stce_9905; return hypot(stce_9904,stce_9905); } template<class stce_10426,class stce_10022>static inline const stce_9776 stce_10020(const stce_9776 _value , const stce_10426 &stce_9904, const stce_10022 &stce_9905) { (void) _value; (void) stce_9904; (void) stce_9905; return stce_9904._value() / _value; } template<class stce_10426,class stce_10022>static inline const stce_9776 stce_10021(const stce_9776 _value , const stce_10426 &stce_9904, const stce_10022 &stce_9905) { (void) _value; (void) stce_9904; (void) stce_9905; return stce_9905._value() / _value; } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << "hypot" << "(" << stce_9950 << "," << stce_9951 <<")"; return stce_9837.str(); } static std::string stce_9906() { return "arg1._value() / _value"; } static std::string stce_9908() { return "arg2._value() / _value"; } }; template<class stce_9776>struct stce_10468{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9786 &stce_9904, const stce_9776 &stce_9905) { (void) stce_9904; (void) stce_9905; return hypot(stce_9904,stce_9905); } template<class stce_9786>static inline const stce_9776 stce_10020(const stce_9776 _value , const stce_9786 &stce_9904, const stce_9776 &stce_9905) { (void) _value; (void) stce_9904; (void) stce_9905; return stce_9904._value() / _value; } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << "hypot" << "(" << stce_9950 << "," << stce_9951 <<")"; return stce_9837.str(); } static std::string stce_9906() { return "arg1._value() / _value"; } }; template<class stce_9776>struct stce_10469{ template<class stce_9786>static inline const stce_9776 stce_10016(const stce_9776 &stce_9904, const stce_9786 &stce_9905) { (void) stce_9904; (void) stce_9905; return hypot(stce_9904,stce_9905); } template<class stce_9786>static inline const stce_9776 stce_10021(const stce_9776 _value , const stce_9776 &stce_9904, const stce_9786 &stce_9905) { (void) _value; (void) stce_9904; (void) stce_9905; return stce_9905._value() / _value; } static std::string stce_10018(const std::string& stce_9950,const std::string& stce_9951) { std::stringstream stce_9837; stce_9837 << "hypot" << "(" << stce_9950 << "," << stce_9951 <<")"; return stce_9837.str(); } static std::string stce_9908() { return "arg2._value() / _value"; } };
  }
}
namespace dco {
namespace internal {
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10458<stce_9776> > operator -( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10458<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10458<stce_9776> > operator -( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10458<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10458<stce_9776> > operator -( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10458<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10458<stce_9776> > operator -( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10458<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10458<stce_9776> > operator -( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10458<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10459<stce_9776> > operator +( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10459<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10459<stce_9776> > operator +( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10459<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10459<stce_9776> > operator +( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10459<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10459<stce_9776> > operator +( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10459<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10459<stce_9776> > operator +( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10459<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10438<stce_9776> > sin( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10438<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10438<stce_9776> > sin( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10438<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10438<stce_9776> > sin( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10438<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10438<stce_9776> > sin( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10438<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10438<stce_9776> > sin( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10438<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10439<stce_9776> > cos( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10439<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10439<stce_9776> > cos( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10439<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10439<stce_9776> > cos( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10439<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10439<stce_9776> > cos( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10439<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10439<stce_9776> > cos( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10439<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10440<stce_9776> > tan( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10440<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10440<stce_9776> > tan( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10440<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10440<stce_9776> > tan( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10440<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10440<stce_9776> > tan( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10440<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10440<stce_9776> > tan( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10440<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10441<stce_9776> > cosh( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10441<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10441<stce_9776> > cosh( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10441<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10441<stce_9776> > cosh( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10441<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10441<stce_9776> > cosh( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10441<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10441<stce_9776> > cosh( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10441<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10442<stce_9776> > sinh( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10442<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10442<stce_9776> > sinh( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10442<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10442<stce_9776> > sinh( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10442<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10442<stce_9776> > sinh( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10442<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10442<stce_9776> > sinh( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10442<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10443<stce_9776> > asin( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10443<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10443<stce_9776> > asin( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10443<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10443<stce_9776> > asin( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10443<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10443<stce_9776> > asin( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10443<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10443<stce_9776> > asin( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10443<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10444<stce_9776> > acos( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10444<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10444<stce_9776> > acos( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10444<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10444<stce_9776> > acos( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10444<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10444<stce_9776> > acos( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10444<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10444<stce_9776> > acos( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10444<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10445<stce_9776> > exp( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10445<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10445<stce_9776> > exp( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10445<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10445<stce_9776> > exp( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10445<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10445<stce_9776> > exp( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10445<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10445<stce_9776> > exp( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10445<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10446<stce_9776> > atan( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10446<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10446<stce_9776> > atan( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10446<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10446<stce_9776> > atan( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10446<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10446<stce_9776> > atan( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10446<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10446<stce_9776> > atan( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10446<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10447<stce_9776> > tanh( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10447<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10447<stce_9776> > tanh( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10447<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10447<stce_9776> > tanh( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10447<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10447<stce_9776> > tanh( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10447<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10447<stce_9776> > tanh( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10447<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10448<stce_9776> > sqrt( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10448<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10448<stce_9776> > sqrt( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10448<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10448<stce_9776> > sqrt( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10448<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10448<stce_9776> > sqrt( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10448<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10448<stce_9776> > sqrt( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10448<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10449<stce_9776> > log( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10449<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10449<stce_9776> > log( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10449<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10449<stce_9776> > log( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10449<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10449<stce_9776> > log( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10449<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10449<stce_9776> > log( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10449<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10450<stce_9776> > erf( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10450<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10450<stce_9776> > erf( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10450<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10450<stce_9776> > erf( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10450<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10450<stce_9776> > erf( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10450<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10450<stce_9776> > erf( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10450<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10451<stce_9776> > erfc( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10451<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10451<stce_9776> > erfc( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10451<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10451<stce_9776> > erfc( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10451<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10451<stce_9776> > erfc( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10451<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10451<stce_9776> > erfc( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10451<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10454<stce_9776> > expm1( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10454<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10454<stce_9776> > expm1( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10454<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10454<stce_9776> > expm1( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10454<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10454<stce_9776> > expm1( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10454<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10454<stce_9776> > expm1( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10454<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10452<stce_9776> > asinh( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10452<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10452<stce_9776> > asinh( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10452<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10452<stce_9776> > asinh( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10452<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10452<stce_9776> > asinh( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10452<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10452<stce_9776> > asinh( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10452<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10453<stce_9776> > acosh( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10453<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10453<stce_9776> > acosh( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10453<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10453<stce_9776> > acosh( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10453<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10453<stce_9776> > acosh( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10453<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10453<stce_9776> > acosh( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10453<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10455<stce_9776> > atanh( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10455<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10455<stce_9776> > atanh( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10455<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10455<stce_9776> > atanh( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10455<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10455<stce_9776> > atanh( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10455<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10455<stce_9776> > atanh( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10455<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10456<stce_9776> > log1p( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10456<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10456<stce_9776> > log1p( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10456<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10456<stce_9776> > log1p( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10456<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10456<stce_9776> > log1p( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10456<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10456<stce_9776> > log1p( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10456<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10457<stce_9776> > log10( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10457<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10457<stce_9776> > log10( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10457<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10457<stce_9776> > log10( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10457<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10457<stce_9776> > log10( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10457<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10457<stce_9776> > log10( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10457<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10460<stce_9776> > fabs( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10460<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10460<stce_9776> > fabs( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10460<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10460<stce_9776> > fabs( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10460<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10460<stce_9776> > fabs( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10460<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10460<stce_9776> > fabs( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10460<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10460<stce_9776> > abs( const dco::internal::active_type<stce_9776, stce_9932> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::stce_10424::stce_10460<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10460<stce_9776> > abs( const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::stce_10424::stce_10460<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10460<stce_9776> > abs( const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::stce_10424::stce_10460<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10460<stce_9776> > abs( const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::stce_10424::stce_10460<stce_9776> >(stce_9950); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10460<stce_9776> > abs( const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950) { return dco::internal::stce_9780<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::stce_10424::stce_10460<stce_9776> >(stce_9950); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> > operator + (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10425<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10430<stce_9776> > operator + (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10430<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10431<stce_9776> > operator + (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10431<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10430<stce_9776> > operator + (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10430<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10431<stce_9776> > operator + (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10431<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10430<stce_9776> > operator + (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10430<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10431<stce_9776> > operator + (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10431<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10430<stce_9776> > operator + (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10430<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10431<stce_9776> > operator + (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10431<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10430<stce_9776> > operator + (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10430<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10431<stce_9776> > operator + (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10431<stce_9776> >(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> > operator - (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10427<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10432<stce_9776> > operator - (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10432<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10433<stce_9776> > operator - (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10433<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10432<stce_9776> > operator - (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10432<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10433<stce_9776> > operator - (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10433<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10432<stce_9776> > operator - (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10432<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10433<stce_9776> > operator - (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10433<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10432<stce_9776> > operator - (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10432<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10433<stce_9776> > operator - (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10433<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10432<stce_9776> > operator - (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10432<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10433<stce_9776> > operator - (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10433<stce_9776> >(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> > operator * (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10428<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10434<stce_9776> > operator * (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10434<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10435<stce_9776> > operator * (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10435<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10434<stce_9776> > operator * (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10434<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10435<stce_9776> > operator * (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10435<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10434<stce_9776> > operator * (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10434<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10435<stce_9776> > operator * (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10435<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10434<stce_9776> > operator * (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10434<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10435<stce_9776> > operator * (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10435<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10434<stce_9776> > operator * (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10434<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10435<stce_9776> > operator * (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10435<stce_9776> >(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> > operator / (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10429<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10436<stce_9776> > operator / (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10436<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10437<stce_9776> > operator / (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10437<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10436<stce_9776> > operator / (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10436<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10437<stce_9776> > operator / (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10437<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10436<stce_9776> > operator / (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10436<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10437<stce_9776> > operator / (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10437<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10436<stce_9776> > operator / (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10436<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10437<stce_9776> > operator / (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10437<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10436<stce_9776> > operator / (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10436<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10437<stce_9776> > operator / (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10437<stce_9776> >(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> > atan2 (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10461<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10462<stce_9776> > atan2 (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10462<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10463<stce_9776> > atan2 (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10463<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10462<stce_9776> > atan2 (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10462<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10463<stce_9776> > atan2 (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10463<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10462<stce_9776> > atan2 (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10462<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10463<stce_9776> > atan2 (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10463<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10462<stce_9776> > atan2 (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10462<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10463<stce_9776> > atan2 (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10463<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10462<stce_9776> > atan2 (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10462<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10463<stce_9776> > atan2 (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10463<stce_9776> >(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> > pow (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10464<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10465<stce_9776> > pow (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10465<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10466<stce_9776> > pow (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10466<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10465<stce_9776> > pow (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10465<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10466<stce_9776> > pow (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10466<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10465<stce_9776> > pow (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10465<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10466<stce_9776> > pow (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10466<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10465<stce_9776> > pow (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10465<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10466<stce_9776> > pow (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10466<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10465<stce_9776> > pow (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10465<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10466<stce_9776> > pow (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10466<stce_9776> >(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::active_type<stce_9776, stce_9932>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::active_type<stce_9776, stce_10474> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::active_type<stce_9776, stce_10474>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> > hypot (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_9951) { return dco::internal::stce_9783<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>,dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>, dco::stce_10424::stce_10467<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10468<stce_9776> > hypot (const dco::internal::active_type<stce_9776, stce_9932> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10468<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10469<stce_9776> > hypot (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::active_type<stce_9776, stce_9932> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::active_type<stce_9776, stce_9932>, dco::stce_10424::stce_10469<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10468<stce_9776> > hypot (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10468<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10469<stce_9776> > hypot (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::stce_10424::stce_10469<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10468<stce_9776> > hypot (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10468<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10469<stce_9776> > hypot (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::stce_10424::stce_10469<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10468<stce_9776> > hypot (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10468<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10469<stce_9776> > hypot (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::stce_10424::stce_10469<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10468<stce_9776> > hypot (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9950, const typename dco::helper::stce_9822<stce_9776>::type &stce_9951) { return dco::internal::stce_9784<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10468<stce_9776> >(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10469<stce_9776> > hypot (const typename dco::helper::stce_9822<stce_9776>::type &stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9951) { return dco::internal::stce_9785<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::stce_10424::stce_10469<stce_9776> >(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 > static inline bool operator == (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > static inline bool operator == (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator == (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > static inline bool operator == (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > static inline bool operator == (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > static inline bool operator == (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator == (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator == (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator == (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator == (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > static inline bool operator == (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator == (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator == (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator == (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator == (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > static inline bool operator == (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator == (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator == (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator == (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator == (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > static inline bool operator == (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator == (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator == (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator == (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator == (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9949(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > static inline bool operator == (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const typename dco::mode<dco::internal::active_type<stce_9776, stce_9932> >::passive_t& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9952(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > static inline bool operator == (const typename dco::mode<dco::internal::active_type<stce_9776, stce_9932> >::passive_t& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9953(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool operator == (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9952(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool operator == (const typename dco::mode<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9953(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool operator == (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9952(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool operator == (const typename dco::mode<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9953(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool operator == (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9952(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool operator == (const typename dco::mode<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9953(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool operator == (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9952(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool operator == (const typename dco::mode<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9953(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator == (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const stce_9786& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9952(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator == (const stce_9786& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9953(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator == (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9952(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator == (const stce_9786& stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9953(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator == (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9952(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator == (const stce_9786& stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9953(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator == (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9952(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator == (const stce_9786& stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9953(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator == (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9952(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator == (const stce_9786& stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9953(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 > static inline bool operator != (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > static inline bool operator != (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator != (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > static inline bool operator != (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > static inline bool operator != (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > static inline bool operator != (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator != (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator != (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator != (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator != (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > static inline bool operator != (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator != (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator != (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator != (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator != (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > static inline bool operator != (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator != (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator != (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator != (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator != (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > static inline bool operator != (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator != (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator != (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator != (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator != (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9954(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > static inline bool operator != (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const typename dco::mode<dco::internal::active_type<stce_9776, stce_9932> >::passive_t& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9955(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > static inline bool operator != (const typename dco::mode<dco::internal::active_type<stce_9776, stce_9932> >::passive_t& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9956(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool operator != (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9955(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool operator != (const typename dco::mode<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9956(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool operator != (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9955(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool operator != (const typename dco::mode<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9956(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool operator != (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9955(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool operator != (const typename dco::mode<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9956(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool operator != (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9955(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool operator != (const typename dco::mode<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9956(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator != (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const stce_9786& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9955(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator != (const stce_9786& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9956(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator != (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9955(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator != (const stce_9786& stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9956(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator != (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9955(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator != (const stce_9786& stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9956(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator != (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9955(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator != (const stce_9786& stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9956(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator != (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9955(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator != (const stce_9786& stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9956(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 > static inline bool operator < (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > static inline bool operator < (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator < (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > static inline bool operator < (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > static inline bool operator < (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > static inline bool operator < (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator < (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator < (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator < (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator < (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > static inline bool operator < (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator < (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator < (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator < (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator < (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > static inline bool operator < (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator < (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator < (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator < (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator < (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > static inline bool operator < (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator < (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator < (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator < (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator < (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9957(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > static inline bool operator < (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const typename dco::mode<dco::internal::active_type<stce_9776, stce_9932> >::passive_t& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9958(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > static inline bool operator < (const typename dco::mode<dco::internal::active_type<stce_9776, stce_9932> >::passive_t& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9959(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool operator < (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9958(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool operator < (const typename dco::mode<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9959(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool operator < (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9958(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool operator < (const typename dco::mode<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9959(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool operator < (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9958(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool operator < (const typename dco::mode<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9959(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool operator < (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9958(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool operator < (const typename dco::mode<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9959(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator < (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const stce_9786& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9958(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator < (const stce_9786& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9959(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator < (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9958(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator < (const stce_9786& stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9959(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator < (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9958(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator < (const stce_9786& stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9959(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator < (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9958(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator < (const stce_9786& stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9959(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator < (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9958(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator < (const stce_9786& stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9959(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 > static inline bool operator <= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > static inline bool operator <= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator <= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > static inline bool operator <= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > static inline bool operator <= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > static inline bool operator <= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > static inline bool operator <= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > static inline bool operator <= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > static inline bool operator <= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator <= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9960(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > static inline bool operator <= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const typename dco::mode<dco::internal::active_type<stce_9776, stce_9932> >::passive_t& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9961(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > static inline bool operator <= (const typename dco::mode<dco::internal::active_type<stce_9776, stce_9932> >::passive_t& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9962(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool operator <= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9961(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool operator <= (const typename dco::mode<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9962(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool operator <= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9961(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool operator <= (const typename dco::mode<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9962(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool operator <= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9961(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool operator <= (const typename dco::mode<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9962(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool operator <= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9961(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool operator <= (const typename dco::mode<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9962(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator <= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const stce_9786& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9961(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator <= (const stce_9786& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9962(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator <= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9961(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator <= (const stce_9786& stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9962(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator <= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9961(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator <= (const stce_9786& stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9962(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator <= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9961(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator <= (const stce_9786& stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9962(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator <= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9961(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator <= (const stce_9786& stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9962(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 > static inline bool operator > (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > static inline bool operator > (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator > (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > static inline bool operator > (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > static inline bool operator > (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > static inline bool operator > (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator > (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator > (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator > (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator > (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > static inline bool operator > (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator > (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator > (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator > (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator > (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > static inline bool operator > (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator > (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator > (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator > (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator > (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > static inline bool operator > (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator > (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator > (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator > (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator > (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9963(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > static inline bool operator > (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const typename dco::mode<dco::internal::active_type<stce_9776, stce_9932> >::passive_t& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9964(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > static inline bool operator > (const typename dco::mode<dco::internal::active_type<stce_9776, stce_9932> >::passive_t& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9965(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool operator > (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9964(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool operator > (const typename dco::mode<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9965(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool operator > (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9964(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool operator > (const typename dco::mode<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9965(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool operator > (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9964(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool operator > (const typename dco::mode<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9965(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool operator > (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9964(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool operator > (const typename dco::mode<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9965(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator > (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const stce_9786& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9964(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator > (const stce_9786& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9965(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator > (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9964(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator > (const stce_9786& stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9965(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator > (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9964(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator > (const stce_9786& stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9965(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator > (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9964(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator > (const stce_9786& stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9965(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator > (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9964(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator > (const stce_9786& stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9965(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 > static inline bool operator >= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > static inline bool operator >= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator >= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > static inline bool operator >= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > static inline bool operator >= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > static inline bool operator >= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > static inline bool operator >= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > static inline bool operator >= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > static inline bool operator >= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::active_type<stce_9776, stce_10474>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline bool operator >= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9966(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > static inline bool operator >= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const typename dco::mode<dco::internal::active_type<stce_9776, stce_9932> >::passive_t& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9967(stce_9950,stce_9951); } template<class stce_9776, class stce_9932 > static inline bool operator >= (const typename dco::mode<dco::internal::active_type<stce_9776, stce_9932> >::passive_t& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9968(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool operator >= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9967(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool operator >= (const typename dco::mode<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9968(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool operator >= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9967(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool operator >= (const typename dco::mode<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9968(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool operator >= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9967(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool operator >= (const typename dco::mode<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9968(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool operator >= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const typename dco::mode<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::passive_t& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9967(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool operator >= (const typename dco::mode<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::passive_t& stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9968(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator >= (const dco::internal::active_type<stce_9776, stce_9932>& stce_9950, const stce_9786& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9967(stce_9950,stce_9951); } template<class stce_9776, class stce_9932, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator >= (const stce_9786& stce_9950, const dco::internal::active_type<stce_9776, stce_9932>& stce_9951) { return dco::internal::active_type<stce_9776, stce_9932>::data_t::stce_9988::stce_9968(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator >= (const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9967(stce_9950,stce_9951); } template<class stce_9776, class stce_9942, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator >= (const stce_9786& stce_9950, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9951) { return dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t::stce_9988::stce_9968(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator >= (const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9967(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator >= (const stce_9786& stce_9950, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t::stce_9988::stce_9968(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator >= (const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9967(stce_9950,stce_9951); } template<class stce_9776, class stce_9934, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator >= (const stce_9786& stce_9950, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9951) { return dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t::stce_9988::stce_9968(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator >= (const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9950, const stce_9786& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9967(stce_9950,stce_9951); } template<class stce_9776, class stce_9935, class stce_9936, typename stce_9786 > static inline typename dco::dco_type_constructable_from<stce_9786, bool>::type operator >= (const stce_9786& stce_9950, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9951) { return dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t::stce_9988::stce_9968(stce_9950,stce_9951); }
template<class stce_9776, class stce_9932 >
static inline void stce_10475(dco::internal::active_type<stce_9776, stce_9932> &stce_9841) {
    double stce_10054 = 0;
    get(stce_9841, stce_10054);
    stce_9841 = stce_10054;
}
template<class stce_9776, class stce_9932 >
    static inline std::istream &operator >> (std::istream &in, dco::internal::active_type<stce_9776, stce_9932> &stce_9841) {
      stce_9776 &stce_10054 = stce_9841._value();
      in >> stce_10054;
      return in;
    }
    using std::ceil; using std::floor; using std::isfinite; using std::isnan; using std::isinf; template<class stce_9776, class stce_9932 > static inline bool isnan(const dco::internal::active_type<stce_9776, stce_9932>& stce_9841) { return isnan(stce_9841._value()); } template<class stce_9776, class stce_9932 > static inline bool isinf(const dco::internal::active_type<stce_9776, stce_9932>& stce_9841) { return isinf(stce_9841._value()); } template<class stce_9776, class stce_9932 > static inline double ceil(const dco::internal::active_type<stce_9776, stce_9932>& stce_9841) { return ceil(stce_9841._value()); } template<class stce_9776, class stce_9932 > static inline double floor(const dco::internal::active_type<stce_9776, stce_9932>& stce_9841) { return floor(stce_9841._value()); } template<class stce_9776, class stce_9932 > static inline bool isfinite(const dco::internal::active_type<stce_9776, stce_9932>& stce_9841) { return isfinite(stce_9841._value()); }
    using std::ceil; using std::floor; using std::isfinite; using std::isnan; using std::isinf; template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool isnan(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9841) { return isnan(stce_9841._value()); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool isinf(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9841) { return isinf(stce_9841._value()); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline double ceil(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9841) { return ceil(stce_9841._value()); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline double floor(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9841) { return floor(stce_9841._value()); } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline bool isfinite(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9841) { return isfinite(stce_9841._value()); }
    using std::ceil; using std::floor; using std::isfinite; using std::isnan; using std::isinf; template<class stce_9776, class stce_9934, class stce_9936 > static inline bool isnan(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9841) { return isnan(stce_9841._value()); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool isinf(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9841) { return isinf(stce_9841._value()); } template<class stce_9776, class stce_9934, class stce_9936 > static inline double ceil(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9841) { return ceil(stce_9841._value()); } template<class stce_9776, class stce_9934, class stce_9936 > static inline double floor(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9841) { return floor(stce_9841._value()); } template<class stce_9776, class stce_9934, class stce_9936 > static inline bool isfinite(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9841) { return isfinite(stce_9841._value()); }
    using std::ceil; using std::floor; using std::isfinite; using std::isnan; using std::isinf; template<class stce_9776, class stce_9935, class stce_9936 > static inline bool isnan(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9841) { return isnan(stce_9841._value()); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool isinf(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9841) { return isinf(stce_9841._value()); } template<class stce_9776, class stce_9935, class stce_9936 > static inline double ceil(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9841) { return ceil(stce_9841._value()); } template<class stce_9776, class stce_9935, class stce_9936 > static inline double floor(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9841) { return floor(stce_9841._value()); } template<class stce_9776, class stce_9935, class stce_9936 > static inline bool isfinite(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9841) { return isfinite(stce_9841._value()); }
    using std::ceil; using std::floor; using std::isfinite; using std::isnan; using std::isinf; template<class stce_9776, class stce_9942, class stce_9936 > static inline bool isnan(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9841) { return isnan(stce_9841._value()); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool isinf(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9841) { return isinf(stce_9841._value()); } template<class stce_9776, class stce_9942, class stce_9936 > static inline double ceil(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9841) { return ceil(stce_9841._value()); } template<class stce_9776, class stce_9942, class stce_9936 > static inline double floor(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9841) { return floor(stce_9841._value()); } template<class stce_9776, class stce_9942, class stce_9936 > static inline bool isfinite(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9841) { return isfinite(stce_9841._value()); }
    template<class stce_9776, class stce_9932 > static inline std::ostream& operator << (std::ostream& out, const dco::internal::active_type<stce_9776, stce_9932>& stce_9841) { out << stce_9841._value(); return out; }
    template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline std::ostream& operator << (std::ostream& out, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>& stce_9841) { out << stce_9841._value(); return out; }
    template<class stce_9776, class stce_9934, class stce_9936 > static inline std::ostream& operator << (std::ostream& out, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>& stce_9841) { out << stce_9841._value(); return out; }
    template<class stce_9776, class stce_9935, class stce_9936 > static inline std::ostream& operator << (std::ostream& out, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>& stce_9841) { out << stce_9841._value(); return out; }
    template<class stce_9776, class stce_9942, class stce_9936 > static inline std::ostream& operator << (std::ostream& out, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>& stce_9841) { out << stce_9841._value(); return out; }
template<class stce_9786> struct stce_10476 {
const static bool stce_10477 = true;
};
template<class stce_9776, class stce_9932 > struct stce_10476<dco::internal::active_type<stce_9776, stce_9932> > { const static bool stce_10477=false; };
template<class stce_9776, class stce_9942, class stce_9936 > struct stce_10476<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> > { const static bool stce_10477=false; };
template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > struct stce_10476<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> > { const static bool stce_10477=false; };
template<class stce_9776, class stce_9934, class stce_9936 > struct stce_10476<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> > { const static bool stce_10477=false; };
template<class stce_9776, class stce_9935, class stce_9936 > struct stce_10476<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> > { const static bool stce_10477=false; };
template <bool stce_10478, class stce_10479, class stce_10480>
struct stce_10481 {
typedef stce_10479 stce_10477;
};
template <class stce_10479, class stce_10480>
struct stce_10481<false, stce_10479, stce_10480> {
typedef stce_10480 stce_10477;
};
template <class stce_9786> struct
stce_10482 {
typedef stce_9786 stce_10477;
};
template<class stce_9776, class stce_9932 > struct stce_10482<dco::internal::active_type<stce_9776, stce_9932> > {typedef dco::internal::active_type< typename dco::internal::active_type<stce_9776, stce_9932>::VALUE_TYPE, typename dco::internal::active_type<stce_9776, stce_9932>::data_t> stce_10477; };
template<class stce_9776, class stce_9942, class stce_9936 > struct stce_10482<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> > {typedef dco::internal::active_type< typename dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::VALUE_TYPE, typename dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>::data_t> stce_10477; };
template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > struct stce_10482<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> > {typedef dco::internal::active_type< typename dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::VALUE_TYPE, typename dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>::data_t> stce_10477; };
template<class stce_9776, class stce_9934, class stce_9936 > struct stce_10482<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> > {typedef dco::internal::active_type< typename dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::VALUE_TYPE, typename dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>::data_t> stce_10477; };
template<class stce_9776, class stce_9935, class stce_9936 > struct stce_10482<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> > {typedef dco::internal::active_type< typename dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::VALUE_TYPE, typename dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>::data_t> stce_10477; };
template<class stce_9781, class stce_9782>
struct stce_10483 {
typedef typename stce_10481<stce_10476<stce_9781>::stce_10477, stce_9781, typename stce_10482<stce_9781>::stce_10477 >::stce_10477 stce_10484;
typedef typename stce_10481<stce_10476<stce_9782>::stce_10477, stce_9782, typename stce_10482<stce_9782>::stce_10477 >::stce_10477 stce_10485;
typedef typename stce_10481<stce_10476<stce_9781>::stce_10477, stce_10485, stce_10484>::stce_10477 stce_10486;
typedef typename stce_10481<stce_10476<stce_10486>::stce_10477, stce_9781, stce_10486>::stce_10477 type;
};
template<class stce_9776, class stce_9932 > static inline typename stce_10483<dco::internal::active_type<stce_9776, stce_9932>, dco::internal::active_type<stce_9776, stce_9932> >::type max(const dco::internal::active_type<stce_9776, stce_9932> &stce_10107, const dco::internal::active_type<stce_9776, stce_9932> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9932 > static inline typename stce_10483<dco::internal::active_type<stce_9776, stce_9932>, dco::internal::active_type<stce_9776, stce_9932> >::type min(const dco::internal::active_type<stce_9776, stce_9932> &stce_10107, const dco::internal::active_type<stce_9776, stce_9932> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > static inline typename stce_10483<dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> >::type max(const dco::internal::active_type<stce_9776, stce_9932> &stce_10107, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9932, class stce_10470, class stce_10471 > static inline typename stce_10483<dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> >::type min(const dco::internal::active_type<stce_9776, stce_9932> &stce_10107, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> >::type max(const dco::internal::active_type<stce_9776, stce_9932> &stce_10107, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9932, class stce_10472, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> >::type min(const dco::internal::active_type<stce_9776, stce_9932> &stce_10107, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > static inline typename stce_10483<dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> >::type max(const dco::internal::active_type<stce_9776, stce_9932> &stce_10107, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9932, class stce_10472, class stce_10471 > static inline typename stce_10483<dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> >::type min(const dco::internal::active_type<stce_9776, stce_9932> &stce_10107, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> >::type max(const dco::internal::active_type<stce_9776, stce_9932> &stce_10107, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9932, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::active_type<stce_9776, stce_9932>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> >::type min(const dco::internal::active_type<stce_9776, stce_9932> &stce_10107, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > static inline typename stce_10483<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::active_type<stce_9776, stce_10474> >::type max(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10107, const dco::internal::active_type<stce_9776, stce_10474> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9942, class stce_9936, class stce_10474 > static inline typename stce_10483<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::active_type<stce_9776, stce_10474> >::type min(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10107, const dco::internal::active_type<stce_9776, stce_10474> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> >::type max(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10107, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9942, class stce_9936, class stce_10470, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> >::type min(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10107, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> >::type max(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10107, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> >::type min(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10107, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> >::type max(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10107, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9942, class stce_9936, class stce_10472, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> >::type min(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10107, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> >::type max(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10107, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9942, class stce_9936, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> >::type min(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10107, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > static inline typename stce_10483<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474> >::type max(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10107, const dco::internal::active_type<stce_9776, stce_10474> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10474 > static inline typename stce_10483<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474> >::type min(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10107, const dco::internal::active_type<stce_9776, stce_10474> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> >::type max(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> >::type min(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> >::type max(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> >::type min(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> >::type max(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> >::type min(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> >::type max(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> >::type min(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > static inline typename stce_10483<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::active_type<stce_9776, stce_10474> >::type max(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10107, const dco::internal::active_type<stce_9776, stce_10474> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9936, class stce_10474 > static inline typename stce_10483<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::active_type<stce_9776, stce_10474> >::type min(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10107, const dco::internal::active_type<stce_9776, stce_10474> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> >::type max(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10107, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9936, class stce_10470, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> >::type min(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10107, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> >::type max(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10107, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> >::type min(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10107, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> >::type max(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10107, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9936, class stce_10472, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> >::type min(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10107, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> >::type max(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10107, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9936, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> >::type min(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10107, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > static inline typename stce_10483<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474> >::type max(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10107, const dco::internal::active_type<stce_9776, stce_10474> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9935, class stce_9936, class stce_10474 > static inline typename stce_10483<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::active_type<stce_9776, stce_10474> >::type min(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10107, const dco::internal::active_type<stce_9776, stce_10474> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> >::type max(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9935, class stce_9936, class stce_10470, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> >::type min(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9780<stce_9776, stce_10470, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> >::type max(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> >::type min(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9783<stce_9776, stce_10472, stce_10473, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> >::type max(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9935, class stce_9936, class stce_10472, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> >::type min(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9784<stce_9776, stce_10472, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> >::type max(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9935, class stce_9936, class stce_10473, class stce_10471 > static inline typename stce_10483<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> >::type min(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10107, const dco::internal::stce_9785<stce_9776, stce_10473, stce_10471> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9932 > static inline typename stce_10483<dco::internal::active_type<stce_9776, stce_9932>, stce_9776>::type max(const dco::internal::active_type<stce_9776, stce_9932> &stce_10107, const stce_9776 &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9932 > static inline typename stce_10483<dco::internal::active_type<stce_9776, stce_9932>, stce_9776>::type min(const dco::internal::active_type<stce_9776, stce_9932> &stce_10107, const stce_9776 &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9942, class stce_9936 > static inline typename stce_10483<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, stce_9776>::type max(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10107, const stce_9776 &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9942, class stce_9936 > static inline typename stce_10483<dco::internal::stce_9780<stce_9776, stce_9942, stce_9936>, stce_9776>::type min(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10107, const stce_9776 &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline typename stce_10483<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, stce_9776>::type max(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10107, const stce_9776 &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline typename stce_10483<dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936>, stce_9776>::type min(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10107, const stce_9776 &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9936 > static inline typename stce_10483<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, stce_9776>::type max(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10107, const stce_9776 &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9936 > static inline typename stce_10483<dco::internal::stce_9784<stce_9776, stce_9934, stce_9936>, stce_9776>::type min(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10107, const stce_9776 &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9935, class stce_9936 > static inline typename stce_10483<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, stce_9776>::type max(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10107, const stce_9776 &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9935, class stce_9936 > static inline typename stce_10483<dco::internal::stce_9785<stce_9776, stce_9935, stce_9936>, stce_9776>::type min(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10107, const stce_9776 &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9932 > static inline typename stce_10483<stce_9776, dco::internal::active_type<stce_9776, stce_9932> >::type max(const stce_9776 &stce_10107, const dco::internal::active_type<stce_9776, stce_9932> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9932 > static inline typename stce_10483<stce_9776, dco::internal::active_type<stce_9776, stce_9932> >::type min(const stce_9776 &stce_10107, const dco::internal::active_type<stce_9776, stce_9932> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9942, class stce_9936 > static inline typename stce_10483<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::type max(const stce_9776 &stce_10107, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9942, class stce_9936 > static inline typename stce_10483<stce_9776, dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> >::type min(const stce_9776 &stce_10107, const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline typename stce_10483<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::type max(const stce_9776 &stce_10107, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9935, class stce_9936 > static inline typename stce_10483<stce_9776, dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> >::type min(const stce_9776 &stce_10107, const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9934, class stce_9936 > static inline typename stce_10483<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::type max(const stce_9776 &stce_10107, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9934, class stce_9936 > static inline typename stce_10483<stce_9776, dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> >::type min(const stce_9776 &stce_10107, const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
template<class stce_9776, class stce_9935, class stce_9936 > static inline typename stce_10483<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::type max(const stce_9776 &stce_10107, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10038) { if (stce_10107 > stce_10038) return stce_10107; else return stce_10038; } template<class stce_9776, class stce_9935, class stce_9936 > static inline typename stce_10483<stce_9776, dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> >::type min(const stce_9776 &stce_10107, const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_10038) { if (stce_10107 < stce_10038) return stce_10107; else return stce_10038; }
}
}
namespace dco {
  namespace instrument {
    extern bool dco_instrument;
    struct stce_10487 {
      struct iterator_t {
        DCO_TAPE_INT _progvarcounter;
        DCO_TAPE_INT _stackcounter;
        iterator_t(DCO_TAPE_INT stce_10488, DCO_TAPE_INT stce_10489) : _progvarcounter(stce_10488), _stackcounter(stce_10489) {}
        DCO_TAPE_INT stce_10490() {
          return _progvarcounter + _stackcounter * 2;
        }
        DCO_TAPE_INT &stce_10491() {
          return _progvarcounter;
        }
      };
      struct stce_10492 {
        DCO_TAPE_INT id;
        std::string stce_10493;
        DCO_TAPE_INT stce_10494;
        iterator_t stce_10156;
        DCO_TAPE_INT stce_10291;
        DCO_TAPE_INT stce_10495;
        DCO_TAPE_INT stce_10126;
        DCO_TAPE_INT stce_10127;
        DCO_TAPE_INT stce_10496;
        DCO_TAPE_INT stce_10497;
        stce_10492 *stce_10392;
        stce_10492(stce_10487 *tape, const std::string &name, stce_10492 *stce_10498) : id(++tape->stce_9855),
          stce_10493(name),
          stce_10494(-1),
          stce_10156(tape->get_position()),
          stce_10291(0),
          stce_10495(0),
          stce_10126(0),
          stce_10127(0),
          stce_10496(0),
          stce_10497(0),
          stce_10392(stce_10498) {
        }
      };
    public:
      struct stce_10388 {
        virtual ~stce_10388() {
        }
        virtual void stce_10079(stce_9846 &stce_9847) {
          (void)stce_9847;
        }
      };
      struct interpretation_settings {
      };
      typedef void (*stce_10499)(stce_10487 &stce_10392, const interpretation_settings &stce_10300, stce_10388 *stce_10393);
      void stce_10500(stce_10499 stce_10501, stce_10388 *stce_10393) {
        (void) stce_10501;
        (void) stce_10393;
      }
      static stce_10487 *get_tape() {
        return NULL;
      }
      DCO_TAPE_INT tape_index() const {
        return 0;
      }
      DCO_TAPE_INT stce_9855;
      std::ofstream stce_10502;
      std::ofstream stce_10503;
      std::ofstream stce_10504;
      std::ofstream stce_10505;
      std::ofstream stce_10506;
      std::ofstream stce_10507;
      DCO_TAPE_INT _progvarcounter;
      DCO_TAPE_INT _stackcounter;
      stce_10492 *stce_10161;
      std::vector<stce_10492 *> stce_10508;
      std::stack<stce_10492 *> stce_10509;
      DCO_TAPE_INT stce_10510;
      stce_10487() : stce_9855(0),
        _progvarcounter(0),
        _stackcounter(0),
        stce_10161(NULL),
        stce_10510(0) { }
      ~stce_10487() {
        _finish_instrument();
      }
      bool is_active() {
        return dco_instrument;
      }
      void switch_to_active() {
        dco_instrument = true;
      }
      void switch_to_passive() {
        dco_instrument = false;
      }
      static void remove(stce_10487 *stce_9786) {
        delete stce_9786;
      }
      static stce_10487 *create(const DCO_TAPE_INT size = 0, const DCO_TAPE_INT stce_10511 = 0) {
        (void) size;
        (void) stce_10511;
        stce_10487 *stce_9837 = new stce_10487();
        stce_9837->_init_instrument();
        return stce_9837;
      }
      static stce_10487 *create(std::string stce_10512) {
        stce_10487 *stce_9837 = new stce_10487();
        stce_9837->_init_instrument(stce_10512);
        return stce_9837;
      }
      template<class stce_9776, class stce_9932>
      void register_variable(dco::internal::active_type<stce_9776, stce_9932> &stce_9841) {
        _progvarcounter++;
        stce_9841.tape_index = _progvarcounter;
        stce_9841.stce_10513 = stce_9855;
      }
      template<class stce_9776, class stce_9932>
      void register_variable(dco::internal::active_type<stce_9776, stce_9932> *stce_9841, DCO_TAPE_INT stce_10094) {
        for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; stce_9829++)
          this->register_variable(stce_9841[stce_9829]);
      }
      template<class stce_9776, class stce_9932>
      void register_variable(std::vector<dco::internal::active_type<stce_9776, stce_9932> > &stce_9841) {
        register_variable(&(stce_9841[0]), stce_9841.size());
      }
      template<class stce_9776, class stce_9932>
      void register_output_variable(dco::internal::active_type<stce_9776, stce_9932> &stce_9841) {
        stce_9841 = 1 * stce_9841;
      }
      template<class stce_9776, class stce_9932>
      void register_output_variable(dco::internal::active_type<stce_9776, stce_9932> *stce_9841, const DCO_TAPE_INT stce_10094) {
        if (stce_10094 == 0) return;
        for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829)
          stce_9841[stce_9829] = 1 * stce_9841[stce_9829];
      }
      template<class stce_9776, class stce_9932>
      void register_output_variable(std::vector<dco::internal::active_type<stce_9776, stce_9932> > &stce_9841) {
        register_output_variable(&(stce_9841[0]), stce_9841.size());
      }
      void _init_instrument(const std::string &stce_10512 = "") {
        stce_9855 = 0;
        std::string stce_10514 = stce_10512;
        if (stce_10512.size() != 0) {
          stce_10514 += "_";
        }
        stce_10502.open((stce_10514 + "nodes.dot").c_str());
        stce_10503.open((stce_10514 + "edges.dot").c_str());
        stce_10502 << "digraph {" << std::endl;
        stce_10504.open((stce_10514 + "nodes.csv").c_str());
        stce_10505.open((stce_10514 + "edges.csv").c_str());
        stce_10506.open((stce_10514 + "ext_nodes.csv").c_str());
        stce_10507.open((stce_10514 + "ext_edges.csv").c_str());
        dco_instrument = true;
        std::string name = "ROOT";
        std::string stce_10392 = "caller";
        stce_10515(name, stce_10392);
      }
      void _finish_instrument() {
        std::string name = "ROOT";
        std::string stce_10392 = "caller";
        stce_10516(name, stce_10392);
        for (size_t stce_9829 = 0; stce_9829 < stce_10508.size(); ++stce_9829) {
          stce_10492 *stce_9815 = stce_10508[stce_9829];
          stce_10502 << stce_9815->id << "[shape=box,label=\"" << stce_9815->stce_10493 << "("
                    << stce_9815->stce_10291 << "," << stce_9815->stce_10126 << "," << stce_9815->stce_10127 << ")\"]" << std::endl;
          stce_10504 << stce_9815->stce_10496 << " "
                    << stce_9815->stce_10291 << " "
                    << stce_9815->stce_10497 << " "
                    << stce_9815->stce_10126 << " "
                    << stce_9815->stce_10127 << " "
                    << stce_9815->stce_10493 << std::endl;
          delete stce_9815;
        }
        stce_10508.clear();
        stce_10503 << "}" << std::endl;
        stce_10502.close();
        stce_10503.close();
        stce_10504.close();
        stce_10505.close();
        stce_10506.close();
        stce_10507.close();
        dco_instrument = false;
      }
      iterator_t get_position() {
        return iterator_t(_progvarcounter, _stackcounter);
      }
      void interpret_adjoint() { }
      void interpret_adjoint_to(iterator_t &stce_10156) {
        (void) stce_10156;
      }
      void reset() {
        std::cerr << "dco instrument -- possible error due to reset() call" << std::endl;
      }
      void reset_to(iterator_t &stce_10156) {
        std::cerr << "dco instrument -- possible error due to reset() call" << std::endl;
        (void) stce_10156;
      }
      void stce_10515(const std::string &stce_10493 , const std::string &stce_10392 ) {
        (void) stce_10493;
        (void) stce_10392;
        stce_10492 *stce_9815 = new stce_10492(this, stce_10493, stce_10161);
        stce_10508.push_back(stce_9815);
        stce_10161 = stce_9815;
        if (! stce_10509.empty()) {
          stce_10492 *stce_10373 = stce_10509.top();
          DCO_TAPE_INT size = get_position().stce_10490() - stce_10373->stce_10156.stce_10490();
          stce_10503 << stce_10373->id << " -> " << stce_9815->id << "[label=\"" << size << "\"]" << std::endl;
          stce_10505 << stce_10373->id << " " << stce_9815->id << " " << size << std::endl;
          stce_10373->stce_10496++;
          stce_10373->stce_10291 += size;
          stce_10373->stce_10495 += size;
          stce_10373->stce_10156 = get_position();
        }
        stce_10509.push(stce_9815);
        stce_10510 = stce_9815->stce_10156.stce_10491();
        stce_9815->stce_10494 = stce_10510;
      }
      void stce_10516(const std::string &stce_10493 , const std::string &stce_10392 ) {
        (void) stce_10493;
        (void) stce_10392;
        stce_10492 *stce_9815 = stce_10509.top();
        DCO_TAPE_INT size = get_position().stce_10490() - stce_9815->stce_10156.stce_10490();
        stce_9815->stce_10291 += size;
        stce_9815->stce_10495 += size;
        stce_10509.pop();
        if (!stce_10509.empty()) {
          stce_10492 *stce_10517 = stce_10509.top();
          stce_10517->stce_10156 = get_position();
          stce_10517->stce_10495 += stce_9815->stce_10495;
          stce_10510 = stce_10517->stce_10494;
          stce_10161 = stce_10517;
        }
      }
    };
  template<class stce_9776, stce_10487 *&stce_10518>
    struct stce_10519 {
    typedef internal::stce_9946 stce_9988;
    typedef void mode_t;
  stce_9776 stce_10520;
  DCO_TAPE_INT tape_index;
  DCO_TAPE_INT _tape_index;
  mutable DCO_TAPE_INT stce_10513;
  mutable stce_10487::stce_10492 *stce_10392;
  stce_10519 &operator =(const stce_10519 &stce_9886) {
    tape_index = stce_9886.tape_index;
    stce_10513 = stce_9886.stce_10513;
    stce_10392 = stce_9886.stce_10392;
    _tape_index = stce_9886._tape_index;
    return *this;
  }
  stce_10519() :
      stce_10520(0.0), tape_index(0), _tape_index(0), stce_10513(0), stce_10392(0) {
    if (stce_10518)
      stce_10513 = stce_10518->stce_9855;
  }
  const stce_9776 &get_derivative() const {
    return stce_10520;
  }
  stce_9776 &get_derivative() {
    return stce_10520;
  }
  void clear() {
    tape_index = 0;
    stce_10513 = 0;
  }
  DCO_TAPE_INT stce_10521() const {
    return stce_9756 && tape_index == 0 ? 0 : 1;
  }
  template<class stce_9932 >
  static DCO_TAPE_INT stce_10522(const dco::internal::active_type<stce_9776, stce_9932> &stce_9841) {
    const stce_10519 &data = stce_9841;
    if (data.tape_index != 0) {
      if (data.stce_10392 != 0) {
        if (stce_10518->stce_10161 != data.stce_10392) {
          data.stce_10392->stce_10127++;
          data.stce_10392 = 0;
        }
      }
      if (data.tape_index <= stce_10518->stce_10510) {
        stce_10487::stce_10492 *stce_10523 = stce_10518->stce_10509.top();
        while (stce_10523) {
          if (data.stce_10513 < stce_10523->id) {
            stce_10523->stce_10126++;
            stce_10523 = stce_10523->stce_10392;
          } else {
            stce_10523 = NULL;
          }
        }
        data.stce_10513 = stce_10518->stce_10509.top()->id;
      }
    }
    return stce_9841.stce_10521();
  }
  template<class stce_9934, class stce_9935, class stce_9936 >
  static DCO_TAPE_INT stce_10522(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841) {
    return stce_10522(stce_9841.stce_9938) + stce_10522(stce_9841.stce_9940);
  }
  template<class stce_9942, class stce_9936 >
  static DCO_TAPE_INT stce_10522(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841) {
    return stce_10522(stce_9841.stce_9943);
  }
  template<class stce_9934, class stce_9936 >
  static DCO_TAPE_INT stce_10522(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841) {
    return stce_10522(stce_9841.stce_9938);
  }
  template<class stce_9935, class stce_9936 >
  static DCO_TAPE_INT stce_10522(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841) {
    return stce_10522(stce_9841.stce_9940);
  }
  template<class stce_9932 >
  static DCO_TAPE_INT stce_10524(const dco::internal::active_type<stce_9776, stce_9932> &stce_9841) {
    (void) stce_9841;
    return 0;
  }
  template<class stce_9934, class stce_9935, class stce_9936 >
  static DCO_TAPE_INT stce_10524(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841) {
    return stce_10524(stce_9841.stce_9938) + stce_10524(stce_9841.stce_9940) + 1;
  }
  template<class stce_9942, class stce_9936 >
  static DCO_TAPE_INT stce_10524(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841) {
    return stce_10524(stce_9841.stce_9943) + 1;
  }
  template<class stce_9934, class stce_9936 >
  static DCO_TAPE_INT stce_10524(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841) {
    return stce_10524(stce_9841.stce_9938) + 1;
  }
  template<class stce_9935, class stce_9936 >
  static DCO_TAPE_INT stce_10524(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841) {
    return stce_10524(stce_9841.stce_9940) + 1;
  }
  template<class stce_9884>
  __attribute__((always_inline)) inline void stce_9885(const stce_9884 &stce_9886) {
    DCO_TAPE_INT stce_10525 = stce_10522(stce_9886);
    if (dco_instrument && (stce_10518->stce_10161 != NULL)) {
      DCO_TAPE_INT stce_10526 = stce_10524(stce_9886);
      stce_10518->stce_10161->stce_10497 += (stce_10526);
    }
    if (NULL != stce_10518 && !stce_10518->is_active()) {
      clear();
    }
    if (stce_10525 <= 0) {
      clear();
      return;
    }
    stce_10518->_stackcounter += (stce_10525 + 1);
    stce_10518->_progvarcounter++;
    tape_index = stce_10518->_progvarcounter;
    stce_10392 = stce_10518->stce_10161;
  }
};
  }
}
namespace dco {
  namespace internal {
    template<class stce_9776>
    struct stce_10527 {
    private:
      struct stce_10528 {
        DCO_TAPE_INT stce_10525;
        DCO_TAPE_INT stce_10529;
      };
      stce_10528 *stce_10530;
      DCO_TAPE_INT stce_10531;
      int *stce_10532;
      DCO_TAPE_INT stce_10533;
      stce_10527(const stce_10527<stce_9776> &) {
        throw dco::exception::create<std::runtime_error>("dco_error: Not implemented yet! Please report, why needed!", "/home/vagrant/dco_cpp_make_release/dco_cpp_dev/src/dco_tape_blob_pattern.hpp", 30);
      }
      stce_10527(DCO_TAPE_INT stce_10525, DCO_TAPE_INT stce_10488) {
        stce_10530 = new stce_10528[stce_9752(stce_10488)];
        stce_10531 = 0;
        stce_10532 = new int[size_t(stce_10525)];
        stce_10533 = 0;
      }
    public:
      struct stce_10534 {
        friend struct stce_10527<stce_9776>;
      private:
        DCO_TAPE_INT stce_10535;
        DCO_TAPE_INT stce_10536;
        stce_10534(DCO_TAPE_INT stce_10537, DCO_TAPE_INT stce_10538) : stce_10535(stce_10537), stce_10536(stce_10538) {}
      public:
        stce_10534() : stce_10535(0), stce_10536(0) {};
        DCO_TAPE_INT stce_10539() {
          return stce_10535;
        }
        DCO_TAPE_INT stce_10540() {
          if (stce_10536 < 0) return -stce_10536;
          return stce_10536;
        }
        bool stce_10541() {
          return stce_10536 < 0;
        }
      };
      struct iterator_t {
      private:
        DCO_TAPE_INT _progvarcounter;
      public:
        DCO_TAPE_INT stce_10488() const {
          return _progvarcounter;
        }
        iterator_t(const DCO_TAPE_INT stce_10542) : _progvarcounter(stce_10542) {}
      };
      iterator_t get_position() {
        return iterator_t(stce_10531);
      }
      typedef std::vector<stce_10534> sparse_jacobi;
      template<class stce_9786>
      sparse_jacobi *build_pattern(stce_9786 *stce_10539, DCO_TAPE_INT stce_10094, const iterator_t &stce_10543) {
        sparse_jacobi *stce_9837 = new sparse_jacobi();
        for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
          stce_10544(stce_9829 + 1, stce_10539[stce_9829].tape_index, stce_10543, stce_9837);
        }
        return stce_9837;
      }
      void create_pattern_file(sparse_jacobi *stce_10545, std::string filename) {
        std::ofstream out(filename.c_str());
        for (size_t stce_9829 = 0; stce_9829 < stce_10545->size(); ++stce_9829) {
          stce_10534 &stce_10546 = (*stce_10545)[stce_9829];
          out << stce_10546.stce_10539() << " " << stce_10546.stce_10540() << " 1" << std::endl;
        }
        out.close();
      }
      void stce_10547(std::string filename) {
        std::ofstream out(filename.c_str());
        out << "digraph {" << std::endl;
        for (DCO_TAPE_INT stce_9829 = 1; stce_9829 <= stce_10531; ++stce_9829) {
          out << stce_9829 << std::endl;
        }
        for (DCO_TAPE_INT stce_9829 = 1; stce_9829 <= stce_10531; ++stce_9829) {
          int stce_10525 = stce_10530[stce_9829].stce_10525;
          int stce_10548 = stce_10530[stce_9829].stce_10529;
          for (int stce_10549 = 0; stce_10549 < stce_10525; ++stce_10549) {
            int stce_10045 = stce_10532[stce_10549 + stce_10548];
            out << stce_9829 << " -> " << stce_10045 << std::endl;
          }
        }
        out << "}" << std::endl;
        out.close();
      }
      void stce_10544(DCO_TAPE_INT stce_10537, DCO_TAPE_INT stce_9855, const iterator_t &stce_10543, sparse_jacobi *stce_10550) {
        if (stce_9855 == 0) return;
        if (stce_9855 <= stce_10543.stce_10488()) {
          stce_10550->push_back(stce_10534(stce_10537, stce_9855));
        } else {
          DCO_TAPE_INT stce_10525 = stce_10530[stce_9855].stce_10525;
          DCO_TAPE_INT stce_10548 = stce_10530[stce_9855].stce_10529;
          for (int stce_9829 = 0; stce_9829 < stce_10525; ++stce_9829) {
            stce_10544(stce_10537, stce_10532[stce_9829 + stce_10548], stce_10543, stce_10550);
          }
        }
      }
      static stce_10527 *create(DCO_TAPE_INT size, DCO_TAPE_INT stce_10511 = 0) {
        if (stce_10511 == 0) stce_10511 = size / 2;
        return new stce_10527(size, stce_10511);
      }
      template<class stce_9932 >
      void register_variable(dco::internal::active_type<stce_9776, stce_9932> &stce_10551) {
        stce_9932 &data = stce_10551.data();
        stce_10531++;
        data.tape_index = stce_10531;
      }
      template<stce_10527 *&global_tape>
      struct stce_10552 {
        typedef stce_9807 mode_t;
        DCO_TAPE_INT tape_index;
        stce_10552() : tape_index(0) {}
        void clear() {
          tape_index = 0;
        }
        template<class stce_9932 >
        static void interpret(const dco::internal::active_type<stce_9776, stce_9932> &stce_9841, bool stce_10541) {
          (void) stce_10541;
          global_tape->stce_10532[global_tape->stce_10533] = stce_9841.tape_index;
          global_tape->stce_10533++;
        }
        template<class stce_9934, class stce_9935, class stce_9936 >
        static void interpret(const dco::internal::stce_9783<stce_9776, stce_9934, stce_9935, stce_9936> &stce_9841, bool stce_10541) {
          interpret(stce_9841.stce_9938, stce_10541);
          interpret(stce_9841.stce_9940, stce_10541);
        }
        template<class stce_9942, class stce_9936 >
        static void interpret(const dco::internal::stce_9780<stce_9776, stce_9942, stce_9936> &stce_9841, bool stce_10541) {
          interpret(stce_9841.stce_9943, stce_10541);
        }
        template<class stce_9934, class stce_9936 >
        static void interpret(const dco::internal::stce_9784<stce_9776, stce_9934, stce_9936> &stce_9841, bool stce_10541) {
          interpret(stce_9841.stce_9938, stce_10541);
        }
        template<class stce_9935, class stce_9936 >
        static void interpret(const dco::internal::stce_9785<stce_9776, stce_9935, stce_9936> &stce_9841, bool stce_10541) {
          interpret(stce_9841.stce_9940, stce_10541);
        }
        template<class stce_9884, class stce_9798>
        static void stce_9885(const stce_9884 &stce_9886, stce_9798 &stce_10553) {
          const int stce_10529 = global_tape->stce_10533;
          interpret(stce_9886, false);
          stce_10552 &data = const_cast<stce_10552 &>(stce_10553);
          if (stce_10529 == global_tape->stce_10533) {
            data.clear();
          } else {
            global_tape->stce_10531++;
            data.tape_index = global_tape->stce_10531;
            global_tape->stce_10530[global_tape->stce_10531].stce_10525 = global_tape->stce_10533 - stce_10529;
            global_tape->stce_10530[global_tape->stce_10531].stce_10529 = stce_10529;
          }
        }
        template<class stce_9884>
        void stce_9885(const stce_9884 &stce_9886) {
          stce_9885(stce_9886, *this);
        }
      };
    };
  }
}
namespace dco {
  namespace instrument {
    typedef stce_10487 tape_t;
    extern tape_t *global_tape;
    typedef stce_10519<double, global_tape> data;
    typedef internal::active_type<double, data> type;
    typedef helper::stce_10100<type, tape_t> external_adjoint_object_t;
  }
  namespace p1t {
    typedef internal::stce_10527<double > tape_t;
    extern tape_t *global_tape;
    typedef tape_t::stce_10552<global_tape> data;
    typedef internal::active_type<double, data> type;
  }
  namespace p1f {
    typedef internal::stce_10051<32, double> data;
    typedef internal::active_type<double, data> type;
  }
  namespace p1f {
    inline void set(type &stce_9841, const bool stce_9753, const DCO_TAPE_INT stce_9830)
    {
      data &stce_10054 = stce_9841;
      stce_10054.stce_10053[stce_9830] = stce_9753;
    }
     inline void get(const type &stce_9841, bool &stce_9753, const DCO_TAPE_INT stce_9830)
    {
      stce_9753 = stce_9841.stce_10053[stce_9830];
    }
     inline void get(type &stce_9841, double &stce_9753)
    {
      stce_9753 = stce_9841._value();
    }
     inline std::string stce_10554(const type &stce_9841)
    {
      return stce_9841.stce_10053.to_string<char, std::char_traits<char>, std::allocator<char> >();
    }
  }
  namespace p1t {
     inline void set(type &stce_9841, const double stce_9753)
    {
      double &value = const_cast<double &>(stce_9841._value());
      value = stce_9753;
    }
     inline void get(type &stce_9841, double &stce_9753)
    {
      stce_9753 = stce_9841._value();
    }
  }
  namespace instrument {
  inline void set(type &stce_9841, const double stce_9753, const DCO_TAPE_INT what = 0)
    {
      stce_10029();;
      if (what == 0) {
        double &value = const_cast<double &>(stce_9841._value());
        value = stce_9753;
      } else if (what == -1) {
      } else {
        throw exception::create<std::invalid_argument>("invalid kind of what (musst be 0 or -1)");
      }
    }
  inline void get(const type &stce_9841, double &stce_9753, const DCO_TAPE_INT what = 0)
    {
      stce_10029();;
      if (what == 0)
        stce_9753 = stce_9841._value();
      else if (what == -1) {
        stce_9753 = 0;
      } else {
        throw exception::create<std::invalid_argument>("invalid kind of what (musst be 0 or -1)");
      }
    }
  }
}
       
       
namespace dco {
namespace internal {
template<class stce_9798, class stce_9799 = typename stce_9798::tape_t::stce_10082>
  struct jacobian_preaccumulator_t: stce_10070<stce_9799> {
  typedef stce_9799 value_t;
  typedef typename stce_9798::tape_t tape_t;
  typedef typename tape_t::iterator_t stce_10555;
  typedef typename tape_t::stce_9917 stce_10556;
  tape_t* const stce_9945;
  stce_10555 stce_10557;
  DCO_TAPE_INT stce_10558;
  typedef stce_9797<stce_9799, tape_t> stce_10350;
  stce_10350 stce_10282;
  bool stce_10559;
  void init() {}
  typedef stce_10142<memory_model::BLOB_TAPE, tape_t> stce_10275;
  typedef typename stce_10275::stce_9917 stce_10560;
  typedef typename stce_10275::iterator stce_10561;
  stce_10275* stce_10138;
  stce_10560* stce_9920;
  typedef typename stce_9798::data_t stce_10562;
  std::vector<stce_10562*> stce_10563;
  DCO_TAPE_INT stce_10298;
  jacobian_preaccumulator_t(tape_t * const tape) :
    stce_9945(tape),
    stce_10282(0, *tape),
    stce_10138(0),
    stce_9920(0),
    stce_10298(0)
  {
    if (stce_9945) {
      stce_10557 = stce_9945->get_position();
      stce_10558 = stce_10557.index();
      stce_10559 = true;
    } else {
      stce_10559 = false;
    }
  }
  ~jacobian_preaccumulator_t() {
    if (stce_10559)
      this->finish();
    if (stce_10138)
      delete stce_10138;
  }
  void start() {
    if (stce_9945) {
      stce_10557 = stce_9945->get_position();
      stce_10558 = stce_10557.index();
      stce_10559 = true;
    } else {
      stce_10559 = false;
    }
  }
  void register_output(stce_9798 &stce_9841)
  {
    if (stce_9945) {
      stce_10563.push_back(&static_cast<stce_10562&>(stce_9841));
    }
  }
  void register_output(std::vector<stce_9798> &stce_9841)
  {
    if (stce_9945) {
      stce_10563.reserve(stce_10563.size() + stce_9841.size());
      for (size_t stce_9829 = 0; stce_9829 < stce_9841.size(); ++stce_9829) {
        stce_10563.push_back(&static_cast<stce_10562&>(stce_9841[stce_9829]));
      }
    }
  }
  void finish() {
    if (!stce_10559) return;
    stce_10559 = false;
    if (!stce_9945) return;
    if (!stce_10563.size()) {
      stce_9945->reset_to(stce_10557);
      return;
    }
    const stce_10555 stce_10564 = stce_9945->get_position();
    const DCO_TAPE_INT stce_10565 = stce_10564.index() - stce_10557.index();
    stce_10282.resize(stce_10565 + 1);
    const DCO_TAPE_INT stce_10566 = stce_9945->size(stce_10564, stce_10557);
    const DCO_TAPE_INT stce_10567 = stce_10566 * (1 + static_cast<DCO_TAPE_INT>(stce_10563.size()));
    if (stce_10138) delete stce_10138;
      tape_options stce_10568;
    stce_10568.blob_size_in_byte() = stce_10567 * stce_10275::entry::size_of();
      stce_10138 = new stce_10275(stce_10568, *stce_9945);
    stce_10282.stce_10295();
    for(typename std::vector<stce_10562*>::iterator stce_10156 = stce_10563.begin(); stce_10156 != stce_10563.end(); ++stce_10156) {
      stce_10562& stce_9815 = **stce_10156;
      if (stce_9815._tape_index() == 0) continue;
      stce_10282.stce_10294(stce_10565-1,0);
      stce_10039(stce_9815._tape_index()) = 1.0;
      stce_9920 = new stce_10560(stce_10138->stce_9921());
      stce_9945->stce_10305(*this,
                                           stce_10564, stce_10557,
                                           interpretation_settings(false, true));
      delete stce_9920;
    }
    stce_9945->reset_to(stce_10557);
    stce_10561 stce_10226 = stce_10138->current();
    for (typename std::vector<stce_10562*>::reverse_iterator stce_10156 = stce_10563.rbegin();
         stce_10156 != stce_10563.rend(); ++stce_10156) {
      stce_10556 stce_10569(stce_9945->stce_9921());
      for (;; --stce_10226) {
        stce_10569.insert(stce_10226->arg(), stce_10226->pval());
        if(stce_10226->stce_10149())
          break;
      }
      --stce_10226;
      (*stce_10156)->_tape_index() = stce_10569.index();
    }
    stce_10563.clear();
  }
  value_t& stce_10039(const DCO_TAPE_INT stce_9830) {
    return stce_10282[stce_9830 - stce_10558];
  }
  template<class stce_10304>
    void stce_10072(const DCO_TAPE_INT &stce_9830, const stce_10304 &stce_9979,
                            const value_t &stce_10073) {
    stce_10072(stce_9830, stce_9979 * stce_10073);
  }
  void stce_10294(DCO_TAPE_INT stce_9860, DCO_TAPE_INT stce_9861) {
    stce_10282.stce_10294(stce_9860,stce_9861);
  }
  void stce_10286(DCO_TAPE_INT stce_9830) {
    stce_10282.stce_10286(stce_9830 - stce_10558);
  }
  value_t& operator[](const DCO_TAPE_INT stce_9830) {
    return stce_10282[stce_9830 - stce_10558];
  }
  template <typename iterator_t>
  void stce_10287(iterator_t& stce_9815) {
    stce_10072(stce_9815->arg(), stce_9815->pval()*(*this)[stce_9815.index()]);
  }
  virtual stce_9799 stce_10071(const DCO_TAPE_INT stce_9830) {
    return stce_10282[stce_9830 - stce_10558];
  }
  virtual void stce_10072(const DCO_TAPE_INT stce_9830, const stce_9799 &stce_10073) {
    if (stce_10073 == 0)
      return;
    if (stce_9830 <= stce_10558) {
      stce_9920->insert(stce_9830, stce_10073);
    } else {
      stce_10039(stce_9830) += stce_10073;
    }
  }
};
template<>
struct jacobian_preaccumulator_t<void, void> {
  jacobian_preaccumulator_t(void*) {}
  void start() {}
  template<typename stce_9786> void register_output(const stce_9786&) {}
  void finish() {}
};
}
}
namespace dco {
template <typename stce_9786>
  struct mode;
  template <class stce_9786, class stce_10570=stce_9786, class stce_10571=stce_10570>
  class ga1s {
    typedef stce_9765<stce_9786, stce_10570, stce_10571, stce_10571> stce_9792;
public:
    typedef stce_9786 value_t;
    typedef typename mode<value_t>::passive_t passive_t;
    static const bool is_dco_type = true;
    static const bool is_adjoint_type = true;
    static const bool is_tangent_type = false;
    static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
    static const memory_model::stce_9770 MEMORY_MODEL = DCO_TAPE_MEMORY_MODEL;
    typedef internal::tape<MEMORY_MODEL, stce_9792> tape_t;
    static tape_t *global_tape;
    typedef dco::internal::single_tape_data<ga1s> data_t;
    typedef typename stce_9792::derivative_t derivative_t;
    typedef dco::internal::active_type<derivative_t, data_t> type;
    typedef typename internal::local_gradient_t<type> local_gradient_t;
    typedef local_gradient_t DCO_DEPRECATED local_gradient_with_activity_t;
    typedef dco::helper::stce_10076<tape_t, typename tape_t::stce_10082 > stce_10388;
    typedef dco::helper::stce_10081<type, tape_t> userdata_object_t;
    typedef dco::helper::stce_10100<type, tape_t> external_adjoint_object_t;
    typedef external_adjoint_object_t efo_t;
    typedef internal::jacobian_preaccumulator_t<type> jacobian_preaccumulator_t;
    DCO_DEPRECATED static void get(const type &stce_9841, derivative_t &stce_9845, DCO_TAPE_INT stce_10572 = 0)
    {
      stce_10029();;
      if (stce_10572 == 0)
        stce_9845 = stce_9841._value();
      else if (stce_10572 == -1) {
        if (stce_9841._is_registered()) {
          stce_9845 = stce_9841._adjoint();
        } else
          stce_9845 = 0;
      } else {
        throw dco::exception::create<std::invalid_argument>("invalid kind of adjoint index in get.");
      }
    }
    DCO_DEPRECATED static void get(const type *stce_9841, derivative_t *stce_10573, const DCO_TAPE_INT stce_10574, const DCO_TAPE_INT what = 0)
    {
      stce_10029();;
      if (what == 0) {
        for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10574; ++stce_9829) {
          stce_10573[stce_9829] = stce_9841[stce_9829]._value();
        }
      } else if (what == -1) {
        for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10574; ++stce_9829) {
          derivative_t &value = stce_9841[stce_9829]._adjoint();
          stce_10573[stce_9829] = value;
        }
      } else {
        throw dco::exception::create<std::invalid_argument>("invalid kind of adjoint index in get.");
      }
    }
    DCO_DEPRECATED static void get(const std::vector<type> &stce_9841, std::vector<derivative_t> &stce_10573, const DCO_TAPE_INT what = 0)
    {
      stce_10573.resize(stce_9841.size());
      get(&(stce_9841[0]), &(stce_10573[0]), stce_9841.size(), what);
    }
    DCO_DEPRECATED static void set(type &stce_9841, const derivative_t stce_9845, DCO_TAPE_INT stce_10572 = 0)
    {
      stce_10029();;
      if (stce_10572 == 0) {
        derivative_t &value = const_cast<derivative_t &>(stce_9841._value());
        value = stce_9845;
      } else if (stce_10572 == -1) {
        if (stce_9841._is_registered()) {
          stce_9841._adjoint() = stce_9845;
        }
      } else {
        throw dco::exception::create<std::invalid_argument>("invalid kind of adjoint index in set.");
      }
    }
    DCO_DEPRECATED static void set(type *stce_9841, const derivative_t *stce_9753, const DCO_TAPE_INT stce_10574, const DCO_TAPE_INT what = 0)
    {
      stce_10029();;
      if (what == 0) {
        for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10574; ++stce_9829) {
          derivative_t &value = const_cast<derivative_t &>(stce_9841[stce_9829]._value());
          value = stce_9753[stce_9829];
        }
      } else if (what == -1) {
        for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10574; ++stce_9829) {
          if (stce_9841[stce_9829]._is_registered()) {
            derivative_t &value = stce_9841[stce_9829]._adjoint();
            value = stce_9753[stce_9829];
          }
        }
      } else {
        throw dco::exception::create<std::invalid_argument>("invalid kind of adjoint index in set.");
      }
    }
    DCO_DEPRECATED static void set(std::vector<type> &stce_9841, const std::vector<derivative_t> &stce_9753, const DCO_TAPE_INT what = 0)
    {
      stce_10029();;
      if (stce_9841.size() != stce_9753.size()) {
        throw dco::exception::create<std::invalid_argument>("vector sizes do not match in set()");
      }
      set(&(stce_9841[0]), &(stce_9753[0]), stce_9841.size(), what);
    }
  };
  template <class stce_9786, class stce_10570, class stce_10571>
  typename ga1s<stce_9786,stce_10570,stce_10571>::tape_t *ga1s<stce_9786,stce_10570,stce_10571>::global_tape = 0;
}
       
namespace dco {
template <class stce_9786, class stce_10570=stce_9786, class stce_10571=stce_10570, memory_model::stce_9770 MEMORY_MODEL=DCO_TAPE_MEMORY_MODEL>
  class gas {
  typedef stce_9765<stce_9786, stce_10570, stce_10571, stce_10571> stce_9792;
public:
  typedef internal::tape<MEMORY_MODEL, stce_9792> tape_t;
  static tape_t *global_tape;
  typedef dco::internal::single_tape_data<gas> data_t;
  typedef typename stce_9792::derivative_t derivative_t;
  typedef dco::internal::active_type<derivative_t, data_t> type;
  typedef typename internal::local_gradient_t<type> local_gradient_t;
  typedef local_gradient_t DCO_DEPRECATED local_gradient_with_activity_t;
  typedef dco::helper::stce_10076<tape_t, typename tape_t::stce_10082 > stce_10388;
  typedef dco::helper::stce_10081<type, tape_t> userdata_object_t;
  typedef dco::helper::stce_10100<type, tape_t> external_adjoint_object_t;
  typedef external_adjoint_object_t efo_t;
  typedef derivative_t value_t;
  static const bool is_dco_type = true;
  static const bool is_adjoint_type = true;
  static const bool is_tangent_type = false;
  static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
  typedef internal::jacobian_preaccumulator_t<type> jacobian_preaccumulator_t;
};
template <class stce_9786, class stce_10570, class stce_10571, memory_model::stce_9770 MEMORY_MODEL>
  typename gas<stce_9786,stce_10570,stce_10571,MEMORY_MODEL>::tape_t *gas<stce_9786,stce_10570,stce_10571,MEMORY_MODEL>::global_tape;
}
       
namespace dco {
 template <class stce_9786, class stce_10570=stce_9786, class stce_10571=stce_10570>
 class ga1s_mod {
   typedef stce_9765<stce_9786, stce_10570, stce_10571, stce_10571> stce_9792;
public:
   static const memory_model::stce_9770 MEMORY_MODEL = memory_model::stce_9775;
   static const memory_model::stce_9770 stce_9793 = DCO_TAPE_MEMORY_MODEL;
   typedef dco::internal::tape<MEMORY_MODEL, stce_9792, stce_9793> tape_t;
   static tape_t *global_tape;
   typedef dco::internal::single_tape_data<ga1s_mod> data_t;
   typedef typename stce_9792::derivative_t derivative_t;
   typedef dco::internal::active_type<derivative_t, data_t> type;
   typedef dco::helper::stce_10076<tape_t, typename tape_t::stce_10082 > stce_10388;
   typedef dco::helper::stce_10081<type, tape_t> userdata_object_t;
   typedef dco::helper::stce_10100<type, tape_t> external_adjoint_object_t;
   typedef external_adjoint_object_t efo_t;
   typedef stce_9786 value_t;
   typedef typename mode<value_t>::passive_t passive_t;
   static const bool is_dco_type = true;
   static const bool is_adjoint_type = true;
   static const bool is_tangent_type = false;
   static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
 };
 template <class stce_9786, class stce_10570, class stce_10571>
 typename ga1s_mod<stce_9786,stce_10570,stce_10571>::tape_t *ga1s_mod<stce_9786,stce_10570,stce_10571>::global_tape = 0;
}
       
namespace dco {
template<class stce_9786, class stce_10570 = stce_9786, class stce_10571 = stce_10570>
class gtas {
  static const memory_model::stce_9770 MEMORY_MODEL = memory_model::stce_9774;
  typedef stce_9765<stce_9786, stce_10570, stce_10571, stce_10571> stce_9792;
public:
  typedef internal::tape<MEMORY_MODEL, stce_9792> tape_t;
  static tape_t *global_tape;
  typedef dco::internal::stce_9996<gtas> data_t;
  typedef typename stce_9792::derivative_t derivative_t;
  typedef dco::internal::active_type<derivative_t, data_t> type;
  typedef typename internal::local_gradient_t<type> local_gradient_t;
  typedef local_gradient_t DCO_DEPRECATED local_gradient_with_activity_t;
  typedef dco::helper::stce_10076<tape_t, typename tape_t::stce_10082 > stce_10388;
  typedef dco::helper::stce_10081<type, tape_t> userdata_object_t;
  typedef dco::helper::stce_10100<type, tape_t> external_adjoint_object_t;
  typedef external_adjoint_object_t efo_t;
  typedef derivative_t value_t;
  typedef typename mode<value_t>::passive_t passive_t;
  static const bool is_dco_type = true;
  static const bool is_adjoint_type = true;
  static const bool is_tangent_type = true;
  static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
  typedef internal::jacobian_preaccumulator_t<type> jacobian_preaccumulator_t;
  typedef typename tape_t::logger logger;
  enum stce_10575 {
    check_branch = 1,
    check_fd = 2,
    check_identity = 4,
    stce_10576 = check_branch | check_fd | check_identity
  };
  static double& fd_eps() {
    assert(global_tape);
    return global_tape->stce_10244;
  }
  static double& ta_eps() {
    assert(global_tape);
    return global_tape->stce_10245;
  }
  static void init(const int stce_10386 = stce_10576) {
    assert(global_tape);
    global_tape->template stce_10260<data_t>();
    global_tape->stce_10241 = (stce_10386 & check_branch) != 0;
    global_tape->stce_10242 = (stce_10386 & check_fd) != 0;
    global_tape->stce_10005 = (stce_10386 & check_identity) != 0;
    global_tape->stce_10247 = 0;
    global_tape->stce_10248 = -1;
  }
  static DCO_TAPE_INT& throw_on_event() {
    assert(global_tape);
    return global_tape->stce_10248;
  }
  static bool& throw_on_warning() {
    assert(global_tape);
    return global_tape->stce_10243;
  }
};
template<typename stce_9786, typename stce_10022, typename stce_10577> typename gtas<stce_9786, stce_10022, stce_10577>::tape_t *gtas<stce_9786, stce_10022, stce_10577>::global_tape;
}
       
namespace dco {
template <class stce_9786, class stce_10570=stce_9786, class stce_10571=stce_10570>
  class ga1sm {
  typedef stce_9765<stce_9786, stce_10570, stce_10571, stce_10571> stce_9792;
public:
  static const memory_model::stce_9770 MEMORY_MODEL = DCO_TAPE_MEMORY_MODEL;
  typedef internal::tape<MEMORY_MODEL, stce_9792> tape_t;
  typedef typename stce_9792::derivative_t derivative_t;
  typedef dco::internal::stce_9991<tape_t, ga1sm> data_t;
  typedef dco::internal::active_type<derivative_t, data_t> type;
  typedef typename internal::local_gradient_t<type> local_gradient_t;
  typedef local_gradient_t DCO_DEPRECATED local_gradient_with_activity_t;
  typedef dco::helper::stce_10076<tape_t, typename stce_9792::ext_adjoint_real_t> stce_10388;
  typedef dco::helper::stce_10081<type, tape_t> userdata_object_t;
  typedef dco::helper::stce_10100<type, tape_t> external_adjoint_object_t;
  typedef external_adjoint_object_t efo_t;
  typedef derivative_t value_t;
  typedef typename mode<value_t>::passive_t passive_t;
  static const bool is_dco_type = true;
  static const bool is_adjoint_type = true;
  static const bool is_tangent_type = false;
  static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
  typedef internal::jacobian_preaccumulator_t<type> jacobian_preaccumulator_t;
  DCO_DEPRECATED static void get(const type &stce_9841, derivative_t &stce_9845, DCO_TAPE_INT stce_10572 = 0)
  {
    stce_10029();;
    if (stce_10572 == 0)
      stce_9845 = stce_9841._value();
    else if (stce_10572 == -1) {
      if (stce_9841._is_registered()) {
        stce_9845 = stce_9841._adjoint();
      } else
        stce_9845 = 0;
    } else {
      throw dco::exception::create<std::invalid_argument>("invalid kind of adjoint index in get.");
    }
  }
  DCO_DEPRECATED static void get(const type *stce_9841, derivative_t *stce_10573, const DCO_TAPE_INT stce_10094, const DCO_TAPE_INT what = 0)
  {
    stce_10029();;
    if (what == 0) {
      for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
        stce_10573[stce_9829] = stce_9841[stce_9829]._value();
      }
    } else if (what == -1) {
      for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
        stce_10573[stce_9829] = stce_9841[stce_9829]._adjoint();
      }
    } else {
      throw dco::exception::create<std::invalid_argument>("invalid kind of adjoint index in get.");
    }
  }
  DCO_DEPRECATED static void get(const std::vector<type> &stce_9841, std::vector<derivative_t> &stce_10573, const DCO_TAPE_INT what = 0)
  {
    stce_10573.resize(stce_9841.size());
    get(&(stce_9841[0]), &(stce_10573[0]), stce_9841.size(), what);
  }
  DCO_DEPRECATED static void set(type &stce_9841, const derivative_t stce_9845, DCO_TAPE_INT stce_10572 = 0)
  {
    stce_10029();;
    if (stce_10572 == 0) {
      derivative_t &value = const_cast<derivative_t &>(stce_9841._value());
      value = stce_9845;
    } else if (stce_10572 == -1) {
      if (stce_9841._is_registered()) {
        derivative_t &value = stce_9841._adjoint();
        value = stce_9845;
      }
    } else {
      throw dco::exception::create<std::invalid_argument>("invalid kind of adjoint index in set.");
    }
  }
  DCO_DEPRECATED static void set(type *stce_9841, const derivative_t *stce_9753, const DCO_TAPE_INT stce_10094, const DCO_TAPE_INT what = 0)
  {
    stce_10029();;
    if (what == 0) {
      for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
        derivative_t &value = const_cast<derivative_t &>(stce_9841[stce_9829]._value());
        value = stce_9753[stce_9829];
      }
    } else if (what == -1) {
      for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
        if (stce_9841[stce_9829]._is_registered()) {
          derivative_t &value = stce_9841[stce_9829]._adjoint();
          value = stce_9753[stce_9829];
        }
      }
    } else {
      throw dco::exception::create<std::invalid_argument>("invalid kind of adjoint index in set.");
    }
  }
  DCO_DEPRECATED static void set(std::vector<type> &stce_9841, const std::vector<derivative_t> &stce_9753, const DCO_TAPE_INT what = 0)
  {
    stce_10029();;
    if (stce_9841.size() != stce_9753.size()) {
      throw dco::exception::create<std::invalid_argument>("vector sizes do not match in set()");
    }
    set(&(stce_9841[0]), &(stce_9753[0]), stce_9841.size(), what);
  }
};
}
       
namespace dco {
template <class stce_9786, class stce_10570=stce_9786, class stce_10571=stce_10570>
  class ga1sm_mod {
  typedef stce_9765<stce_9786, stce_10570, stce_10571, stce_10571> stce_9792;
public:
   static const memory_model::stce_9770 MEMORY_MODEL = memory_model::stce_9775;
   static const memory_model::stce_9770 stce_9793 = DCO_TAPE_MEMORY_MODEL;
   typedef dco::internal::tape<MEMORY_MODEL, stce_9792, stce_9793> tape_t;
  typedef typename stce_9792::derivative_t derivative_t;
  typedef dco::internal::stce_9991<tape_t, ga1sm_mod> data_t;
  typedef dco::internal::active_type<derivative_t, data_t> type;
  typedef typename internal::local_gradient_t<type> local_gradient_t;
  typedef local_gradient_t DCO_DEPRECATED local_gradient_with_activity_t;
  typedef dco::helper::stce_10076<tape_t, typename stce_9792::ext_adjoint_real_t> stce_10388;
  typedef dco::helper::stce_10081<type, tape_t> userdata_object_t;
  typedef dco::helper::stce_10100<type, tape_t> external_adjoint_object_t;
  typedef external_adjoint_object_t efo_t;
  typedef derivative_t value_t;
  typedef typename mode<value_t>::passive_t passive_t;
  static const bool is_dco_type = true;
  static const bool is_adjoint_type = true;
  static const bool is_tangent_type = false;
  static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
};
}
       
namespace dco
{
template <class stce_9786, const int stce_9835=1, class stce_10570=stce_9786, class stce_10571=stce_10570>
  class ga1vm {
  static const memory_model::stce_9770 MEMORY_MODEL = DCO_TAPE_MEMORY_MODEL;
  typedef stce_9765<stce_9786, stce_10570, dco::helper::stce_9824<stce_10570, stce_9835>, stce_10571> stce_9792;
public:
  typedef internal::tape<MEMORY_MODEL, stce_9792> tape_t;
  static tape_t *global_tape;
  typedef dco::internal::stce_9991<tape_t, ga1vm> data_t;
  typedef typename stce_9792::derivative_t derivative_t;
  typedef typename stce_9792::adjoint_real_t adjoint_real_t;
  typedef dco::internal::active_type<derivative_t, data_t> type;
  typedef typename internal::local_gradient_t<type> local_gradient_t;
  typedef local_gradient_t DCO_DEPRECATED local_gradient_with_activity_t;
  typedef dco::helper::stce_10076<tape_t, typename tape_t::stce_10082 > stce_10388;
  typedef dco::helper::stce_10081<type, tape_t> userdata_object_t;
  typedef dco::helper::stce_10100<type, tape_t> external_adjoint_object_t;
  typedef external_adjoint_object_t efo_t;
  typedef derivative_t value_t;
  typedef typename mode<value_t>::passive_t passive_t;
  static const bool is_dco_type = true;
  static const bool is_adjoint_type = true;
  static const bool is_tangent_type = false;
  static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
  typedef internal::jacobian_preaccumulator_t<type> jacobian_preaccumulator_t;
};
}
       
namespace dco
{
template <class stce_9786, const int stce_9835=1, class stce_10570=stce_9786, class stce_10571=stce_10570>
  class ga1vm_mod {
  typedef stce_9765<stce_9786, stce_10570, dco::helper::stce_9824<stce_10570, stce_9835>, stce_10571> stce_9792;
public:
  static const memory_model::stce_9770 MEMORY_MODEL = memory_model::stce_9775;
  static const memory_model::stce_9770 stce_9793 = DCO_TAPE_MEMORY_MODEL;
  typedef dco::internal::tape<MEMORY_MODEL, stce_9792, stce_9793> tape_t;
  static tape_t *global_tape;
  typedef dco::internal::stce_9991<tape_t, ga1vm_mod> data_t;
  typedef typename stce_9792::derivative_t derivative_t;
  typedef typename stce_9792::adjoint_real_t adjoint_real_t;
  typedef dco::internal::active_type<derivative_t, data_t> type;
  typedef typename internal::local_gradient_t<type> local_gradient_t;
  typedef local_gradient_t DCO_DEPRECATED local_gradient_with_activity_t;
  typedef dco::helper::stce_10076<tape_t, typename tape_t::stce_10082 > stce_10388;
  typedef dco::helper::stce_10081<type, tape_t> userdata_object_t;
  typedef dco::helper::stce_10100<type, tape_t> external_adjoint_object_t;
  typedef external_adjoint_object_t efo_t;
  typedef derivative_t value_t;
  typedef typename mode<value_t>::passive_t passive_t;
  static const bool is_dco_type = true;
  static const bool is_adjoint_type = true;
  static const bool is_tangent_type = false;
  static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
};
}
namespace dco
{
template <class stce_9786, const int stce_9835=1, class stce_10570=stce_9786, class stce_10571=stce_10570>
  class ga1v {
  static const memory_model::stce_9770 MEMORY_MODEL = DCO_TAPE_MEMORY_MODEL;
  typedef stce_9765<stce_9786, stce_10570 ,dco::helper::stce_9824<stce_10570, stce_9835>, stce_10571> stce_9792;
public:
  typedef typename dco::internal::tape<MEMORY_MODEL, stce_9792> tape_t;
  static tape_t *global_tape;
  typedef dco::internal::single_tape_data<ga1v> data_t;
  typedef typename stce_9792::derivative_t derivative_t;
  typedef typename stce_9792::adjoint_real_t adjoint_real_t;
  typedef dco::internal::active_type<derivative_t, data_t> type;
  typedef typename internal::local_gradient_t<type> local_gradient_t;
  typedef local_gradient_t DCO_DEPRECATED local_gradient_with_activity_t;
  typedef dco::helper::stce_10076<tape_t, typename tape_t::stce_10082 > stce_10388;
  typedef dco::helper::stce_10081<type, tape_t> userdata_object_t;
  typedef dco::helper::stce_10100<type, tape_t> external_adjoint_object_t;
  typedef external_adjoint_object_t efo_t;
  typedef derivative_t value_t;
  typedef typename mode<value_t>::passive_t passive_t;
  static const bool is_dco_type = true;
  static const bool is_adjoint_type = true;
  static const bool is_tangent_type = false;
  static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
  typedef internal::jacobian_preaccumulator_t<type> jacobian_preaccumulator_t;
public:
  DCO_DEPRECATED static void get(const type &stce_9841, derivative_t &stce_9845, DCO_TAPE_INT stce_10572 = 0, DCO_TAPE_INT stce_9830 = 0) {
    if (stce_10572 == -1) {
      data_t &stce_10578 = const_cast<data_t &>(stce_9841);
      stce_9845 = stce_10578._adjoint()[stce_9830];
    } else if (stce_10572 == 0) {
      stce_9845 = stce_9841._value();
    } else
      throw dco::exception::create<std::runtime_error>("wrong component requested.");
  }
  DCO_DEPRECATED static void set(type &stce_9841, const derivative_t &stce_9845, DCO_TAPE_INT stce_10572 = 0, DCO_TAPE_INT stce_9830 = 0) {
    if (stce_10572 == -1) {
      data_t &stce_10578 = const_cast<data_t &>(stce_9841);
      stce_10578._adjoint()[stce_9830] = stce_9845;
    } else if (stce_10572 == 0) {
      derivative_t &stce_10579 = const_cast<derivative_t &>(stce_9841._value());
      stce_10579 = stce_9845;
    } else
      throw dco::exception::create<std::runtime_error>("wrong component requested.");
  }
};
template <class stce_9786, const int stce_9835, class stce_10570, class stce_10571>
  typename ga1v<stce_9786,stce_9835,stce_10570,stce_10571>::tape_t *ga1v<stce_9786,stce_9835,stce_10570,stce_10571>::global_tape;
}
       
namespace dco {
template <class stce_9786, const int stce_9835=1, class stce_10570=stce_9786, class stce_10571=stce_10570>
  class ga1v_mod {
  typedef stce_9765<stce_9786, stce_10570 ,dco::helper::stce_9824<stce_10570, stce_9835>, stce_10571> stce_9792;
public:
   static const memory_model::stce_9770 MEMORY_MODEL = memory_model::stce_9775;
   static const memory_model::stce_9770 stce_9793 = DCO_TAPE_MEMORY_MODEL;
   typedef dco::internal::tape<MEMORY_MODEL, stce_9792, stce_9793> tape_t;
  static tape_t *global_tape;
  typedef dco::internal::single_tape_data<ga1v_mod> data_t;
  typedef typename stce_9792::derivative_t derivative_t;
  typedef typename stce_9792::adjoint_real_t adjoint_real_t;
  typedef dco::internal::active_type<derivative_t, data_t> type;
  typedef typename internal::local_gradient_t<type> local_gradient_t;
  typedef local_gradient_t DCO_DEPRECATED local_gradient_with_activity_t;
  typedef dco::helper::stce_10076<tape_t, typename tape_t::stce_10082 > stce_10388;
  typedef dco::helper::stce_10081<type, tape_t> userdata_object_t;
  typedef dco::helper::stce_10100<type, tape_t> external_adjoint_object_t;
  typedef external_adjoint_object_t efo_t;
  typedef derivative_t value_t;
  typedef typename mode<value_t>::passive_t passive_t;
  static const bool is_dco_type = true;
  static const bool is_adjoint_type = true;
  static const bool is_tangent_type = false;
  static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
};
template <class stce_9786, const int stce_9835, class stce_10570, class stce_10571>
  typename ga1v_mod<stce_9786,stce_9835,stce_10570,stce_10571>::tape_t *ga1v_mod<stce_9786,stce_9835,stce_10570,stce_10571>::global_tape;
}
namespace dco
{
  template <typename stce_10580>
  struct ginstrument {
    typedef instrument::type type;
    typedef instrument::tape_t tape_t;
    typedef instrument::data data;
    typedef instrument::external_adjoint_object_t external_adjoint_object_t;
    static tape_t* &global_tape;
  };
  template <typename stce_10580> typename ginstrument<stce_10580>::tape_t *&ginstrument<stce_10580>::global_tape = instrument::global_tape;
}
       
namespace dco
{
template <class stce_9786>
  class gt1s {
public:
  typedef stce_9786 derivative_t;
  typedef dco::internal::ts_data<derivative_t, gt1s> data_t;
  typedef dco::internal::active_type<stce_9786, data_t> type;
  typedef stce_9786 value_t;
  typedef typename mode<value_t>::passive_t passive_t;
  typedef void tape_t;
  typedef void local_gradient_t;
  typedef void local_gradient_with_activity_t;
  typedef void external_adjoint_object_t;
  typedef external_adjoint_object_t efo_t;
  typedef internal::jacobian_preaccumulator_t<void, void> jacobian_preaccumulator_t;
  static const bool is_dco_type = true;
  static const bool is_adjoint_type = false;
  static const bool is_tangent_type = true;
  static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
  DCO_DEPRECATED static void get(const type &stce_9841, stce_9786 &stce_9845, int stce_10572 = 0)
  {
    stce_10029();;
    if (stce_10572 == 0)
      stce_9845 = stce_9841._value();
    else if (stce_10572 == 1) {
      stce_9841.stce_10039(stce_9845);
    } else {
      throw dco::exception::create<std::invalid_argument>("invalid kind of tangent-linear index in get");
    }
  }
  DCO_DEPRECATED static void set(type &stce_9841, const stce_9786 &stce_9845, DCO_TAPE_INT stce_10572 = 0)
  {
    stce_10029();;
    if (stce_10572 == 0) {
      stce_9786 &value = const_cast<stce_9786 &>(stce_9841._value());
      value = stce_9845;
    } else if (stce_10572 == 1) {
      stce_9841.stce_9985(stce_9845);
    } else {
      throw dco::exception::create<std::invalid_argument>("invalid kind of tangent-linear index in set");
    }
  }
  DCO_DEPRECATED static void get(const type *stce_9841, stce_9786 *stce_10573, const DCO_TAPE_INT stce_10094, const DCO_TAPE_INT what = 0)
  {
    stce_10029();;
    if (what == 0) {
      for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
        stce_10573[stce_9829] = stce_9841[stce_9829]._value();
      }
    } else if (what == 1) {
      for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
        stce_9841[stce_9829].stce_10039(stce_10573[stce_9829]);
      }
    } else {
      throw dco::exception::create<std::invalid_argument>("invalid kind of tangent-linear index in get");
    }
  }
  DCO_DEPRECATED static void get(const std::vector<type> &stce_9841, std::vector<stce_9786> &stce_10573, const DCO_TAPE_INT what = 0)
  {
    stce_10573.resize(stce_9841.size());
    get(&(stce_9841[0]), &(stce_10573[0]), stce_9841.size(), what);
  }
  DCO_DEPRECATED static void set(type *stce_9841, const stce_9786 *stce_9753, const DCO_TAPE_INT stce_10094, const DCO_TAPE_INT what = 0)
  {
    stce_10029();;
    if (what == 0) {
      for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
        stce_9786 &value = const_cast<stce_9786 &>(stce_9841[stce_9829]._value());
        value = stce_9753[stce_9829];
      }
    } else if (what == 1) {
      for (DCO_TAPE_INT stce_9829 = 0; stce_9829 < stce_10094; ++stce_9829) {
        stce_9841[stce_9829].stce_9985(stce_9753[stce_9829]);
      }
    } else {
      throw dco::exception::create<std::invalid_argument>("invalid kind of tangent-linear index in set");
    }
  }
  DCO_DEPRECATED static void set(std::vector<type> &stce_9841, const std::vector<stce_9786> &stce_9753, const int what = 0)
  {
    if (stce_9841.size() != stce_9753.size()) {
      throw dco::exception::create<std::invalid_argument>("vector sizes do not match in gt1s::set()");
    }
    stce_10029();;
    set(&(stce_9841[0]), &(stce_9753[0]), stce_9841.size(), what);
  }
};
}
       
namespace dco {
template <class stce_9786, typename DERIVATIVE_T, bool stce_10581, typename mode_t> class stce_10582 {
  public:
    typedef dco::internal::stce_10047<stce_9786, DERIVATIVE_T, mode_t> data_t;
  };
template <class stce_9786, typename DERIVATIVE_T, typename mode_t> class stce_10582<stce_9786, DERIVATIVE_T, false, mode_t> {
  public:
    typedef dco::internal::stce_10043<stce_9786, DERIVATIVE_T, mode_t> data_t;
  };
  template <class stce_9786, int stce_10583=1, bool stce_10581 = true>
  class gt1v {
  public:
    typedef dco::helper::stce_9824<stce_9786, stce_10583> derivative_t;
    typedef typename stce_10582<stce_9786, derivative_t, stce_10581, gt1v>::data_t data_t;
    typedef dco::internal::active_type<stce_9786, data_t> type;
    typedef stce_9786 value_t;
    typedef typename mode<value_t>::passive_t passive_t;
    typedef void tape_t;
    typedef void local_gradient_t;
    typedef void local_gradient_with_activity_t;
    typedef void external_adjoint_object_t;
    typedef external_adjoint_object_t efo_t;
    typedef internal::jacobian_preaccumulator_t<void, void> jacobian_preaccumulator_t;
    static const bool is_dco_type = true;
    static const bool is_adjoint_type = false;
    static const bool is_tangent_type = true;
    static const int stce_9808 = dco::mode<stce_9786>::stce_9808 + 1;
    DCO_DEPRECATED static void get(const type &stce_9841, stce_9786 &stce_9845, DCO_TAPE_INT stce_10572 = 0, DCO_TAPE_INT stce_9830 = 0)
    {
      stce_10029();;
      if (stce_10572 == 0) stce_9845 = stce_9841._value();
      else if (stce_10572 == 1) {
        stce_9841.stce_10039(stce_9845, stce_9830);
      } else {
        throw dco::exception::create<std::invalid_argument>("invalid kind of tangent-linear vector mode index in get");
      }
    }
    DCO_DEPRECATED static void set(type &stce_9841, const stce_9786 &stce_9845, DCO_TAPE_INT stce_10572 = 0, DCO_TAPE_INT stce_9830 = 0)
    {
      stce_10029();;
      if (stce_10572 == 0) {
        stce_9786 &value = const_cast<stce_9786 &>(stce_9841._value());
        value = stce_9845;
      } else if (stce_10572 == 1) {
        data_t &stce_10578 = const_cast<data_t &>(stce_9841);
        stce_10578.stce_9985(stce_9845, stce_9830);
      } else {
        throw dco::exception::create<std::invalid_argument>("invalid kind of tangent-linear vector mode index in set");
      }
    }
  };
}
       
namespace dco {
  template <class stce_10584>
  class stce_10585 {
  public:
    typedef stce_10584 type;
    typedef stce_10584 value_t;
    typedef stce_10584 passive_t;
    typedef void derivative_t;
    typedef void tape_t;
    typedef void local_gradient_t;
    typedef void local_gradient_with_activity_t;
    typedef void external_adjoint_object_t;
    typedef external_adjoint_object_t efo_t;
    typedef internal::jacobian_preaccumulator_t<void, void> jacobian_preaccumulator_t;
    static const bool is_dco_type = false;
    static const bool is_adjoint_type = false;
    static const bool is_tangent_type = false;
    static const int stce_9808 = 0;
    static tape_t *global_tape;
  };
template <class stce_10584>
  typename stce_10585<stce_10584>::tape_t *stce_10585<stce_10584>::global_tape = 0;
}
       
       
namespace dco {
template <class stce_9800>
  struct mode : public dco::stce_10585<stce_9800> { };
template <typename stce_9776, typename stce_9777>
  struct mode<internal::active_type<stce_9776, stce_9777> > :
    public stce_9777::mode_t {};
template <typename stce_9786, typename stce_10068, typename stce_10586>
  struct mode<internal::stce_9780<stce_9786, stce_10068, stce_10586> > :
    public internal::stce_9780<stce_9786, stce_10068, stce_10586>::data_t::mode_t {};
template<class stce_9786, class stce_10587, class stce_10588, class stce_10586>
  struct mode<internal::stce_9783<stce_9786, stce_10587, stce_10588, stce_10586> > :
    public internal::stce_9783<stce_9786, stce_10587, stce_10588, stce_10586>::data_t::mode_t {};
template<class stce_9786, class stce_10068, class stce_10586>
  struct mode<internal::stce_9784<stce_9786, stce_10068, stce_10586> > :
    public internal::stce_9784<stce_9786, stce_10068, stce_10586>::data_t::mode_t {};
template<class stce_9786, class stce_10068, class stce_10586>
  struct mode<internal::stce_9785<stce_9786, stce_10068, stce_10586> > :
    public internal::stce_9785<stce_9786, stce_10068, stce_10586>::data_t::mode_t {};
}
namespace dco {
template <typename stce_10120>
  struct gbcp {
  typedef typename dco::mode<stce_10120>::value_t value_t;
  typedef typename mode<value_t>::passive_t passive_t;
  typedef internal::jacobian_preaccumulator_t<void, void> jacobian_preaccumulator_t;
  typedef dco::internal::stce_10012<typename dco::mode<stce_10120>::data_t, gbcp> data_t;
  typedef dco::internal::active_type<value_t, data_t> type;
  static const bool is_dco_type = dco::mode<value_t>::is_dco_type;
  static const bool is_adjoint_type = dco::mode<value_t>::is_adjoint_type;
  static const bool is_tangent_type = dco::mode<value_t>::is_tangent_type;
  static const int stce_9808 = dco::mode<stce_10120>::stce_9808 - 1;
};
template <>
  struct gbcp<float> {
  typedef internal::jacobian_preaccumulator_t<void, void> jacobian_preaccumulator_t;
  typedef float value_t;
  typedef float type;
  static const bool is_dco_type = false;
  static const bool is_adjoint_type = false;
  static const bool is_tangent_type = false;
  static const int stce_9808 = 0;
};
template <>
  struct gbcp<double> {
  typedef internal::jacobian_preaccumulator_t<void, void> jacobian_preaccumulator_t;
  typedef double value_t;
  typedef double type;
  static const bool is_dco_type = false;
  static const bool is_adjoint_type = false;
  static const bool is_tangent_type = false;
  static const int stce_9808 = 0;
};
template <typename value_t, typename stce_10367, typename stce_10589>
  struct gbcp<internal::active_type<value_t, internal::stce_10012<stce_10367, stce_10589> > > : public stce_10589 {};
}
       
extern "C" {
  extern void* stce_10590();
  extern void stce_10591(void *);
  extern void* stce_10592(const struct stce_10593* stce_10386);
  extern void* stce_10594(const void* stce_10595);
  extern int stce_10596(const void*); extern int* stce_10597(struct stce_10593 *); extern void stce_10598(void*, int);
  extern int stce_10599(const void*); extern int* stce_10600(struct stce_10593 *); extern void stce_10601(void*, int);
  extern int stce_10602(const void*); extern int* stce_10603(struct stce_10593 *); extern void stce_10604(void*, int);
  extern int stce_10605(const void*); extern int* stce_10606(struct stce_10593 *); extern void stce_10607(void*, int);
  extern double stce_10608(const void*); extern double* stce_10609(struct stce_10593 *); extern void stce_10610(void*, double);
  extern int stce_10611(const void*); extern int* stce_10612(struct stce_10593 *); extern void stce_10613(void*, int);
  extern void* stce_10614(const void*); extern void** stce_10615(struct stce_10593 *); extern void stce_10616(void*, void*);
}
namespace dco {
  template <typename stce_9786>
  struct stce_10107 {
    typedef stce_9786 derivative_t;
    typedef ga1s<stce_9786> mode;
    typedef typename mode::type type;
    typedef type w_rtype;
    typedef typename mode::tape_t ir_t;
    typedef typename mode::stce_10388 stce_10388;
    typedef typename mode::userdata_object_t userdata_object_t;
    typedef typename mode::external_adjoint_object_t external_adjoint_object_t;
    typedef external_adjoint_object_t efo_t;
    static ir_t* &global_ir;
    static void* create_config() {
      return stce_10590();
    }
    static void remove_config(void* stce_10386) {
      stce_10591(stce_10386);
    }
    static void set_adjoint_mode(void* stce_10386, int mode) { stce_10598(stce_10386, mode); } static int get_adjoint_mode(const void* stce_10386) { return stce_10596(stce_10386); } static int adjoint_mode(const void* stce_10386) { return stce_10596(stce_10386); } static int& adjoint_mode(void* stce_10386) { int* mode = stce_10597(static_cast<struct stce_10593*>(stce_10386)); return *mode; };
    static void set_callback_mode(void* stce_10386, int mode) { stce_10601(stce_10386, mode); } static int get_callback_mode(const void* stce_10386) { return stce_10599(stce_10386); } static int callback_mode(const void* stce_10386) { return stce_10599(stce_10386); } static int& callback_mode(void* stce_10386) { int* mode = stce_10600(static_cast<struct stce_10593*>(stce_10386)); return *mode; };
    static void stce_10617(void* stce_10386, int mode) { stce_10604(stce_10386, mode); } static int stce_10618(const void* stce_10386) { return stce_10602(stce_10386); } static int checkpoint_mode(const void* stce_10386) { return stce_10602(stce_10386); } static int& checkpoint_mode(void* stce_10386) { int* mode = stce_10603(static_cast<struct stce_10593*>(stce_10386)); return *mode; };
    static void stce_10619(void* stce_10386, int mode) { stce_10607(stce_10386, mode); } static int stce_10620(const void* stce_10386) { return stce_10605(stce_10386); } static int number_of_checkpoints(const void* stce_10386) { return stce_10605(stce_10386); } static int& number_of_checkpoints(void* stce_10386) { int* mode = stce_10606(static_cast<struct stce_10593*>(stce_10386)); return *mode; };
    static void stce_10621(void* stce_10386, double mode) { stce_10610(stce_10386, mode); } static double stce_10622(const void* stce_10386) { return stce_10608(stce_10386); } static double primal_checkpoint_step(const void* stce_10386) { return stce_10608(stce_10386); } static double& primal_checkpoint_step(void* stce_10386) { double* mode = stce_10609(static_cast<struct stce_10593*>(stce_10386)); return *mode; };
    static void stce_10623(void* stce_10386, int mode) { stce_10613(stce_10386, mode); } static int stce_10624(const void* stce_10386) { return stce_10611(stce_10386); } static int primal_restore_mode(const void* stce_10386) { return stce_10611(stce_10386); } static int& primal_restore_mode(void* stce_10386) { int* mode = stce_10612(static_cast<struct stce_10593*>(stce_10386)); return *mode; };
    static void stce_10625(void* stce_10386, void* mode) { stce_10616(stce_10386, mode); } static void* stce_10626(const void* stce_10386) { return stce_10614(stce_10386); } static void* _routine_data(const void* stce_10386) { return stce_10614(stce_10386); } static void*& _routine_data(void* stce_10386) { void** mode = stce_10615(static_cast<struct stce_10593*>(stce_10386)); return *mode; };
    static void* stce_10627(const void* stce_10386) {
      return stce_10592(static_cast<const struct stce_10593*>(stce_10386));
    }
  };
  template <typename stce_9786> typename stce_10107<stce_9786>::ir_t* &stce_10107<stce_9786>::global_ir = stce_10107<stce_9786>::mode::global_tape;
  typedef stce_10107<double> a1w;
    typedef double w_rtype;
}
namespace dco {
  namespace instrument {
    class instrumentation_data {
      const std::string stce_10195;
      const std::string stce_10628;
      bool stce_10629;
    public:
      instrumentation_data(std::string stce_10630) : stce_10195(stce_10630), stce_10628("caller"), stce_10629(false) {
        if (dco::instrument::dco_instrument && dco::instrument::global_tape) {
          dco::instrument::global_tape->stce_10515(stce_10195, stce_10628);
          stce_10629 = true;
        }
      }
      ~instrumentation_data() {
        if (stce_10629) {
          dco::instrument::global_tape->stce_10516(stce_10195, stce_10628);
        }
      }
    };
    inline void stce_10631(std::string stce_10630) {
      std::string stce_10628 = "caller";
      dco::instrument::global_tape->stce_10515(stce_10630, stce_10628);
    }
    inline void stce_10632(std::string stce_10630) {
      std::string stce_10628 = "caller";
      dco::instrument::global_tape->stce_10516(stce_10630, stce_10628);
    }
  }
}
       


#include "dco_std_compatibility.hpp"
#include "dco_allocation.hpp"

#ifdef _MSC_VER
#pragma warning( pop )
#endif

