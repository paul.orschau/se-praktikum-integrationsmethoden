/*
dco/c++ v3.3.1
  - Algorithmic Differentiation by Operator Overloading in C++

COPYRIGHT
The Numerical Algorithms Group Limited and
Software and Tools for Computational Engineering @ RWTH Aachen University, 2017

This file is part of dco/c++.


*/
#pragma once

//** This sets the maximal log level that can be switched on at run
//** time. Caution: A high log level has negative performance impact,
//** even though dco::logger::level() is set to -1.
#ifndef DCO_LOG_MAX_LEVEL
# define DCO_LOG_MAX_LEVEL -1
#endif

//** This defines whether to use chunk tape or not. When using chunk
//** tape, the file tape can be used as well.
#ifdef DCO_CHUNK_TAPE
# define DCO_TAPE_MEMORY_MODEL memory_model::CHUNK_TAPE
#else
# define DCO_TAPE_MEMORY_MODEL memory_model::BLOB_TAPE_SPLINT
#endif

//** define basic integer type for tape index
#ifdef DCO_TAPE_USE_LONG_INT
#define DCO_TAPE_INT_DEFINE long int
#else
#define DCO_TAPE_INT_DEFINE int
#endif

//** undocumented feature: 
//** this macro enables a constructor taking the type passed as argument
#define DCO_ENABLE_TYPE_CONSTRUCTOR(T)          \
  namespace dco {                               \
  template <typename T2>                        \
  struct dco_type_constructable_from<T,T2> {    \
    typedef T2 type;                            \
  };                                            \
  }

//** important if using compiler with C++11 support;
//**   when using auto and a template expression library, local references dangle
//**   if using auto, make sure to define DCO_AUTO_SUPPORT
//***   performance implications: might slow down recording when working with long right-hand-sides
#if (__cplusplus > 199711L)
#  ifndef DCO_AUTO_SUPPORT
#    ifndef DCO_DISABLE_AUTO_WARNING
#      warning Your compiler has C++11 capabilities. If you are using 'auto', make sure defining 'DCO_AUTO_SUPPORT'. You can disable this warning by defining 'DCO_DISABLE_AUTO_WARNING'.
#    endif
#  endif
#endif

#ifdef DCO_AUTO_SUPPORT
#define DCO_TEMPORARY_REFORCOPY
#else
#define DCO_TEMPORARY_REFORCOPY &
#endif


//** use DEPRECATED to generate compiler-warnings, if a marked
//** function is instantiated (only implemented for gnu compiler and
//** VisualC++)
#if __GNUC__
#  define DCO_DEPRECATED __attribute__((deprecated))
#elif defined(_WIN32)
#  define DCO_DEPRECATED __declspec(deprecated)
#else
#  define DCO_DEPRECATED
#endif

//** can be used to force inlining by the compiler
#if defined(_MSC_VER)
#  define DCO_STRONG_INLINE __forceinline
#  define DCO_ALWAYS_INLINE DCO_STRONG_INLINE
#else
#  define DCO_STRONG_INLINE inline
#  define DCO_ALWAYS_INLINE __attribute__((always_inline)) inline
#endif

//** in this case, modulo adjoint propagation uses the bit-wise binary
//** & instead of modulo operation (%). For doing this, the adjoint
//** vector size needs to be a power of 2 => rounded up.
namespace dco {
#ifdef DCO_BITWISE_MODULO
static const bool HAS_BITWISE_MODULO = true;
#else
static const bool HAS_BITWISE_MODULO = false;
#endif

}


