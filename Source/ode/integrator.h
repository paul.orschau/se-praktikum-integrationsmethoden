// A Case Study in Simulation Software Engineering with C++
// Uwe Naumann, STCE, RWTH Aachen, 2018

#ifndef _INTEGRATOR_INC_
#define _INTEGRATOR_INC_

#include "ode.h"

// INTEGRATOR Class
// An abstract superclass, which provides the necessary interface for the ODE_SOLVER class
template<typename TS=double, int NP=Dynamic, int NS=Dynamic, typename TP=TS>
class INTEGRATOR {
  typedef Matrix<TS,NS,1>  VTS;
public:
  // Routines
  virtual VTS integrate(TP lb, TP ub, ODE<TS,NP,NS,TP>& ode)=0;
};

#endif