#ifndef _IMP_EULER_INC_
#define _IMP_EULER_INC_

#include "../integrator.h"

// IMPLICIT EULER SYSTEM Class
// This class represents the nonlinear system of equations that has to be solved for the implicit euler scheme.
template<typename TS=double, int NP=Dynamic, int NS=Dynamic, typename TP=TS>
class IMPLICIT_EULER_SYSTEM : public NONLINEAR_SYSTEM<TS,NP,NS,TP> {
  typedef Matrix<TS,NS,1> VTS;
  typedef Matrix<TS,NS,NS> MTS;
  using NONLINEAR_SYSTEM<TS,NP,NS,TP>::f;
  using NONLINEAR_SYSTEM<TS,NP,NS,TP>::dfdx;
  ODE<TS,NP,NS,TP>& _ode;   // ODE to solve
  VTS& _x_prev;             // Last time step
  TP _dt;                   // Step size
public:
  using NONLINEAR_SYSTEM<TS,NP,NS,TP>::x;

  // Constructor
  IMPLICIT_EULER_SYSTEM(ODE<TS,NP,NS,TP>& ode, VTS& x_prev, TP dt) : NONLINEAR_SYSTEM<TS,NP,NS,TP>(ode.np(),ode.ns()), _ode(ode), _x_prev(x_prev), _dt(dt) {
    x()=_x_prev;
  }

  // Functions
  VTS f() { 
    _ode.x()=x();
    return _ode.x()-_x_prev-_dt*_ode.g();
  }
  MTS dfdx() { 
    _ode.x()=x();
    return Matrix<TS,NS,NS>::Identity()-_dt*_ode.dgdx();
  }
};

// IMPLICIT EULER Class
// This integration scheme uses the gradient at the next time step for integration.
// It is an implicit scheme.
template<typename TS=double, int NP=Dynamic, int NS=Dynamic, typename TP=TS>
class IMPLICIT_EULER : public INTEGRATOR<TS,NP,NS,TP> {
  typedef Matrix<TS,NS,1>  VTS;
  typedef Matrix<TS,NS,NS> MTS;
  typedef Matrix<TP,NP,1>  VTP;
  NONLINEAR_SOLVER<TS,NP,NS,TP>& _nlsol;  // Solver
public:
  // Constructor
  IMPLICIT_EULER(NONLINEAR_SOLVER<TS,NP,NS,TP>& nlsol) : _nlsol(nlsol) {};

  // Routines
  VTS integrate(TP lb, TP ub, ODE<TS,NP,NS,TP>& ode) {
    TP h = ub-lb;
    VTS x_prev = ode.x(); // Remember current time step
    IMPLICIT_EULER_SYSTEM<TS,NP,NS,TP> odesys(ode,x_prev,h);
    _nlsol.solve(odesys);
    ode.x()=x_prev; // Reset time step
    return odesys.x()-x_prev; // Return difference
  }
};

#endif