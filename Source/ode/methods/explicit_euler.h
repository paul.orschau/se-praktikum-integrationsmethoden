#ifndef _EXP_EULER_INC_
#define _EXP_EULER_INC_

#include "../integrator.h"

// EXPLICIT EULER Class
// This integration scheme uses the gradient at the current time step for integration.
// It is an explicit scheme.
template<typename TS=double, int NP=Dynamic, int NS=Dynamic, typename TP=TS>
class EXPLICIT_EULER : public INTEGRATOR<TS,NP,NS,TP> {
  typedef Matrix<TS,NS,1>  VTS;
  typedef Matrix<TS,NS,NS> MTS;
  typedef Matrix<TP,NP,1>  VTP;
public:
  // Routines
  VTS integrate(TP lb, TP ub, ODE<TS,NP,NS,TP>& ode) {
    TP h = ub-lb;
    return h*ode.g();
  }
};

#endif