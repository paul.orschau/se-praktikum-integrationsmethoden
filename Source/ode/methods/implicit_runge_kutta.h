#ifndef _IMP_RUNGE_KUTTA_INC_
#define _IMP_RUNGE_KUTTA_INC_

#include "../integrator.h"

// IMPLICIT RUNGE KUTTA SYSTEM Class
// This class represents the nonlinear system of equations that has to be solved for the implicit runge kutta scheme.
template<const int S, const int SN, typename TS=double, int NP=Dynamic, int NS=Dynamic, typename TP=TS>
class IMPLICIT_RUNGE_KUTTA_SYSTEM : public NONLINEAR_SYSTEM<TP,NP,SN,TP> {
  typedef Matrix<TS,NS,1>   VTS;
  typedef Matrix<TS,NS,NS>  MTS;
  typedef Matrix<TP,SN,1>   VTK;
  typedef Matrix<TP,SN,SN>  MTK;
  typedef Matrix<TP,S,S>    KK;
  using NONLINEAR_SYSTEM<TP,NP,SN,TP>::f;
  using NONLINEAR_SYSTEM<TP,NP,SN,TP>::dfdx;
  ODE<TS,NP,NS,TP>& _ode;   // ODE to solve
  VTS& _x_prev;             // Last time step
  TP _dt;                   // Step size
  KK& _A;                   // Tableau
public:
  using NONLINEAR_SYSTEM<TP,NP,SN,TP>::x;

  // Constructor
  IMPLICIT_RUNGE_KUTTA_SYSTEM(ODE<TS,NP,NS,TP>& ode, VTS& x_prev, TP dt, VTK& k_init, KK& A) : NONLINEAR_SYSTEM<TP,NP,SN,TP>(ode.np(),k_init.size()), _ode(ode), _x_prev(x_prev), _dt(dt), _A(A) {
    x()=k_init;
  }

  // Functions
  // Explanation can be found here:
  // https://www.ams.org/journals/mcom/1991-57-196/S0025-5718-1991-1094945-2/S0025-5718-1991-1094945-2.pdf
  VTK f() { 
    VTK result = VTK::Zero(); // D(Y)
    // Get all gradients
    VTK grad = VTK::Zero();   // F(Y)
    for (int i=0;i<S;i++) {
      _ode.x() = x().segment(NS*i,NS);   // yi
      grad.segment(NS*i,NS) = _ode.g();  // f(yi)
    }
    // Calculate D(Y)
    for(int i=0;i<S;i++) {
      VTS curr = VTS::Zero(); // NS rows of (AxI)F(Y)
      for (int j=0;j<NS;j++) {
        for (int k=0;k<S;k++) {
          curr(j) += _A(i,k)*grad(j+NS*k);   // the i*NS+j th row of (AxI)F(Y)
        }
      }
      result.segment(NS*i,NS) = _x_prev - x().segment(NS*i,NS) + _dt*curr;    // D(Y) = Xr - Y + h(AxI)F(Y)
    }
    return result;
  }
  MTK dfdx() { 
    MTK result = MTK::Zero(); // D'(Y)

    // calculate (AxI)F'(Y)
    for (int i=0;i<S;i++) { // ith column block
      _ode.x() = x().segment(NS*i,NS);     // yi
      for (int j=0;j<S;j++) { // jth row block
        result.block(NS*j,NS*i,NS,NS) = _dt*_A(j,i)*_ode.dgdx();   // aji * f'(yi)
      }
    }
    return result-MTK::Identity();
  }
};

// IMPLICIT RUNGE KUTTA Class
// This integration scheme has several stages which can also depend on each other.
// A system of nonlinear equations has to be solved in each time step.
// It is an implicit scheme.
template<const int S, const int SN, typename TS=double, int NP=Dynamic, int NS=Dynamic, typename TP=TS>
class IMPLICIT_RUNGE_KUTTA : public INTEGRATOR<TS,NP,NS,TP> {
  typedef Matrix<TS,NS,1>  VTS;
  typedef Matrix<TS,NS,NS> MTS;
  typedef Matrix<TP,NP,1>  VTP;
  typedef Matrix<TP,SN,1>  VTK;
  typedef Matrix<TP,SN,SN> MTK;
  typedef Matrix<TP,S,1>   VKK;
  typedef Matrix<TP,S,S>   KK;
  NONLINEAR_SOLVER<TP,NP,SN,TP>& _nlsol; // Solver
  KK _A;
  VKK _b;
  VKK _c;
public:
  // Constructor
  IMPLICIT_RUNGE_KUTTA(NONLINEAR_SOLVER<TP,NP,SN,TP>& nlsol) : _nlsol(nlsol), _A(KK::Zero()), _b(VKK::Zero()), _c(VKK::Zero()) {};

  // Setter methods
  void setA(KK& A) { _A=A; }
  void setB(VKK& b) { _b=b; }
  void setC(VKK& c) { _c=c; }

  VTS integrate(TP lb, TP ub, ODE<TS,NP,NS,TP>& ode) {
    TP h = ub-lb;
    VTS x_prev = ode.x(); // remember old x

    // estimate F(Y) 
    VTK k;
    for (int i=0;i<S;i++) k.segment(NS*i,NS) = ode.g();

    // set up implicit system to find k
    IMPLICIT_RUNGE_KUTTA_SYSTEM<S,SN,TS,NP,NS,TP> rksys(ode,x_prev,h,k,_A);

    _nlsol.solve(rksys);

    VTK grad = VTK::Zero();   // F(Y)
    for (int i=0;i<S;i++) {
      ode.x() = rksys.x().segment(NS*i,NS);   // yi
      grad.segment(NS*i,NS) = ode.g();  // f(yi)
    }
 
    ode.x() = x_prev; // reset x in ode

    // apply weighted sum of k
    VTS sum = VTS::Zero();
    for (int i=0;i<S;i++) sum += _b(i)*grad.segment(NS*i,NS);

    return h*sum;
  }
};

#endif